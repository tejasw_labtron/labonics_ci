

/***********************************************************
      Cart functions
************************************************************/



$(function() {
    $(".delete-from-cart").click(function() {
        var rconfirm = confirm("Do you surely want to delete this product from cart?");
       if(rconfirm)
       {
         var row_id    = $(this).data("productrowid");
       
          $.ajax({
            type: "POST",
            url: base_url + "cart/delete_cart",
            data: {row_id : row_id},
             dataType:"json",
            success: function(data) 
            {
              if(data.success==true)
              {
                $("#lab_cart_count").empty();
                $("#lab_cart_count").text(data.cart_count);
                location.reload();
              }
              else
              {
                swal({
                     title: "",
                     text: data.message,
                     type: "error",
                     timer: 3000,
                     });
              }
              
            }
          });

      }

    })
});


$(function() {
    $(".empty-cart").click(function() {
        var rconfirm = confirm("Do you surely want to empty the cart?");
       if(rconfirm)
       {
          $.ajax({
            type: "POST",
            url: base_url + "cart/empty_cart",
             dataType:"json",
            success: function(data) 
            {
              if(data.success==true)
              {
              
                location.reload();
              }
              else
              {
                swal({
                     title: "",
                     text: data.message,
                     type: "error",
                     timer: 3000,
                     });
              }
              
            }
          });

      }

    })
});


