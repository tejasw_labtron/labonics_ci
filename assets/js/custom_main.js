/***********************************************************
      Compare Products
************************************************************/


$(document).ready(function(){
   
    var $btn_compare = $('.btn-compare');
    var get_checked_id = function (e) {
        var array = [];
        var count = 0;
        $(".action--compare-add .checkbox:checked").each(function () {
            count++;
            if (count > 4) {
                alert('Cannot add more than 4 products to Compare');
                return false;
            }

            if (count > 0) {
                $btn_compare.show();
            }
            if (count == 0 || count == null) {
                $btn_compare.hide();
            }
            array.push(this.value);
            $btn_compare.text('COMPARE (' + count + ')');
        });
        if (count == 0) {
            $btn_compare.hide();
        }
        var ids = array.join(",");
        if (ids && count > 1) {
            //alert(ids);

         
            $btn_compare.attr('href', base_url + 'compare?products='+ ids);
        }
    };
    $(".checkbox").on("click", get_checked_id);

    /*category sidebar*/
if ($(".Category_page_class").length > 0) {

    var url = window.location.pathname;
    var i;

    for(i=0;i<=2;i++){// i<=1 for live site(.com) , i<=2 for localhost
        url = url.substring(url.indexOf("/")+1);
    }
    var category = url;
    if(url.indexOf("/")!=-1){
        category = url.substring(0,url.indexOf("/"));
    }
    $('#'+category).show();
    $('.'+category+'_showsub').hide();
    $('.'+category+'_hidesub').show();
    $('ul.sidebar_categories').prepend($('.'+category));
  }
/*end category sidebar*/
});


/***********************************************************
      Cart functions
************************************************************/

$(function() {
/*   $(".mini-cart-wrap").click(function() {
        $(".cart-list").html('');

        $.ajax({
            type: "POST",
            url: base_url + "cart/show_cart",
            success: function(data) 
            {
             /* console.log("in")
              console.log(data);]
                $(".cart-list").html(data);
            }
          });

        //$(".cart-list").toggleClass("show_myfiza");

    })*/

$(".minicart-icon").hover( function(){
 // $(".cart-dropdown").slideToggle();
   $(".cart-dropdown").html('');
     $.ajax({
            type: "POST",
            url: base_url + "cart/show_cart",
            success: function(data) 
            {
             /* console.log("in")
              console.log(data);*/
                $(".cart-dropdown").html(data);
            }
          });
  });


});


$(function() {
    $(".add-to-cart").click(function() {
        
         var product_id    = $(this).data("productid");
         var product_qty    = $(this).data("productqty");
        $.ajax({
            type: "POST",
            url: base_url + "cart/add_to_cart",
            data: {product_id : product_id,product_qty:product_qty},
             dataType:"json",
            success: function(data) 
            {
              if(data.success==true)
              {
                $("#lab_cart_count").empty();
                $("#lab_cart_count").text(data.cart_count);
                $(".count-amount").html(data.total_amount);                
                swal({
                     title: "",
                     text: data.message,
                     type: "success",
                     timer: 3000,
                     });
                location.reload();
              }
              else
              {
                swal({
                     title: "",
                     text: data.message,
                     type: "error",
                     timer: 3000,
                     });
              }
              
            }
          });

       

    })
});


$(function() {
    $(".update-to-cart").click(function() {

         var quantity = new Array();
         var rowid = new Array();
         var theValue = document.getElementById('theValue').value; 

         for(i=1;i<=theValue;i++)
        {
          quantity.push($('#quantity'+i).val());
          rowid.push($('#rowid'+i).val());
        } 

        data={
           quantity:quantity,
           rowid:rowid,
           theValue:theValue
         },

        $.ajax({
            type: "POST",
            url: base_url + "cart/update_to_cart",
           data:data,
             dataType:"json",
            success: function(data) 
            {
              if(data.success==true)
              {
                $("#lab_cart_count").empty();
                $("#lab_cart_count").text(data.cart_count);
                 $(".count-amount").html(data.total_amount);
                location.reload();
              }
              else
              {
                swal({
                     title: "",
                     text: "Some error occured , please try again",
                     type: "error",
                     timer: 3000,
                     });
              }
              
            }
          });

       

    })
});


$(function() {
    $(".delete-from-cart").click(function() {
        var rconfirm = confirm("Do you surely want to delete this product from cart?");
       if(rconfirm)
       {
         var row_id    = $(this).data("productrowid");
       
          $.ajax({
            type: "POST",
            url: base_url + "cart/delete_cart",
            data: {row_id : row_id},
             dataType:"json",
            success: function(data) 
            {
              if(data.success==true)
              {
                $("#lab_cart_count").empty();
                $("#lab_cart_count").text(data.cart_count);
                location.reload();
              }
              else
              {
                swal({
                     title: "",
                     text: data.message,
                     type: "error",
                     timer: 3000,
                     });
              }
              
            }
          });

      }

    })
});

$(function() {
    $(".remove-from-cart").click(function() {
        var rconfirm = confirm("Do you surely want to delete this product from cart?");
       if(rconfirm)
       {
         var row_id    = $(this).data("productrowid");
       
          $.ajax({
            type: "POST",
            url: base_url + "cart/delete_cart",
            data: {row_id : row_id},
             dataType:"json",
            success: function(data) 
            {
              if(data.success==true)
              {
                $("#lab_cart_count").empty();
                $("#lab_cart_count").text(data.cart_count);
                location.reload();
              }
              else
              {
                swal({
                     title: "",
                     text: data.message,
                     type: "error",
                     timer: 3000,
                     });
              }
              
            }
          });

      }

    })
});


$(function() {
    $(".empty-cart").click(function() {
        var rconfirm = confirm("Do you surely want to empty the cart?");
       if(rconfirm)
       {
          $.ajax({
            type: "POST",
            url: base_url + "cart/empty_cart",
             dataType:"json",
            success: function(data) 
            {
              if(data.success==true)
              {
              
                location.reload();
              }
              else
              {
                swal({
                     title: "",
                     text: data.message,
                     type: "error",
                     timer: 3000,
                     });
              }
              
            }
          });

      }

    })
});


/***********************************************************
     Contact Us Forms Validation 
************************************************************/

$(function()
{
    $("#customization_form").validate(
        {  
    
            errorClass  : "errormesssage",
            ignore      : 'input[type="hidden"]',
            rules: 
            {
                company_name: 
                {
                    required    : true
                }, 
               
                email: 
                {
                    email       : true,
                    required    :true,
                   
                },  
                phone: 
                {
                   required     :true,
                },
               
                
             },
             messages: 
             {
                 company_name : 
                 {
                     required   : "Please provide company name.",
                 },
              
                email: 
                {
                    required    : "Please provide your email address.",
                    email       : "Please provide valid email address.",
                },
                phone: 
                {
                   required     : "Please provide phone.",
                },
              
                
            },
            submitHandler: function(form)
            {
                try
                {
                   dataString = $('#customization_form').serialize();
                   
                    $('#form_submit_reg').button('loading');
    
                    $.ajax
                        ({
                        type    : "POST",
                        url     : base_url+ "customization",
                        data    : dataString,
                        dataType: "json",
                        beforeSend: function()
                        {
                             
                            $("#form_submit_reg").html('<i class="fa fa-spinner fa-spin" style="font-size:18px"></i> Connecting');
                        },
                        success: function(response)
                        {
    
                            if(response.success == true)
                            {
                                
                                //$("#contact_us_thanks").show();
                                 swal({
                                        "title": "", 
                                        "text": response.message, 
                                        "type": "success",
                                        timer: 3000,
                                        "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                                    });
    
                                $('#form_submit_reg').html('Send Message');
                                $('#customization_form')[0].reset();
                            }
                            else
                            {
                                 $('#customization_form')[0].reset();
    
                                 swal({
                                        "title": "", 
                                        "text": response.message, 
                                        "type": "error",
                                        "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                                    });
    
                                $('#form_submit_reg').button('reset');
                            }
    
                        }
                    });
                }
                catch(e)
                {
                    console.log(e);
                }
            }
    
        });
    $("#contact_form").validate(
    {  

        errorClass  : "errormesssage",
        ignore      : 'input[type="hidden"]',
        rules: 
        {
            name: 
            {
                required    : true
            }, 
           
            email: 
            {
                email       : true,
                required    :true,
               
            },  
            message: 
            {
               required     :true,
            },
           
            
         },
         messages: 
         {
             name : 
             {
                 required   : "Please provide contact name.",
             },
          
            email: 
            {
                required    : "Please provide your email address.",
                email       : "Please provide valid email address.",
            },
            message: 
            {
               required     : "Please enter your message.",
            },
          
            
        },
        submitHandler: function(form)
        {
            try
            {
               dataString = $('#contact_form').serialize();
                  
                $('#form_submit_reg').button('loading');

                $.ajax
                    ({
                    type    : "POST",
                    url     : base_url+ "contact-us",
                    data    : dataString,
                    dataType: "json",
                    beforeSend: function()
                    {
                         
                        $("#form_submit_reg").html('<i class="fa fa-spinner fa-spin" style="font-size:18px"></i> Connecting');
                    },
                    success: function(response)
                    {
                       

                        if(response.success == true)
                        {
                            
                            //$("#contact_us_thanks").show();
                             swal({
                                    "title": "", 
                                    "text": response.message, 
                                    "type": "success",
                                    timer: 3000,
                                    "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                                });

                            $('#form_submit_reg').html('Send Message');
                            $('#contact_form')[0].reset();
                        }
                        else
                        {
                             $('#contact_form')[0].reset();

                             swal({
                                    "title": "", 
                                    "text": response.message, 
                                    "type": "error",
                                    "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                                });

                            $('#form_submit_reg').button('reset');
                        }

                    }
                });
            }
            catch(e)
            {
                console.log(e);
            }
        }

    });
    jQuery.validator.addMethod("alpha_numeric", function(value, element) 
    {
        return this.optional(element) ||  /^((\+)|0|-?)?[0-9]\d{9,11}$/g.test(value);
    });


    $("#getintouch_form").validate(
    {  

        errorClass      : "errormesssage",
        ignore          : 'input[type="hidden"]',
        rules: 
        {
            c_emai: 
            {
                email   : true,
                required:true,
            },  
           
         },
         messages: 
         {
        
            c_email: 
            {
                required: "Please provide your email address.",
                email   : "Please provide valid email address.",
            },
         
         },
        submitHandler: function(form)
        {
            try
            {
                dataString = $('#getintouch_form').serialize();
                $('#form_submit_git').button('loading');
            
            $.ajax
                ({
                    type    : "POST",
                    url     : base_url+ "home/getintouch_Data",
                    data    : dataString,
                    dataType:"json",
                    beforeSend: function()
                    {
                        $("#form_submit_git").html('<i class="fa fa-spinner fa-spin" style="font-size:18px"></i>Processing');
                    },
                    success: function(response)
                    {
                      
                        if(response.success==true)
                        {
                            
                             swal({
                                    "title" : "", 
                                    "text"  : response.message, 
                                    "type"  : "success",
                                    timer   : 3000,
                                    "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                                });

                            $('#form_submit_git').html('Subscribe');
                            $('#getintouch_form')[0].reset();
                          
                        }
                        else
                        {
                           
                             swal({
                                    "title" : "", 
                                    "text"  : response.message, 
                                    "type"  : "error",
                                    "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                                });

                            $('#form_submit_git').button('reset');
                        }

                    }
                });
            }
            catch (e)
            {
                console.log(e);
            }
        }

    });
    jQuery.validator.addMethod("alpha_numeric", function(value, element) 
    {
        return this.optional(element) ||  /^((\+)|0|-?)?[0-9]\d{9,11}$/g.test(value);
    });


/***********************************************************
     Get Quote Forms Validation 
************************************************************/


    $("#getquote_form").validate(
    {  
        errorClass              : "errormesssage",
        ignore                  : 'input[type="hidden"]',
        rules: 
        {
            c_name: 
            {
                required        : true
            }, 
            c_phone:
            {
                alpha_numeric   : true,
                minlength       : 10,
                maxlength       : 14,
            },
            c_email: 
            {
                email           : true,
                required        : true,
               
            },  
            c_message: 
            {
               required         : true,
            },
           
            
         },
         messages: 
         {
            c_name : 
            {
                 required       : "Please provide company name.",
            },
            c_phone:
            {
                
                alpha_numeric   : "enter valid number.",
                minlength       : "Your phone number must be of 10 digits.",
                maxlength       : "Your phone number can be max of 14 digits.",
            },
            
            c_email: 
            {
                required        : "Please provide your email address.",
                email           : "Please provide valid email address.",
            },
            c_message:
            {
               required         : "Please enter your message.",
            },
          
            
         },
        submitHandler: function(form)
        {
            try
            {
               
                dataString = $('#getquote_form').serialize();
                $('#form_submit_reg').button('loading');
            
            $.ajax
                ({
                    type    : "POST",
                    url     : base_url+ "home/getquote_Data",
                    data    : dataString,
                    dataType:"json",

                    beforeSend: function()
                    {
                        $("#form_submit_reg").html('<i class="fa fa-spinner fa-spin" style="font-size:18px"></i> Connecting');
                    },
                    success: function(response)
                    {
                         $('#getQuote').modal('hide');

                        if(response.success==true)
                        {
                            swal({
                                "title": "", 
                                "text": response.message, 
                                "type": "success",
                                timer: 3000,
                                "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                            });
                            // $('#getquote_thanks').modal('show');
                          
                            $('#getquote_form')[0].reset();
                            $('#form_submit_reg').html('Get Quote');
                          
                        }
                        else
                        {
                            
                             swal({
                                    "title": "", 
                                    "text": response.message, 
                                    "type": "error",
                                    "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                                });

                            $('#form_submit_reg').button('reset');
                        }

                    }
                });
            }
            catch(e)
            {
                console.log(e);
            }
        }

    });
    jQuery.validator.addMethod("alpha_numeric", function(value, element) 
    {
        return this.optional(element) ||  /^((\+)|0|-?)?[0-9]\d{9,11}$/g.test(value);
    });

    $("#getallquote_form").validate(
    {  
        errorClass              : "errormesssage",
        ignore                  : 'input[type="hidden"]',
        rules: 
        {
            c_email: 
            {
                email           : true,
                required        : true,
               
            },
            
         },
         messages: 
         {
            
            c_email: 
            {
                required        : "Please provide your email address.",
                email           : "Please provide valid email address.",
            },
           
          
            
         },
        submitHandler: function(form)
        {
            try
            {
               
                dataString = $('#getallquote_form').serialize();
                $('#form_submit_reg').button('loading');
            
            $.ajax
                ({
                    type    : "POST",
                    url     : base_url+ "home/getallquote_Data",
                    data    : dataString,
                    dataType:"json",

                    beforeSend: function()
                    {
                        $("#form_submit_reg").html('<i class="fa fa-spinner fa-spin" style="font-size:18px"></i> Connecting');
                    },
                    success: function(response)
                    {
                         $('#getAllQuote').modal('hide');

                        if(response.success==true)
                        {
                             swal({
                                    "title": "", 
                                    "text": response.message, 
                                    "type": "success",
                                    "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                                });
                          
                            $('#getallquote_form')[0].reset();
                            $('#form_submit_reg').html('Get Quote');
                          
                        }
                        else
                        {
                            
                             swal({
                                    "title": "", 
                                    "text": response.message, 
                                    "type": "error",
                                    "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                                });

                            $('#form_submit_reg').button('reset');
                        }

                    }
                });
            }
            catch(e)
            {
                console.log(e);
            }
        }

    });

});

/***********************************************************
    Admin Distributors functions
************************************************************/


function activate_distributor(id)
{
  //alert("in distributor_active");
   //var auth_id = $(this).closest("tr").find("input[name='auth_id']").val();
   var auth_id = id;
   var result = confirm("Do you surely want to approve this company?");
   if(result)
   {
     $.ajax
        ({
            type: "POST",
            url:base_url+ "login/approve_distributor",
            data: {auth_id:auth_id},
            dataType:"json",
            success: function(response)
            {
               
                if(response.success==true)
                {
                    location.reload();
                }
                else
                {
                   
                    swal({
                        "title": "", 
                        "text": response.message, 
                        "type": "error",
                        "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                    });
                    
                }

            }
        });
   }
    
}

function block_distributor(id)
{
   var auth_id = id;
   var result = confirm("Do you surely want to block this company?");
   if(result)
   {
     $.ajax
        ({
            type: "POST",
            url:base_url+ "login/block_distributor",
            data: {auth_id:auth_id},
            dataType:"json",
            success: function(response)
            {
                if(response.success==true)
                {
                   
                   location.reload();
                }
                else
                {
                    swal({
                        "title": "", 
                        "text": response.message, 
                        "type": "error",
                        "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                    });
                    
                }

            }
        });
   }
    
}

/***********************************************************
    Login/Registeration/ FOrgot Password Forms Validation 
************************************************************/


$(function()
{
   
    $("#login_form").validate(
    {
        errorClass: "errormesssage",
        rules:
        {
          email: "required",
          password: "required"
        },
        messages:
        {
          email: "Please Enter your Email Id",
          password: "Please Enter a Password"
        },
        submitHandler: function(form)
        {
          
            try
            {
                var usr_username = document.getElementById('email').value;
                var usr_password = document.getElementById('password').value;
                var rememberme = 0;
                /*if (document.getElementById('rememberme').checked)
                {
                    var rememberme = 1;
                }
                else
                {
                    var rememberme = 0;
                }*/
                var ref = $('#ref').val();
                data = {
                    usr_username: usr_username,
                    usr_password: usr_password,
                    rememberme: rememberme,
                    ref: ref
                }

                $('.btn_save').button('loading');

                $.ajax(
                {
                    type: "POST",
                    url: base_url + "login/check_login",
                    data: data,
                    dataType: "json",
                    beforeSend: function()
                    {
                        $("#form_submit").html('<i class="fa fa-spinner fa-spin" style="font-size:18px"></i> Connecting');
                    },
                    success: function(response)
                    {
                      console.log(response);
                        if (response.success == true)
                        {
                           swal({
                                 title: "",
                                 text: "Logged In Successfully",
                                 type: "success",
                                 timer: 3000,
                                 });
                                
                                   window.location.href = response.linkn;
                                   
                                 
                             //swal(response.message);
                              
                        }
                        else
                        {
                            $('.btn_save').button('reset');
                            $("#form_submit").html(' Sign In');
                            swal(response.message);
                        }
                    }
                });
            }
            catch (e)
            {
                console.log(e);
            }
        }
    });

    $("#reg-frm").validate(
    {  
        errorClass: "errormesssage",
        ignore: 'input[type="hidden"]',
         rules: {
            company_name: {
                required: true
            }, 
            contact_person: {
                required: true
            },  
            password: {
                required: true,
                minlength: 5,
            },  
            password_confirmation: {
                required: true,
                minlength: 5,
                equalTo: "#password",
            },  
            
            contact_number:
            {
                alpha_numeric:true,
                minlength: 10,
                maxlength:14,
            },
            email: {
                email: true,
                required:true,
                remote: {
                        url: base_url + "login/remote_email_exists",
                        type: "post",
                        data: {c_email: function () {
                                return $('#email').val();
                            }
                        }
                    },
            },  
            country: {
               required:true,
            },
           
            
         },
         messages: {
             company_name : {
                 required: "Please provide company name.",
             },
            contact_person: {
                required: "Please provide company person name.",
            },  
             
            contact_number:
            {
                
                alpha_numeric: "enter valid number.",
                minlength: "Your phone number must be of 10 digits.",
                maxlength: "Your phone number can be max of 14 digits.",
            },
            
            email: {
                required: "Please provide your email address.",
                email: "Please provide valid email address.",
                remote:"Email Id already registered",
            },
           
            password: {
                required: "Please enter your password",
                minlength: "Your password must be at least 5 characters long.",
            },  
            password_confirmation: {
                required: "Please re-enter new password.",
                minlength: "Your password must be at least 5 characters long.",
                equalTo: "Your passwords do not match.",
            },
            country: {
               required:"Please enter location.",
            },
          
            
         },
        submitHandler: function(form)
        {
            try
            {
               

                  dataString = $('#reg-frm').serialize();
                   $('.btn_regsave').button('loading');
            
            $.ajax
                ({
                    type: "POST",
                    url:base_url+ "login/new_register",
                    data: dataString,
                    dataType:"json",
                    beforeSend: function()
                    {
                       $('.btn_regsave').html('<i class="fa fa-spinner fa-spin" style="font-size:18px"></i> Connecting');
                    },
                    success: function(response)
                    {
                       
                        if(response.success==true)
                        {
                             swal({
                                    "title": "", 
                                    "text": response.message, 
                                    "type": "success",
                                    timer: 3000,
                                    "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                                });
                          window.location.href=response.linkn;
                        }
                        else
                        {
                             swal({
                                    "title": "", 
                                    "text": response.message, 
                                    "type": "error",
                                    "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                                });
                            $('.btn_regsave').button('reset');
                            $("#form_submit").html(' Regsiter');
                        }

                    }
                });
            }
            catch (e)
            {
                console.log(e);
            }
        }

    });
        jQuery.validator.addMethod("alpha_numeric", function(value, element) {
            return this.optional(element) ||  /^((\+)|0|-?)?[0-9]\d{9,11}$/g.test(value);
    });


    $("#user-edit").validate(
    {  
        errorClass: "errormesssage",
        ignore: 'input[type="hidden"]',
         rules: {
            c_name: {
                required: true
            }, 
             c_person: {
                required: true
            },  
            
            
            c_phone:
            {
                alpha_numeric:true,
                minlength: 10,
                maxlength:14,
            },
           
            c_location: {
               required:true,
            },
           
            
         },
         messages: {
             c_name : {
                 required: "Please provide company name.",
             },
            c_person: {
                required: "Please provide company person name.",
            },  
             
            c_phone:
            {
                
                alpha_numeric: "enter valid number.",
                minlength: "Your phone number must be of 10 digits.",
                maxlength: "Your phone number can be max of 14 digits.",
            },
            c_location: {
               required:"Please enter location.",
            },
          
            
         },
        submitHandler: function(form)
        {
            try
            {
               
                var user_id = document.getElementById('enc_user_id').value;
                  dataString = $('#user-edit').serialize();
                   $('.btn_regsave').button('loading');
            
            $.ajax
                ({
                    type: "POST",
                    url:base_url+ "login/user_profile_edit/"+user_id,
                    data: dataString,
                    dataType:"json",
                    beforeSend: function()
                    {
                       $('.btn_regsave').html('<i class="fa fa-spinner fa-spin" style="font-size:18px"></i> Connecting');
                    },
                    success: function(response)
                    {
                       
                        if(response.success==true)
                        {
                             swal({
                                    "title": "", 
                                    "text": response.message, 
                                    "type": "success",
                                    timer: 3000,
                                    "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                                });
                          window.location.href=response.linkn;
                        }
                        else
                        {
                             swal({
                                    "title": "", 
                                    "text": response.message, 
                                    "type": "error",
                                    "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                                });
                            $('.btn_regsave').button('reset');
                            $("#form_submit").html(' Regsiter');
                        }

                    }
                });
            }
            catch (e)
            {
                console.log(e);
            }
        }

    });
        jQuery.validator.addMethod("alpha_numeric", function(value, element) {
            return this.optional(element) ||  /^((\+)|0|-?)?[0-9]\d{9,11}$/g.test(value);
    });


    $("#ftpw_email_form").validate(
    {
        errorClass: "errormesssage",
        rules:
        {
          email: "required",
        },
        messages:
        {
          email: "Please Enter your Email Id",
        },
        submitHandler: function(form)
        {
            try
            {
                var usr_username = document.getElementById('email').value;
             
                data = {
                    usr_username: usr_username
                }

                $('.ftpw_email').button('loading');

                $.ajax(
                {
                    type: "POST",
                    url: base_url + "forgot-password",
                    data: data,
                    dataType: "json",
                    beforeSend: function()
                    {
                        $("#form_submit_ftpw_email").html('<i class="fa fa-spinner fa-spin" style="font-size:18px"></i> Connecting');
                    },
                    success: function(response)
                    {
                      console.log(response);
                        if (response.success == true)
                        {
                          swal({
                                    "title": "", 
                                    "text": response.message, 
                                    "type": "success",
                                    timer: 5000,
                                    "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                                });
                          window.location.href=response.linkn;
                              
                        }
                        else
                        {

                            $('.ftpw_email').button('reset');
                            $("#form_submit_ftpw_email").html('Submit');
                            swal(response.message);
                        }
                    }
                });
            }
            catch (e)
            {
                console.log(e);
            }
        }
    });


     $("#reset_password_form").validate(
    {
        errorClass: "errormesssage",
        rules:
        {
            c_password: {
                    required: true,
                    minlength: 5,
            },  
            confirm_password: {
                required: true,
                minlength: 5,
                equalTo: "#c_password",
            },  
        },
        messages:
        {
            c_password: {
                required: "Please enter your password",
                minlength: "Your password must be at least 5 characters long.",
            },  
            confirm_password: {
                required: "Please re-enter new password.",
                minlength: "Your password must be at least 5 characters long.",
                equalTo: "Your passwords do not match.",
            },
        },
        submitHandler: function(form)
        {
            try
            {
                var c_password = document.getElementById('c_password').value;
                var auth_id = document.getElementById('auth_id').value;
                var encrypted_id = document.getElementById('encrypted_id').value;
              
                data = {
                    c_password: c_password,auth_id:auth_id
                }

                $('.rst_pswd').button('loading');

                $.ajax(
                {
                    type: "POST",
                    url: base_url + "forgot-password-"+encrypted_id,
                    data: data,
                    dataType: "json",
                    beforeSend: function()
                    {
                        $("#form_submit_rst_pswd").html('<i class="fa fa-spinner fa-spin" style="font-size:18px"></i> Connecting');
                    },
                    success: function(response)
                    {
                      console.log(response);
                        if (response.success == true)
                        {
                           swal({
                                 title: "",
                                 text: "Password changed Successfully!! Please login",
                                 type: "success",
                                 timer: 5000,
                                 });
                                
                                 window.location.href=response.linkn;
                                 
                             //swal(response.message);
                              
                        }
                        else
                        {

                            $('.rst_pswd').button('reset');
                            $("#form_submit_rst_pswd").html('Submit');
                            swal(response.message);
                        }
                    }
                });
            }
            catch (e)
            {
                console.log(e);
            }
        }
    });

});


/***********************************************************
    A-Z Product Display functions
************************************************************/

function list_products(alphabet)
{
    $('.alpha_products').hide();
    $('#'+alphabet).css('display','block');
}

function hide_products()
{
    $('.alpha_products').hide();
}


/***********************************************************
   Search  functions
************************************************************/

//  document.getElementById("search_form_submit").onclick = function()
//   {
//     var product  = $("#product_name").val();
//     if(product != "" && product.length > 0)
//       {
//         document.getElementById("search_products").submit();
//       }
//     return false;
//   } 

  /*document.getElementById("search_form_submit_2").onclick = function()
  {
    var product  = $("#product_name_2").val();
    if(product != "" && product.length > 0)
      {
        document.getElementById("search_products_2").submit();
      }
    return false;
  } 
*/
/***********************************************************
  Catalog modal 
************************************************************/

   function getproducts_cat(id)
  {
      $.ajax({
          type: "POST",
          url: base_url+"product/get_catalog_products_by_catid",
          data : { category_id:id},
          success: function(response){
            console.log(response);
                $("#myCatalogModal").modal('show');
                $("#addcards").html(response);
             
          
          }
      }); 
  }


/***********************************************************
 Footer sitemap 
************************************************************/

  $('#sitemap_click').click(function() 
  {
    $('#sitemap_display').toggle("slow", function()
    {
      $('#sitemap_display').get(0).scrollIntoView();
    }); 
    $("i", this).toggleClass("icon-arrow-up2 icon-arrow-down2");

  });

   // scroll to top
$(window).on('scroll',function(){
    if($(this).scrollTop() > 600){
        $('.scroll-top').removeClass('not-visible');
    }
    else{
        $('.scroll-top').addClass('not-visible');
    }
});
$('.scroll-top').on('click',function (event){
    $('html,body').animate({
        scrollTop:0
    },1000);
});