<?php 
class Labtron_library
{
    
    public $CI;
    private $rest_username;
    private $rest_password;
    public function __construct()
    {
        $this->CI           = &get_instance();
        $this->rest_username = $this->CI->config->item('rest_user');
        $this->rest_password = $this->CI->config->item('rest_pass');

         $this->CI->load->database();
    }

    function sendEmail($arrEmailData = array())
    {
        log_message('ERROR','>> sendEmail inside if ');

        $email_from_addr        = isset($arrEmailData['admin_email_from'])?$arrEmailData['admin_email_from']:$this->CI->config->item('admin_email_from');
        $email_from_name        = isset($arrEmailData['admin_from_name'])?$arrEmailData['admin_from_name']:$this->CI->config->item('admin_from_name');
        
        $email_message          = $arrEmailData['email_content'];
        $email_template         = $arrEmailData['email_template'];
        $email_template_data    = $arrEmailData['email_template_data'];
        $subject                = $arrEmailData['email_subject'];
        
        //Find and replace values in email
        $find                   = array("#NAME#", "#EMAIL#", "#COMPANYNAME#");
        $replace                = array($arrEmailData['name'], $arrEmailData['email'], $this->CI->config->item('admin_from_name'));
        $email_message_string   = str_replace($find, $replace, $email_message);
        $email                  = $arrEmailData['email'];
        
        $bcc_email              = $this->CI->config->item('email_bcc');
        if(isset($arrEmailData['nobcc']) && !empty($arrEmailData['nobcc']))
        {
            $bcc_email          = '';
        }
        
        
        if(!empty($email))
        {   
         
            if(is_array($email))
            {
                $member_email           = $email;
            }
            else
            {
                $member_email           = array($email);
            }
            
            if($this->CI->config->item('mail_is_smtp'))
            {
               log_message('ERROR','>> sendEmail inside if  mail_is_smtp');
                $email_config   = array(
                  'protocol'    => 'smtp',
                  'smtp_host'   => (!empty(bsnprm_value(BSN_MAIL_SMTP_HOST)))?bsnprm_value(BSN_MAIL_SMTP_HOST):$this->CI->config->item('mail_smtp_host'),

                  'smtp_user'   => (!empty(bsnprm_value(BSN_MAIL_SMTP_USER)))?bsnprm_value(BSN_MAIL_SMTP_USER):$this->CI->config->item('mail_smtp_user'),

                  'smtp_pass'   => (!empty(bsnprm_value(BSN_MAIL_SMTP_PASS)))?bsnprm_value(BSN_MAIL_SMTP_PASS):$this->CI->config->item('mail_smtp_pass'),

                  'smtp_port'   => (!empty(bsnprm_value(BSN_MAIL_SMTP_PORT)))?bsnprm_value(BSN_MAIL_SMTP_PORT):$this->CI->config->item('mail_smtp_port'),
                  //'smtp_crypto'   => 'ssl',
                  //'smtp_keepalive' => TRUE,
                  //'smtp_timeout' =>'120',
                  '_smtp_auth'  => "TRUE",
                  'mailtype'    => 'html',
                  'charset'     => 'utf-8',
                  'crlf'        => "\r\n",
                  'newline'     => "\r\n"
                );  
            }
            else
            {

               log_message('ERROR','>> sendEmail inside if not mail_is_smtp');

                $email_config   = array(

                      'useragent'    => 'CodeIgniter',
                      'protocol'    => 'mail',
                      'smtp_host'   => 'localhost',

                      /* 'smtp_port'   => 25,
                      'smtp_crypto' => '',
                      'smtp_keepalive' => FALSE,
                      'smtp_timeout' =>5,*/
                      'mailtype'    => 'html',
                    /*  'wordwrap'    => TRUE,
                        'charset'     => 'UTF-8',
                        '_smtp_auth' => "TRUE",*/

                      'crlf'        => "\r\n",
                      'newline'     => "\r\n"

                    );  

            }
            //system > libraries > email
            $this->CI->load->library('email');
            $this->CI->email->initialize($email_config);
            $this->CI->email->clear(TRUE);  
            $this->CI->email->set_mailtype('html');     
            $this->CI->email->set_newline("\r\n");
            $this->CI->email->from($email_from_addr,$email_from_name);  
            $this->CI->email->to($member_email); 
            
            //bcc_email
            if(isset($bcc_email) && !empty($bcc_email))
            {
                $this->CI->email->bcc($bcc_email);
            }
            
            if(isset($arrEmailData['attachment']) && !empty($arrEmailData['attachment']))
            {
                $this->CI->email->attach($arrEmailData['attachment']); 
            }
            $this->CI->email->subject($subject);
            if(!empty($email_template))
            {
                $data['emailData'] = $email_template_data;
                $email_message_string = $this->CI->load->view('./emailtemplate/'.$email_template, $data , true);
            }
             
            log_message('ERROR','>> sendEmail inside if reached here');
            
            $this->CI->email->message($email_message_string);  
            $this->CI->email->send();
            /*echo "<pre>";
            print_r($this->CI->email->print_debugger());
            exit;*/
        }
    }
}
 ?>
