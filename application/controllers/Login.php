<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {

	public function __construct()
    {
        parent::__construct();
    }

	public function index()
	{
		log_message('LABLOG','Login Page >> ');

		$data = '';
		$this->render_page('distributor/login',$data);
	}

	public function check_login()
	{
		$ref 		= $this->input->post('ref');
		$rememberme = $this->input->post('rememberme');     

		if(isset($_COOKIE['labtron_usr_username'])  &&  isset($_COOKIE['labtron_usr_password']))
		{ 
			
			$email = get_cookie('labtron_usr_username');
			$pwd   = get_cookie('labtron_usr_password');

			$_POST['usr_username'] = $email;
			$_POST['usr_password'] = $pwd;

			$value='1';  
			log_message('LABLOG','>>checklogin - cookie function check value'.$value );
			log_message('LABLOG','>>email'.$_POST['usr_username'] );
			log_message('LABLOG','>>password'.$_POST['usr_password'] );
		}
		else
		{
			$value='0';
			log_message('LABLOG','>>checklogin - cookie  function check value'.$value ); 

		}

		$arrRegister 			= array();
		$arrRegister['email'] 	= $this->input->post('usr_username');
		$pwd 					= $this->input->post('usr_password');

		if($value == '0')
            $arrRegister['password']	= encrypt_password($pwd);
        elseif ($value == '1')
            $arrRegister['password']	= $pwd; 


		$user_data = $this->general_model->getauthData($arrRegister);

		if(isset($user_data) && $user_data != '')
		{
			//Start of Condition to set the coookie in the client browser

			if(!isset($_COOKIE['labtron_usr_username'])  && !isset($_COOKIE['labtron_usr_password']))
			{ 
				log_message('LABLOG','>>checklogin - set cookie  function ');    
				if($rememberme == 1) 
				{
					log_message('LABLOG','>>checklogin - inside set cookie  function ');  
					set_cookie('labtron_usr_username', $user_data['email'], 3600*24*30*12*10);
					set_cookie('labtron_usr_password', $user_data['password'] , 3600*24*30*12*10 );

					log_message('LABLOG','>>checklogin - Cookie is Set - Email: '.$user_data['email']. ', usr_password: '.$user_data['password']);
				}
			}
			
	       	 //End of Condition to set the coookie in the client browser
			$check_status = 'no';
			switch (true) 
			{
			  	case ($user_data['role'] == AUTH_ROLE_ADMIN):
			   	 	$check_status = 'yes';
					log_message('LABLOG','>>checklogin - cookie  function return true >>AUTH_ROLE_ADMIN' );echo json_encode(array("success"=>true,"message"=>'Login Sucess' ,'linkn'=>base_url('distributor-dashboard')));
			  	break;
			  	case(isset($_COOKIE['labtron_usr_username']) && isset($_COOKIE['labtron_usr_password'])):
			   		$check_status = 'yes';
					log_message('LABLOG','>>checklogin - cookie  function return true >> ' );       
					echo json_encode(array("success"=>true,"message"=>'Login Sucess' ,'linkn'=>base_url('')));	
			 	break;
			   case($user_data['role'] == AUTH_ROLE_DISTRIBUTOR && $user_data['status'] != STATUS_ACTIVE):
			   		$check_status = 'no';
					echo json_encode(array('success'=>false,'message'=>'Please Keep patience your Account is currently under review.'));
			 	break;

			    case($user_data['role'] == AUTH_ROLE_DISTRIBUTOR && $user_data['status'] == STATUS_BLOCKED):
			   		$check_status = 'no';
					echo json_encode(array('success'=>false,'message'=>'Sorry, Your Account has been blocked by the Admin.'));		
			    break;
			    default:
			    	$check_status = 'yes';
					log_message('LABLOG','>>checklogin - cookie  function jsoncode' );
					echo json_encode(array("success"=>true,"message"=>'Login Success' ,'linkn'=>base_url()));
			  	break;
			}

			if($check_status == 'yes')
			{
				$newdata = array(
				   'id' 			=> $user_data['id'],
                   'email' 			=> $user_data['email'],
				   'company_name'  	=> $user_data['company_name'],
				   'company_person' => $user_data['company_person'],
				   'role' 			=> $user_data['role'],
				   'is_logged_in'	=> TRUE );
				
				$this->session->set_userdata($newdata);
				 $cart_data     = $user_data['cart_log'];
				 $cart_data	 	= json_decode($cart_data,true);
				$this->cart->insert($cart_data);
			}

		}
		else
		{
			if(isset($_COOKIE['labtron_usr_username']) && isset($_COOKIE['labtron_usr_password']))
			{
				return false;
				exit;
			} 
			else
			{
				echo json_encode(array('success'=>false,'message'=>'Email or password are not matching.'));
				exit;
			}
		}
	}
	
	public function logout()
	{

		$cart_data 				= json_encode($this->cart->contents());
		
		$auth_id 				= $this->session->userdata('id');
		$arrData['cart_log'] 	= $cart_data;

		update('id', $auth_id, $arrData, 'distributors');

		$newdata = array(
				   'id' 			=> '',
                   'email' 			=> '',
				   'company_name'  	=> '',
				   'company_person' => '',
				   'role' 			=> '',
				   'is_logged_in'	=> TRUE);


		$this->session->unset_userdata($newdata);

		$this->session->sess_destroy();

		//unset cookie on 'logout controller'

		delete_cookie('labtron_usr_username');
		delete_cookie('labtron_usr_password'); 

	    redirect('','refresh'); 
	}

	public function new_register()
	{

		$data['ref']= base_url();

		if($this->form_validation->run('register_user_validation') == FALSE)
		{
			$this->render_page('distributor/register',$data);
		}
		else
		{

			$arrData 		= array();
			$company_name 	= $this->input->post('company_name');
			$user_name 		= $this->create_username($company_name);

			$arrData 		= array(
		         'company_name'		=> $this->input->post('company_name'),
		         'password'			=> encrypt_password(trim($this->input->post('password'))),
		         'email'			=> $this->input->post('email'),
		         'contact_number'	=> $this->input->post('contact_number'),
		         'company_person'	=> $this->input->post('contact_person'),
		         'country'			=> $this->input->post('country'),
		         'username'			=> $user_name,
		         'role'				=> AUTH_ROLE_DISTRIBUTOR,
		         'created_dt'		=> date('Y-m-d H:i:s'),
		         'status'			=> STATUS_INACTIVE
		         );
				if($this->input->post('address') != ''){
					$arrData['address'] = $this->input->post('address');
				}else{
				$arrData['address'] = '';
				}

				$auth_id	= insert('distributors',$arrData);
				log_message('LABLOG','>>distributors auth_id'.$auth_id);

			if($auth_id)
			{
				$arrRegister = array();
				$arrRegister['auth_id'] = $auth_id;
				
				$customer_data = $this->general_model->getauthData($arrRegister);

					//Admin Mail
			    $arrEmailData                         = array();
			    $arrEmailData['email_subject']        = 'New Distributor Request - '.(bsnprm_value(BSN_WEBSITE_NAME))?bsnprm_value(BSN_WEBSITE_NAME):PROJECT_NAME;
			    $arrEmailData['email_content']        = '';
			    $arrEmailData['admin_email_from'] 	  = $customer_data['email'];
			    $arrEmailData['email_template']       = 'adminRegistration';
			    $arrEmailData['email_template_data']  = $customer_data;
			    $arrEmailData['name']                 = '';
			    $arrEmailData['email']                = (bsnprm_value(BSN_ADMIN_EMAIL))?bsnprm_value(BSN_ADMIN_EMAIL):ADMIN_EMAIL;
			    $arrEmailData['nobcc']        		  = "Yes";

			      //User Mail
			    $arrEmailData1                         = array();
			    $arrEmailData1['email_subject']        = 'Welcome to'.(bsnprm_value(BSN_WEBSITE_NAME))?bsnprm_value(BSN_WEBSITE_NAME):PROJECT_NAME.' !!';
			    $arrEmailData1['email_content']        = '';
			    $arrEmailData1['email_template']       = 'userRegistration';
			    $arrEmailData1['email_template_data']  = '';
			    $arrEmailData1['name']                 = '';
			    $arrEmailData1['email']                = $customer_data['email'];
			      //$arrEmailData1['nobcc']        		 = "No";


			    $this->labtron_library->sendEmail($arrEmailData);
			    $this->labtron_library->sendEmail($arrEmailData1);

			    echo json_encode(array("success"=>true,"message"=>'Registration Sucess' ,'linkn'=>base_url('register-thankyou')));
			}
			else
			{
				echo json_encode(array("success"=>false,"message"=>"Some error occured while registering, please try again"));

			}
		}
	}

	public function thankyou_register()
	{
		$data 							= '';

		$this->render_page('distributor/thank-you',$data);
	}

	public function remote_email_exists()
	{
		$email 	= $this->input->post('c_email');

		$result = $this->general_model->chk_email_exists($email);
		$status = "true";

		if ($result)
		{
			$status = $email . " is already taken. Please try another one.";
		}

		echo json_encode($status);
	}

	public function create_username($company_name)
	{
		$username = str_replace(' ', '_', $company_name).generateRandomOtp(5);
		$result = $this->general_model->chk_username_exists($username);
		if($result)
		{
			$this->create_username($company_name,$person_name);
		}
		return $username;
	}

	public function user_profile($user_id)
	{
		$data = array();
	
		$user_ids						= decrypt_openssl($user_id);
		$sdata['auth_id']				= $user_ids;
		$data['user_data']   			= $this->general_model->getauthData($sdata);
		$data['enc_user_id']   			= $user_id;
		$this->render_page('distributor/user_profile',$data);
	}


	public function user_profile_edit($user_id)
	{
		$data = array();
	
		$user_ids						= decrypt_openssl($user_id);
		$sdata['auth_id']				= $user_ids;
		$data['user_data']   			= $this->general_model->getauthData($sdata);
		$data['enc_user_id']   			= $user_id;


		if($this->form_validation->run('user_edit_validation') == FALSE)
		{
			$this->render_page('distributor/user_edit_profile',$data);
		}
		else
		{

			$arrData 		= array();

			$arrData 		= array(
		         'company_name'		=> $this->input->post('c_name'),
		         'email'			=> $this->input->post('c_email'),
		         'address'			=> '',
		         'contact_number'	=> $this->input->post('c_phone'),
		         'company_person'	=> $this->input->post('c_person'),
		         'country'			=> $this->input->post('c_location'),
		         );

				$auth_id	= update('id', $user_ids, $arrData, 'distributors');
				log_message('LABLOG','>>distributors auth_id'.$auth_id);

			if($auth_id)
			{
				$arrRegister = array();
				$arrRegister['auth_id'] = $auth_id;
				
				$customer_data = $this->general_model->getauthData($arrRegister);

					//Admin Mail
			    $arrEmailData                         = array();
			    $arrEmailData['email_subject']        = 'Updated Distributor Detials - '.(bsnprm_value(BSN_WEBSITE_NAME))?bsnprm_value(BSN_WEBSITE_NAME):PROJECT_NAME;
			    $arrEmailData['email_content']        = '';
			    $arrEmailData['admin_email_from'] 	  = $customer_data['email'];
			    $arrEmailData['email_template']       = 'adminDistrbutrUpdate';
			    $arrEmailData['email_template_data']  = $customer_data;
			    $arrEmailData['name']                 = '';
			    $arrEmailData['email']                = (bsnprm_value(BSN_ADMIN_EMAIL))?bsnprm_value(BSN_ADMIN_EMAIL):ADMIN_EMAIL;
			    $arrEmailData['nobcc']        		  = "Yes";

			      //User Mail
			    $arrEmailData1                         = array();
			    $arrEmailData1['email_subject']        = 'Porfile Updated on'.(bsnprm_value(BSN_WEBSITE_NAME))?bsnprm_value(BSN_WEBSITE_NAME):PROJECT_NAME.' !!';
			    $arrEmailData1['email_content']        = '';
			    $arrEmailData1['email_template']       = 'userProfileUpdate';
			    $arrEmailData1['email_template_data']  = '';
			    $arrEmailData1['name']                 = '';
			    $arrEmailData1['email']                = $customer_data['email'];
			      //$arrEmailData1['nobcc']        		 = "No";


			    $this->labtron_library->sendEmail($arrEmailData);
			    $this->labtron_library->sendEmail($arrEmailData1);

			    echo json_encode(array("success"=>true,"message"=>'Updated Sucess' ,'linkn'=>base_url('user-profile-'.$user_id)));
			}
			else
			{
				echo json_encode(array("success"=>false,"message"=>"Some error occured while updating, please try again"));

			}
		}


		
	}

	/* Forgot Password FUNCTIONS STARTS */

	public function email_verification()
	{

		log_message('LABLOG','forgot_password email_verification Page >> ');

		$data 							= '';

		if($this->form_validation->run('email_verification_validation') == FALSE)
		{	
			$this->render_page('distributor/email_verification',$data);
		}
		else
		{
			sleep(3);

			$arrRegister['email'] 		= $this->input->post('usr_username');

			$user_data 					= $this->general_model->getauthData($arrRegister);

			if(isset($user_data) && $user_data != '' && !empty($user_data))
			{
				$encrypted_id 							=  encrypt_openssl($user_data['id'].generateRandomOtp(5));
				$user_data['encrypted_id'] 				=  $encrypted_id;


				$auth_id 								= $user_data['id'];
				$arrData['forgotten_password_code'] 	= $encrypted_id;

				update('id', $auth_id, $arrData, 'distributors');

			       //User Mail
			      $arrEmailData1                         = array();
			      $arrEmailData1['email_subject']        = (bsnprm_value(BSN_WEBSITE_NAME))?bsnprm_value(BSN_WEBSITE_NAME):PROJECT_NAME.' | Password Reset Link!';
			      $arrEmailData1['email_content']        = '';
			      $arrEmailData1['email_template']       = 'userForgotPassword';
			      $arrEmailData1['email_template_data']  = $user_data;
			      $arrEmailData1['name']                 = '';
			      $arrEmailData1['email']                = $user_data['email'];
			      //$arrEmailData1['nobcc']        		 = "No";

			    $this->labtron_library->sendEmail($arrEmailData1);

			    echo json_encode(array("success"=>true,"message"=>'Mail successfully sent, please click on the link given on mail and proceed' ,'linkn'=>base_url('home')));
			}else{
				echo json_encode(array('success'=>false,'message'=>'Email id doesnot exist.'));
			}
		}

	}

	public function forgot_password($encrypted_id)
	{
		log_message('LABLOG','forgot_password function Page >> ');
		$data 							= array();
		$decrypted_id 				= decrypt_openssl($encrypted_id);
		$arrRegister['auth_id'] 	= substr($decrypted_id,0,-5);

		$user_data 					= $this->general_model->getauthData($arrRegister);
		$user_data['encrypted_id'] 	= $encrypted_id;
		
		if(isset($user_data) && $user_data != '' && !empty($user_data) && ($user_data['forgotten_password_code'] === $encrypted_id))
		{
			

			$data['user_data']     		= $user_data;

			if($this->form_validation->run('reset_password_validation') == FALSE)
			{	
				$this->render_page('distributor/forgot_password',$data);
			}
			else
			{
				sleep(3);

				$auth_id 							= $this->input->post('auth_id');
				$arrData['password'] 				= encrypt_password($this->input->post('c_password'));
				$arrData['forgotten_password_code'] = '';

				$id =  update('id', $auth_id, $arrData, 'distributors');

				if($id)
				{
					log_message('LABLOG','>>setNewPassword >> success ');    
					echo json_encode(array("success"=>true,"message"=>'Password reset success' ,'linkn'=>base_url('login')));	
					exit;
				}

				echo json_encode(array('success'=>false,'message'=>'Some error occured !! .'));
				exit;
			}
		}else{
			redirect('error-page');
		}
	}

	/* DASHBOARD FUNCTIONS STARTS */

	public function dashboard_view()
	{
		$data = array();
	
		$user_role = $this->session->userdata('role');
		/*echo $user_role;
			exit;*/
		if($user_role == AUTH_ROLE_ADMIN)
		{

			$data['distributor_list'] = $this->general_model->distributor_list();
		}
		

		$this->render_page('distributor/dashboard',$data);
	}

	public function approve_distributor()
	{
		$auth_id 				= $this->input->post('auth_id');
		$arrData['status'] 		= STATUS_ACTIVE;
		$id 					= update('id', $auth_id, $arrData, 'distributors');
		$userdata 				= detail_data('*','distributors','id  ='.$auth_id);

		  //User Mail
	    $arrEmailData1                         = array();
	    $arrEmailData1['email_subject']        = (bsnprm_value(BSN_WEBSITE_NAME))?bsnprm_value(BSN_WEBSITE_NAME):PROJECT_NAME.' | Distributor Registration !';
	    $arrEmailData1['email_content']        = '';
	    $arrEmailData1['email_template']       = 'userRegisterApproval';
	    $arrEmailData1['email_template_data']  = $userdata;
	    $arrEmailData1['name']                 = '';
	    $arrEmailData1['email']                = $userdata['email'];

	      //$arrEmailData1['nobcc']        		 = "No";

	    $this->labtron_library->sendEmail($arrEmailData1);

		if($id)
		{
			log_message('LABLOG','>>approve_distributor >> success ');    
			echo json_encode(array("success"=>true,"message"=>'Data updated successfully'));	
			exit;
		}

		echo json_encode(array('success'=>false,'message'=>'Some error occured !! .'));
		exit;
	}

	public function block_distributor()
	{
		$auth_id 				= $this->input->post('auth_id');
		$arrData['status'] 		= STATUS_BLOCKED;

		$id 					=  update('id', $auth_id, $arrData, 'distributors');
		$userdata 				= detail_data('*','distributors','id  ='.$auth_id);

		  //User Mail
	    $arrEmailData1                         = array();
	    $arrEmailData1['email_subject']        = (bsnprm_value(BSN_WEBSITE_NAME))?bsnprm_value(BSN_WEBSITE_NAME):PROJECT_NAME.' | Distributor Account Blocked !';
	    $arrEmailData1['email_content']        = '';
	    $arrEmailData1['email_template']       = 'userRegisterBlocked';
	    $arrEmailData1['email_template_data']  = $userdata;
	    $arrEmailData1['name']                 = '';
	    $arrEmailData1['email']                = $userdata['email'];
	      //$arrEmailData1['nobcc']        		 = "No";

	    $this->labtron_library->sendEmail($arrEmailData1);

		if($id)
		{
			log_message('LABLOG','>>block_distributor >> success ');    
			echo json_encode(array("success"=>true,"message"=>'Data updated successfully'));	
			exit;
		}

		echo json_encode(array('success'=>false,'message'=>'Some error occured !! .'));
		exit;
	}

}
