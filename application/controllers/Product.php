<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends MY_Controller {


	public function product_check()
	{
		$url 				= url_checker('url');
		$last 				= url_checker('last');
		$first_segment 		= url_checker('first');
		$last_category 		= url_checker('category_url');
		$check_section 		= check_section($last);
		$check_category 	= check_category($last_category);
		$check_product 		= check_product($last);

		switch (true) 
		{
			case ($last == 'categories'):
				$this->all_category_list();
				break;
			case ($first_segment == 'catalogs'):
				$this->product_by_catalog_list();
				break;
			case ($last == 'catalog'):
				 redirect('catalogs', 'refresh');
				break;
			case ($first_segment == 'catalog'):
				$this->catalog($last);
				break;
			case ($first_segment == 'compare'):
				$product_skus 		= explode(',', $this->input->get('products'));
				$this->compare_products($product_skus);
				break;
			case ($first_segment == 'search'):
				$product 			=  trim($this->input->get('keyword'));
				$product_section 	=  trim($this->input->get('category_id'));

				if(empty($product))
				{
					$product 		=  trim($this->input->get('product_name_2'));
				}

				$this->search_products($product,$product_section);
				break;
			case ($last == 'products'):
				$this->all_product_list();
				break;
			case ($check_section == 2):
				$this->product_by_section_list($last);
				break;
			case ($check_category == 2):
				$this->product_by_category_list($last_category);
				break;
			case ($check_product == 2):
				$this->product_details($url);
				break;
			
			default:
				redirect('error-page');
				break;
		}
		
	}

	public function all_product_list()
	{
		$data = '';
		$this->render_page('product/allProductList',$data); 
	}

	public function all_category_list()
	{
		$data 						  = '';

		
		$this->render_page('product/allCategoryList',$data);
	}
	
	public function product_by_catalog_list()
	{
		
		$data 							= array();
		/****** COMMON DATA  ****/
		$data 							= common_data();
		/****** COMMON DATA  ****/

		$this->load->view('product/productByCatalogList.php',$data);
	}

	public function get_catalog_products_by_catid()
	{
		$category_id 					= $this->input->post('category_id');
	
		$sdata['category_id']			= $category_id;
		$data 							= $this->categoryList($sdata);

		$data['load_more'] =  $this->load->view('product/productCatalogModel',$data);
	}

	public function product_by_section_list($section_url)
	{
		
		$sdata['section_url']		= $section_url;
		$data			= $this->sectionList($sdata);
		
		
		$this->render_page('product/productBySectionList',$data);
	}
	public function sectionList($sData)
	{
		$section_data			= $this->product_model->get_section('row',$sData);
		$scat['category_section']		= $section_data->id;

		if(isset($section_data) && $section_data !='' && !empty($section_data))
		{

			$data['all_categories'] 	  	 	= $this->product_model->get_category('result_array',$scat);
			$data['section_data']		  = $section_data;

			return $data;
		}
		else
		{
			redirect('error-page');
		}
	}

	public function product_by_category_list($category_url)
	{
		/*echo $category_url;
		exit();*/
		$sdata['category_page_url']		= $category_url;
		$data 							= $this->categoryList($sdata);
		$scat['product_limit'] 		 	= 10;
		$data['random_product'] 	 	= $this->product_model->get_products('result_array',$scat);
		$data['section']         		= $this->uri->segment(1);

		$this->render_page('product/productByCategoryList',$data);
	}

	public function categoryList($sdata)
	{
		$category_data						= $this->product_model->get_category('row_array',$sdata);


		if(isset($category_data) && $category_data !='' && !empty($category_data))
		{
			$highest_level 					= getproductlevel();

			/****** COMMON DATA  ****/
			$data 							= common_data($category_data['meta_title'],$category_data['meta_keyword'],$category_data['meta_description']);
			/****** COMMON DATA  ****/
			 
			/* All category display, left hand side of page */

			/* Get product data*/
			if((empty($category_data['level']) || $category_data['level'] != $highest_level) && !empty($data['all_category'][$category_data['id']]['all_children_ids']))
			{
				$scat['product_category_ids'] 	= $data['all_category'][$category_data['id']]['all_children_ids'];
				$scat['product_orderby']		="sku";
				$data['all_products'] 	  	 	= $this->product_model->get_products('result_array',$scat);
				
			}
			else
			{
				$scat['product_category'] 		= $category_data['id'];
				$scat['product_orderby']		="sku";
				$data['all_products'] 	  		= $this->product_model->get_products('result_array',$scat);

			}
			$data['category_data']		  = $category_data;

			return $data;
		}
		else
		{
			redirect('error-page');
		}
	}

	public function product_details($url)
	{
		
		$data 		= array();
		$length 	= count($url);

		/*check length to get the number of level for product*/

		$produrl 				 = end($url);
		
		$scontent['product_sku'] = $produrl;
		$product_data 	 		 = $this->product_model->get_products('row_array',$scontent);

		/****** COMMON DATA  ****/
		$data 							= common_data($product_data['meta_title'],$product_data['meta_keyword'],$product_data['meta_description']);
		/****** COMMON DATA  ****/

		$data['product_data'] 	 = $product_data;
		$data['product_tab'] 	 = list_data('*', 'product_tabs');
		$scat['category_id']   = $product_data['category_id'];

		$scat['except_id']   		= $product_data['id'];
		$scat['product_category']   = $product_data['category_id'];
		$relate_products    		= $this->product_model->get_products('result_array',$scat);
		
		$data['relate_products']    = $relate_products;
		// $data['relate_products'] 	= implode('| ', array_map(function ($string) { return '<a href="'.base_url().$string['page_url'].'" class="sub-link">'.$string['name'].'</a>';}, $relate_products));
		
		$this->render_page('product/productDetail',$data);
	}

	public function catalog($sku)
	{
		$data 							= array();

		$scontent['product_sku'] 	= $sku;
		$data['product'] 	 		= $this->product_model->get_products('row_array',$scontent);
		$data['product_tab'] 	 	= list_data('*', 'product_tabs');

		$this->render_page('product/catalog', $data);
	}

	public function compare_products($product_ids)
	{
		$data = array();

		$scontent['product_ids'] 	= $product_ids;
		$data['product'] 	 		= $this->product_model->get_products('result_array',$scontent);

		$keys 						= array();
		$p_sku 						= array();
        $uni 						= array();

	     if(isset($data['product']) && $data['product'] != '') 
	     {
	        foreach ($data['product'] as $row) 
	        {
	        	$p_sku[] = $row['sku'];
	            $spec = json_decode($row['specifications'], true);
				if($spec){
	            foreach ($spec as $key => $value) {
	                if (!in_array($key, $uni)) {
	                    $uni[] = $key;
	                }
	            }
			}
				
	        }
   		}
   		if(isset($data['product']) && $data['product'] != '') 
	    {
	        foreach ($data['product'] as $row) 
			{
	            $spec = json_decode($row['specifications'], true);
	            if(isset($uni) && $uni != '') 
	    		{
		            foreach ($uni as $k) 
		            {
		                if (!isset($spec[$k])) {
		                    $keys[$k]['child'][] = "&mdash;";
		                } else {
		                    $keys[$k]['child'][] = $spec[$k];
		                }
		            }
		        }
	        }
	    }
		
		$data['spec_key'] 				= $keys;
		$data['product_sku']			= $p_sku;

		$this->render_page('product/productByCompare', $data);
	}

	public function search_products($product,$product_section)
	{
		$data = array();
		/****** COMMON DATA  ****/
		$data 							= common_data();
		/****** COMMON DATA  ****/

		$data['scontent']['search_product'] 	= $product;
		$data['scontent']['category_section'] 	= $product_section;
		$data['all_products'] 	 	 			= $this->product_model->get_cat_products('result_array',$data['scontent']);

		$this->render_page('product/searchProduct', $data);
	}



}
