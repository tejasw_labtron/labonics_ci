<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends MY_Controller {

	function add_to_cart()
	{ 
		 $row_id 						= 0;
		 $pData 						= $this->cart->contents();
		 $product_id 					= $this->input->post('product_id');
		 $product_qty 					= $this->input->post('product_qty');
		 $sproductData['product_sku'] 	= $product_id;
		 $productDetail 				= $this->product_model->get_products('row_array',$sproductData);
		
		 $product_name 					= $productDetail['name'];
		 $price 						= (int)$productDetail['price'];
		 $product_price 				= (!empty($productDetail['price']) && $price > 0)?$productDetail['price']:0;
		 
		if(isset($pData) && $pData != '')
		{	
			log_message('LABLOG','>>add_to_cart :: if isset');

			foreach($pData as $item) 
			{
			 	 log_message('LABLOG','>>add_to_cart :: for loop ');
		        // check product id , if exist update the quantity
		        if($item['id'] == $product_id)
		        { 
		        	// Set value to your variable
		           $row_id 	= $item['rowid'];
		           $new_qty = $item['qty'] + $product_qty;

		           $data 	=  array(
	            	'rowid'	=> $row_id,
			        'qty'	=> $new_qty );
		            $this->cart->update($data);

		        }
		    }
		}

		log_message('LABLOG','>> add Cart ::  row_id  Exist>> '.$row_id);

		if(empty($row_id)) 
		{
			$data 		= array(
	            'id'	=>  $product_id, 
	            'name' 	=>  $product_name, 
	            'price' => 	$product_price, 
	            'qty' 	=> 	$product_qty, 
	        );
	         $this->cart->insert($data);

			 log_message('LABLOG','>>add_to_cart ::insert ');
			
	     }
	    
      	$cart_count = count($this->cart->contents());
      	$total_amount = $this->cart->format_number($this->cart->total());

      	 log_message('LABLOG','>>add_to_cart ::cart_count '.$cart_count);
      	 log_message('LABLOG','>>add_to_cart ::total_amount '.$total_amount);
	
      	if(!empty($cart_count) && $cart_count !='')
      	{
      		echo json_encode(array("success"=>true,"message"=>$product_name.' updated to cart',"cart_count"=>$cart_count,"total_amount"=>$total_amount));
      	}
      	else
      	{
      		echo json_encode(array("success"=>false,"message"=>'Some LABLOG occured'));
      	}

        
    }

    function update_to_cart()
	{
		$rowid 		= $_POST['rowid'];
		$quantity 	= $_POST['quantity'];
	
		for ($i = 0; $i < $_POST['theValue']; $i++) 
		{
			if(isset($rowid[$i]) && $rowid[$i] !='')
			{
				$data=$this->cart->update(array(
			       'rowid'=>$rowid[$i],
			       'qty'=> $quantity[$i]
			   ));

			    $this->cart->update($data);  
			    log_message('LABLOG','>>updateCart ::  Cart update  ');
			}
		}

      	$cart_count = count($this->cart->contents());
      	$total_amount = $this->cart->format_number($this->cart->total());
      	
      	log_message('LABLOG','>>update_to_cart ::cart_count '.$cart_count);
      	log_message('LABLOG','>>update_to_cart ::total_amount '.$total_amount);
		
		echo json_encode(array("success"=>true,"cart_count"=>$cart_count,"total_amount"=>$total_amount));
	}
 
    function show_cart()
    { 
     	$this->load->view('cart');
    }
 
    function empty_cart()
    { 
       $this->cart->destroy();
       echo json_encode(array("success"=>true,"message"=>'cart updated successfully'));
    }

	function delete_cart()
    { 
        $data 		= array(
            'rowid' => $this->input->post('row_id'), 
            'qty' 	=> 0 
        );
        $result 	= $this->cart->update($data);
        $cart_count = count($this->cart->contents());
        echo json_encode(array("success"=>true,"message"=>'cart updated successfully'));
    }

    function view_cart()
    {
    	$data 							= '';

    	$this->render_page('cart-view',$data);
    }

}
