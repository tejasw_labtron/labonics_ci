<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

	public function index()
	{

		// $scat['product_limit'] 	 		= 10;
		// $data['all_products'] 	 		= $this->product_model->get_products('result_array',$scat);
		// $data['new_products'] 	 		= $this->product_model->get_new_products('result_array');
		$data['banner_data'] 			= banner_data();
		$data['rndm_products'] = $this->general_model->get_random_products();
		
		$this->render_page('home',$data); 

	}

	public function about()
	{

		$data = '';
		$this->render_page('aboutus',$data);
	}

	public function customization()
	{

		$data = '';
		if($this->form_validation->run('customization_validation') == FALSE)
		{	
			$this->render_page('customization',$data);
		}
		else
		{
			sleep(3);
			$arrData = array
			(
		         'contact_person'		=>$this->input->post('contact_person'),
		         'email'		=>$this->input->post('email'),
		         /*'c_web_url'	=>$this->input->post('c_web_url'),*/
		         'phone'		=>$this->input->post('phone'),
		         'company_name'		=>$this->input->post('company_name'),
		         'product_name'	=>$this->input->post('product_name'),
		         'product_specifications'	=>$this->input->post('product_specifications') 
		    );


				//Admin Mail
		      $arrEmailData                         = array();
		      $arrEmailData['email_subject']        = 'Customization Details -'.(bsnprm_value(BSN_WEBSITE_NAME))?bsnprm_value(BSN_WEBSITE_NAME):PROJECT_NAME;
		      $arrEmailData['admin_email_from'] 	= $this->input->post('email');
		      $arrEmailData['email_content']        = '';
		      $arrEmailData['email_template']       = 'adminCustomization';
		      $arrEmailData['email_template_data']  = $arrData;
		      $arrEmailData['name']                 = '';
		      $arrEmailData['email']                = (bsnprm_value(BSN_ADMIN_EMAIL))?bsnprm_value(BSN_ADMIN_EMAIL):ADMIN_EMAIL;
		      $arrEmailData['nobcc']        		= "Yes";

		       //User Mail
		      $arrEmailData1                         = array();
		      $arrEmailData1['email_subject']        = (bsnprm_value(BSN_WEBSITE_NAME))?bsnprm_value(BSN_WEBSITE_NAME):PROJECT_NAME.' | Thankyou for your Enquiry !';
		      $arrEmailData1['email_content']        = '';
		      $arrEmailData1['email_template']       = 'userCustomization';
		      $arrEmailData1['email_template_data']  = '';
		      $arrEmailData1['name']                 = '';
		      $arrEmailData1['email']                = $this->input->post('email');
		      //$arrEmailData1['nobcc']        		 = "No";


		    $this->labtron_library->sendEmail($arrEmailData);
		    $this->labtron_library->sendEmail($arrEmailData1);

		    echo json_encode(array("success"=>true,"message"=>'Your Message was sent successfully, We\'ll get back to you soon' ,'linkn'=>base_url('thank-you')));
		}
	}

	public function sitemap()
	{

		$data = '';
		$this->render_page('sitemap',$data);
	}

	public function contact()
	{
		$data = '';

		if($this->form_validation->run('contact_us_validation') == FALSE)
		{	
			$this->render_page('contactus',$data);
		}
		else
		{
			sleep(3);
			$arrData = array
			(
		         'c_name'		=>$this->input->post('name'),
		         'c_email'		=>$this->input->post('email'),
		         /*'c_web_url'	=>$this->input->post('c_web_url'),*/
		         'c_phone'		=>$this->input->post('phone'),
		         'c_subject'	=>$this->input->post('subject'),
		         'c_message'	=>$this->input->post('message') 
		    );


				//Admin Mail
		      $arrEmailData                         = array();
		      $arrEmailData['email_subject']        = 'Contact Details -'.(bsnprm_value(BSN_WEBSITE_NAME))?bsnprm_value(BSN_WEBSITE_NAME):PROJECT_NAME;
		      $arrEmailData['admin_email_from'] 	= $this->input->post('c_email');
		      $arrEmailData['email_content']        = '';
		      $arrEmailData['email_template']       = 'adminContactUs';
		      $arrEmailData['email_template_data']  = $arrData;
		      $arrEmailData['name']                 = '';
		      $arrEmailData['email']                = (bsnprm_value(BSN_ADMIN_EMAIL))?bsnprm_value(BSN_ADMIN_EMAIL):ADMIN_EMAIL;
		      $arrEmailData['nobcc']        		= "Yes";

		       //User Mail
		      $arrEmailData1                         = array();
		      $arrEmailData1['email_subject']        = (bsnprm_value(BSN_WEBSITE_NAME))?bsnprm_value(BSN_WEBSITE_NAME):PROJECT_NAME.' | Thankyou for your Enquiry !';
		      $arrEmailData1['email_content']        = '';
		      $arrEmailData1['email_template']       = 'userContactUs';
		      $arrEmailData1['email_template_data']  = '';
		      $arrEmailData1['name']                 = '';
		      $arrEmailData1['email']                = $this->input->post('c_email');
		      //$arrEmailData1['nobcc']        		 = "No";


		    $this->labtron_library->sendEmail($arrEmailData);
		    $this->labtron_library->sendEmail($arrEmailData1);

		    echo json_encode(array("success"=>true,"message"=>'Your Message was sent successfully, We\'ll get back to you soon' ,'linkn'=>base_url('thank-you')));
		}
 
	}

	public function getintouch_Data()
	{
		if($this->form_validation->run('get_in_touch_validation') == FALSE)
		{
			$url = $_SERVER['HTTP_REFERER'];
			echo json_encode(array("success"=>false,"message"=>'Something went wrong, Please try again' ,'linkn'=>base_url($url)));
			
	    }
	    else
	    {
	    	sleep(3);
	    	$arrData = array ('c_email' =>$this->input->post('c_email'));

			//Admin Mail
		      $arrEmailData                         = array();
		      $arrEmailData['email_subject']        = 'Contact Details -'.(bsnprm_value(BSN_WEBSITE_NAME))?bsnprm_value(BSN_WEBSITE_NAME):PROJECT_NAME;
		      $arrEmailData['admin_email_from'] 	= $this->input->post('c_email');
		      $arrEmailData['email_content']        = '';
		      $arrEmailData['email_template']       = 'adminGetInTouch';
		      $arrEmailData['email_template_data']  = $arrData;
		      $arrEmailData['name']                 = '';
		      $arrEmailData['email']                = (bsnprm_value(BSN_ADMIN_EMAIL))?bsnprm_value(BSN_ADMIN_EMAIL):ADMIN_EMAIL;
		      $arrEmailData['nobcc']        		= "Yes";

		      $this->labtron_library->sendEmail($arrEmailData);


	    	echo json_encode(array("success"=>true,"message"=>'We will contact you shortly!!' ,'linkn'=>base_url('thank-you')));
	    }
	}

	public function getquote_Data()
	{
		
		if($this->form_validation->run('quote_form_validation') == FALSE)
		{
			$url = $_SERVER['HTTP_REFERER'];
			echo json_encode(array("success"=>false,"message"=>'Something went wrong, Please try again' ,'linkn'=>base_url($url)));
	    }
	    else
	    {
	    	sleep(3);
			$arrData = array(
			         'c_name'		=>$this->input->post('c_name'),
			         'c_email'		=>$this->input->post('c_email'),
			         'c_subject'	=>$this->input->post('c_subject'),
			         'c_product'	=>$this->input->post('c_product'),
			         'c_phone'		=>$this->input->post('c_phone'),
			         'c_message'	=>$this->input->post('c_message')
			         );
			
			//Admin Mail
		      $arrEmailData                         = array();
		      $arrEmailData['email_subject']        = 'Quotation Request - '.(bsnprm_value(BSN_WEBSITE_NAME))?bsnprm_value(BSN_WEBSITE_NAME):PROJECT_NAME;
		      $arrEmailData['admin_email_from'] 	= $this->input->post('c_email');
		      $arrEmailData['email_content']        = '';
		      $arrEmailData['email_template']       = 'adminQuoteForm';
		      $arrEmailData['email_template_data']  = $arrData;
		      $arrEmailData['name']                 = '';
		      $arrEmailData['email']                = (bsnprm_value(BSN_ADMIN_EMAIL))?bsnprm_value(BSN_ADMIN_EMAIL):ADMIN_EMAIL;
		      $arrEmailData['nobcc']        		= "Yes";

		       //User Mail
		      $arrEmailData1                         = array();
		      $arrEmailData1['email_subject']        = (bsnprm_value(BSN_WEBSITE_NAME))?bsnprm_value(BSN_WEBSITE_NAME):PROJECT_NAME.' | Thankyou for your Enquiry ! ';
		      $arrEmailData1['email_content']        = '';
		      $arrEmailData1['email_template']       = 'userQuoteForm';
		      $arrEmailData1['email_template_data']  = '';
		      $arrEmailData1['name']                 = '';
		      $arrEmailData1['email']                = $this->input->post('c_email');
		      //$arrEmailData1['nobcc']        		 = "No";


		     $this->labtron_library->sendEmail($arrEmailData);
		     $this->labtron_library->sendEmail($arrEmailData1);
	  
		     echo json_encode(array("success"=>true,"message"=>'Quote recieved successfuly' ,'linkn'=>base_url('thank-you')));
		}
	}

	public function getallquote_Data()
	{
		
		if($this->form_validation->run('allquote_form_validation') == FALSE)
		{
			$url  			= $_SERVER['HTTP_REFERER'];
			
			echo json_encode(array("success"=>false,"message"=>'Something went wrong, Please try again' ,'linkn'=>base_url($url)));
	    }
	    else
	    {
	    	sleep(3);

	    	$arrData['c_email'] 					= $this->input->post('c_email');
			$arrData['cart'] 	 					= $this->cart->contents();
			
			//Admin Mail
		      $arrEmailData                         = array();
		      $arrEmailData['email_subject']        = 'Quotation Request - '.(bsnprm_value(BSN_WEBSITE_NAME))?bsnprm_value(BSN_WEBSITE_NAME):PROJECT_NAME;
		      $arrEmailData['admin_email_from'] 	= $this->input->post('c_email');
		      $arrEmailData['email_content']        = '';
		      $arrEmailData['email_template']       = 'adminAllQuoteForm';
		      $arrEmailData['email_template_data']  = $arrData;
		      $arrEmailData['name']                 = '';
		      $arrEmailData['email']                = (bsnprm_value(BSN_ADMIN_EMAIL))?bsnprm_value(BSN_ADMIN_EMAIL):ADMIN_EMAIL;
		      //$arrEmailData['nobcc']        		= "Yes";

		       //User Mail
		      $arrEmailData1                         = array();
		      $arrEmailData1['email_subject']        = (bsnprm_value(BSN_WEBSITE_NAME))?bsnprm_value(BSN_WEBSITE_NAME):PROJECT_NAME.' | Thankyou for your Enquiry ! ';
		      $arrEmailData1['email_content']        = '';
		      $arrEmailData1['email_template']       = 'userQuoteForm';
		      $arrEmailData1['email_template_data']  = '';
		      $arrEmailData1['name']                 = '';
		      $arrEmailData1['email']                = $this->input->post('c_email');
		      $arrEmailData1['nobcc']        		 = "No";


		     $this->labtron_library->sendEmail($arrEmailData);
		     $this->labtron_library->sendEmail($arrEmailData1);
	  
		     echo json_encode(array("success"=>true,"message"=>'Quote recieved successfuly' ,'linkn'=>base_url('thank-you')));
		}
	}

	public function error_page_404()
	{
		$data = '';
		
		$this->render_page('error_page',$data);
	}


}
