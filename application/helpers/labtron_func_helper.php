<?php 

    /***********************************************************
			NORMAL CRUD FUNCTIONS STARTS
	************************************************************/

	function update($field, $id, $array, $table)
	{
		$CI 		 = & get_instance();
		$CI->db->where($field, $id);
		$CI->db->update($table, $array);
		
		log_message('ERROR','>>library update >>'.$CI->db->last_query());

		return $id;
	}

	function insert($table, $array)
	{
		$CI 		 = & get_instance();
		$CI->db->insert($table, $array);

		log_message('ERROR','>>library insert >>'.$CI->db->last_query());

		$id = $CI->db->insert_id();
		return $id;
	}

  	function detail_data($fields, $table_name, $where_condition = '', $order_by = '')
    {
    	$CI 		 = & get_instance();
        $sql = 'select ' . $fields . ' from ' . $table_name;
        if ($where_condition) $sql.= ' where ' . $where_condition;
        if ($order_by) $sql.= ' ' . $order_by;
        return $CI->db->query($sql)->row_array();
    }

    function list_data($fields, $table_name, $where_condition = '', $order_by = '')
    {
    	$CI 		 = & get_instance();
        $sql = 'select ' . $fields . ' from ' . $table_name;
        if ($where_condition) $sql.= ' where ' . $where_condition;
        if ($order_by) $sql.= ' ' . $order_by;
        return $CI->db->query($sql)->result_array();
    }

    function field_data($field, $table_name, $id = '')
    {
    	$CI 		 = & get_instance();
        $prefix = explode("_", $field) [0];
        $sql = 'select ' . $field . ' retfield from ' . $table_name . ' where ' . $prefix . '_id = ' . $id;
        return $CI->db->query($sql)->row()->retfield;
    }

	 /***********************************************************
			NORMAL CRUD FUNCTIONS END
	************************************************************/

     /***********************************************************
			ENCRYPTION?DECRYPTION FUNCTIONS STARTS
	************************************************************/

	function encrypt_password($password = '')
	{
		$new_password = openssl_encrypt($password,CIPHER,KEY);
		return $new_password;
	}

	function decrypt_password($password = '')
	{

		$new_password = openssl_decrypt($password,CIPHER,KEY);
		return $new_password;
	}

	function encrypt_key_in_array($data, $key_val_arr)
	{
	    $CI = & get_instance();
	    $key = array_keys($key_val_arr);

	    $x = 'a';

	    for($i = 0; $i < count($data); $i++)
	    {
	      for($j = 0; $j < count($key); $j++)
	      {
	        $data[$i]->{$key_val_arr[$key[$j]]} = $CI->crm_auth->encrypt_openssl($data[$i]->{$key[$j]});
	      }
	    }

	    return $data;
	}

	function base64url_encode($data) { 
		return rtrim(strtr(base64_encode($data), '+/', '-_'), '='); 
	} 

	function base64url_decode($data) { 
	  return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT)); 
	} 
	
	function encrypt_openssl($string)
	 {
		
		$output = false;
		$encrypt_method = "AES-256-CBC";
		$secret_key = KEY; 
		$secret_iv 	= CIPHER;
		// hash
	   $key = hash('sha256', $secret_key);
		// iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
	   $iv = substr(hash('sha256', $secret_iv), 0, 16);
		
			$output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
			$output = base64url_encode($output);
		
		return $output;
	}
	
	function decrypt_openssl($string)
	{
		
		$output = false;
		$encrypt_method = "AES-256-CBC";
		$secret_key = KEY;
		$secret_iv = CIPHER;
		// hash
		$key = hash('sha256', $secret_key);
		// iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
		$iv = substr(hash('sha256', $secret_iv), 0, 16);
		
		$string1 = base64url_decode($string);
		$output = openssl_decrypt($string1, $encrypt_method, $key, 0, $iv);
			
		return $output;
	}

	/***********************************************************
			ENCRYPTION?DECRYPTION FUNCTIONS END
	************************************************************/

	 /***********************************************************
			PROJECTWISE FUNCTIONS START
	************************************************************/

	function ci_breadcrumbs_helper()
    {
    	$CI 		 				= & get_instance();
       	$url 						= $CI->uri->segment_array();
		$total 						= $CI->uri->total_segments();
		$breadcrumbs['Home'] 		= base_url();
		$breadcrumb_link[0] 		= substr_replace(base_url(),"",-1);

		for($i=1;$i<=$total;$i++)
		{
			$breadcrumb_link[$i]=$breadcrumb_link[$i-1]."/".$CI->uri->segment($i);
			$breadcrumbs[str_replace('-',' ',$CI->uri->segment($i))] = $breadcrumb_link[$i];
		}

		
		$crumb_html 			=  '<ol class="breadcrumb">';

		foreach($breadcrumbs as $breadcrumb => $link)
		{ 
			$last_piece 		= end($breadcrumbs);

			if($last_piece !== $link)
				$crumb_html 	.=   '<li class="breadcrumb-item"><a href="'.$link.'">'.ucwords($breadcrumb).'</a> </li> ';
			else

			
				$crumb_html		.=  ' <li  class="breadcrumb-item active">'.ucwords($breadcrumb).'  </li>';
		}

		$crumb_html 			.=  '</ol>';

		return $crumb_html;
    }

    function get_meta($keyword = '')
	{
		$CI 		 = & get_instance();
		if(empty($keyword)) { return NULL; }
		$CI->db->select('pgs.*');
		$CI->db->from('pages pgs');
		$CI->db->where('pgs.name',$keyword);
		$query  = $CI->db->get();
		if($query->num_rows() > 0){
			$response = $query->result_array();
		}
		return $response;
	}

	function bsnprm_value($name = '')
	{
		$CI 		 = & get_instance();	

		if(empty($name)) { return NULL; }

		$CI->db->select('c1.bpm_value as bpm_value');
		$CI->db->from('adm_bsn_prm c1');
		$CI->db->where('c1.bpm_name', $name);
		$query  = $CI->db->get();
		$rows 	= $query->row_array();
		$bpm_value 	= $rows['bpm_value'];
		return $bpm_value;
	}

	function generateRandomOtp($length) 
	{
		$characters = '123456789';
		$randomNo = '';
		for ($i = 0; $i < $length; $i++) {
			$randomNo .= $characters[rand(0, strlen($characters) - 1)];
		}
		return $randomNo;
	}

	function url_checker($type = '')
	{
		$CI 		 						= &get_instance();
		$return_value = '';
		$url 				= $CI->uri->segment_array();
		switch (true) 
		{
			case ($type == 'url'):
				return $return_value = $url; 
				break;
			case ($type == 'first'):
				return $return_value = $CI->uri->segment(1); 
				break;
			case ($type == 'last'):
				return $return_value = end($url); 
				break;
			case ($type == 'category_url'):
			$url_category 		= $CI->uri->uri_string(); 
			$last_category 		= substr($url_category, ($pos = strpos($url_category, '/')) !== false ? $pos + 1 : 0); 
				return $return_value = $last_category; 
				break;
			default:
				return $return_value = '';
				break;
		}
	}
  	
  	function get_pages_content_data($page_url = '')
	{
		$CI 		 = & get_instance();
		
		$sqlQuery 	= 'select * from pages where page_url = "'.$page_url.'" ';
		$query 		= $CI->general_model->executeSqlQuery($sqlQuery,'row_array');
		
		if(isset($query) && $query != '')
		{
			return $query;
		}
		return false;
	}

  	function common_data($meta_title = '',$meta_keyword='',$meta_description = '')
	{
		$CI 		 						= & get_instance();
		$CI->load->model('product_model');
		$data 								= array();
		$scontent['category_level'] 		= 'all';
	    $data['all_category']       		= $CI->product_model->get_category('result_array',$scontent); 
	   	//$data['all_section']       			= $CI->product_model->get_section('result_array');
	   	$data['project_name']       		= (bsnprm_value(BSN_WEBSITE_NAME))?bsnprm_value(BSN_WEBSITE_NAME):PROJECT_NAME;
	    $data['first_segment']     			= url_checker('first');
	    $last_segments     					= url_checker('last');
	    $last_segment     					= (!empty($last_segments))?$last_segments:'home';
	    $data['content']					= array();

	    $content							= get_pages_content_data($last_segment);
	    if(isset($content) && !empty($content) && $content != '')
	    {
	    	$meta_title						= $content['meta_title'];
	    	$meta_keyword					= $content['meta_keyword'];
	    	$meta_description				= $content['meta_description'];
	    	$data['content']				= $content;
	    }

	    $data['meta']						= array();
	    $data['meta']['meta-title']			= (isset($meta_title) && !empty($meta_title))?$meta_title:PROJECT_TITLE;
		$data['meta']['meta-keyword']		= (isset($meta_keyword) && !empty($meta_keyword))?$meta_keyword:PROJECT_KEYWORD;
		$data['meta']['meta-description']	= (isset($meta_description) && !empty($meta_description))?$meta_description:PROJECT_DESCRIPTION;
		$data['user_session']				= array();
		$data['user_session']['id']    		= (!empty($CI->session->userdata('id')))?$CI->session->userdata('id'):'';
		$data['user_session']['name']    	= (!empty($CI->session->userdata('company_person')))?$CI->session->userdata('company_person'):'';
		$data['user_session']['email']    	= (!empty($CI->session->userdata('id')))?$CI->session->userdata('email'):'';
		$data['user_session']['role']    	= (!empty($CI->session->userdata('role')))?$CI->session->userdata('role'):'';
	   	
	    return $data;
	}

	function banner_data()
	{
		$CI 		 = & get_instance();
		
		$sqlQuery 	= 'select * from banners where status = "'.STATUS_ACTIVE.'" order by `order` ';
		$query 		= $CI->general_model->executeSqlQuery($sqlQuery,'result_array');
		
		
		if(isset($query) && $query != '')
		{
			return $query;
		}
		return false;
	}

		/***********************************************************
			PRODUCTWISE FUNCTIONS START
		************************************************************/
		function getproductlevel()
		{
			$CI 		 = & get_instance();

			$CI->db->select('cat.level as level');
			$CI->db->from('categories cat');
			$CI->db->order_by('level','desc');
			$CI->db->limit(1);

			$query  = $CI->db->get();
			$rows 	= $query->row_array();
			
			$level 	= $rows['level'];
			return $level;
		}

	   function check_section($last = '')
		{
			$CI 		 = & get_instance();
				
			if(empty($last)) { return NULL; }
			

			$CI->db->select('sec.page_url');
			$CI->db->from('sections sec');
			$CI->db->where('sec.page_url', $last);
			$query  = $CI->db->get();
			

			$res = 1;
			if($query->num_rows() > 0){
				$res = 2;
			}
			else{
				$res = 1;
			}
			return $res;
		}

		function check_category($last = '')
		{
			$CI 		 = & get_instance();
				
			if(empty($last)) { return NULL; }
			

			$CI->db->select('cat.page_url');
			$CI->db->from('categories cat');
			$CI->db->where('cat.page_url', $last);
			$query  = $CI->db->get();
			

			$res = 1;
			if($query->num_rows() > 0){
				$res = 2;
			}
			else{
				$res = 1;
			}
			return $res;
		}

		function check_product($last = '')
		{
			$CI 		 = & get_instance();
				
			if(empty($last)) { return NULL; }
			

			$CI->db->select('prd.sku');
			$CI->db->from('products prd');
			$CI->db->where('prd.sku', $last);
			$query  = $CI->db->get();
			
			$res = 1;
			if($query->num_rows() > 0){
				$res = 2;
			}
			else{
				$res = 1;
			}
			return $res;
		}

		function feature_html($features)
		{
			if(strpos($features,"<br>")){
                $tag = "<br>";
            }
            else if(strpos($features,"</br>")){
                $tag = "</br>";
            }
            else if(strpos($features,"<br/>")){
                $tag = "<br/>";
            }
            //echo $tag;
            //$features = preg_split('/(<br>)/', $features , -1, PREG_SPLIT_NO_EMPTY); 
            $features = explode($tag, $features);
            return $features;
		}

		function image_url_helper($url,$type)
		{
			$img_src = '';
			$json = str_replace(array("\t","\n"), "",$url);
			$data = json_decode($json,true); 
			
			if ($data) {
				$img_src = $data[$type];
			}

			if(file_exists($img_src))
			{	
			 	$img_src;	
			}
			else
			{
				if($type == 'small')
				{
					$img_src = "assets/images/dummy_image_150x150.png";
				}else{
					$img_src = "assets/images/dummy_image_250x250.png";
				}
				
			}
			return $img_src;
		}

		function check_for_specification_display($variable)
		{
			if(empty($variable)) 
			{ 
				return NULL; 
			}
			elseif(is_object($variable))
			{
				echo "<table class="."table table-bordered displayshow1".">";
				echo "<tbody>";
				foreach($variable as $k => $val){ ?>
					<tr>
					<?php	if(!empty($val))
						{
							echo "<td class='trset1'><strong>".$k."</strong></td>";
							echo "<td>".$val."</td>";
						} ?>
					</tr>
				<?php }	echo "</tbody></table>";
			}
			else 
			{	
				echo $variable;	
			}
		}

		/***********************************************************
			PRODUCTWISE FUNCTIONS END
		************************************************************/

    /***********************************************************
			PROJECTWISE FUNCTIONS END
	************************************************************/
	

	/***
	 * CATEGORY FUNCTIONS
	 */

	function get_cat_section($sec_id){
		$CI 		 = & get_instance();

		$CI->db->select('page_url')->from('sections')->where('id',$sec_id);
		$query = $CI->db->get();
		$response = $query->row();
		return $response->page_url;
	}

	function get_prd_section($cat_id){
		$CI 		 = & get_instance();
		$CI->db->select('sec.page_url')
		->from('categories cat')
		->join('sections sec','sec.id = cat.section_id')
		->where('cat.id',$cat_id)
		->where('sec.status',1)
		->where('cat.status',1);
		$query = $CI->db->get();
		if($query->num_rows() > 0){
		   $section = $query->row();
		}
		return $section->page_url;
		// $CI->db->select('page_url')->from('sections')->where('id',$sec_id);
		// $query = $CI->db->get();
		// $response = $query->row();
		// return $response->page_url;
	}
?>