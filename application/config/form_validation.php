<?php


$config = 
   [
   
      'register_user_validation'       => 
         [
            [
               'field'   => 'company_name', 
               'label'   => 'Company name', 
               'rules'   => 'trim|required'
            ],
            [
               'field'   => 'contact_person', 
               'label'   => 'Contact person', 
               'rules'   => 'trim|required'
            ],

            [
               'field'   => 'email', 
               'label'   => 'Email Address', 
               'rules'   => 'trim|required|valid_email|is_unique[distributors.email]'
            ],

            [
               'field'   => 'password', 
               'label'   => 'Password', 
               'rules'   => 'trim|required'

            ],

            [
               'field'   => 'contact_number', 
               'label'   => 'Contact number', 
               'rules'   => 'trim|numeric'
            ],

            [
               'field'   => 'country', 
               'label'   => 'Country', 
               'rules'   => 'trim|required'
            ],
         ],

      'login_user_validation'          => 
         [
               [
                  'field'   => 'email', 
                  'label'   => 'Email', 
                  'rules'   => 'trim|required'
               ],

               [
                  'field'   => 'password', 
                  'label'   => 'Password', 
                  'rules'   => 'trim|required'

               ],
         ],

      'user_edit_validation'       => 
         [
            [
               'field'   => 'c_name', 
               'label'   => 'Company name', 
               'rules'   => 'trim|required'
            ],
            [
               'field'   => 'c_person', 
               'label'   => 'Contact person', 
               'rules'   => 'trim|required'
            ],
            [
               'field'   => 'c_phones', 
               'label'   => 'Contact number', 
               'rules'   => 'trim|numeric'
            ],
            [
               'field'   => 'c_location', 
               'label'   => 'Location', 
               'rules'   => 'trim|required'
            ],
         ],

      'email_verification_validation'  => 
         [
               [
                  'field'   => 'usr_username', 
                  'label'   => 'User name', 
                  'rules'   => 'trim|required'
               ]
         ],

      'reset_password_validation'      => 
         [
               [
                  'field'   => 'c_password', 
                  'label'   => 'Password', 
                  'rules'   => 'trim|required'
               ]
         ],

      'customization_validation'       =>
         [
            [
               'field'   => 'company_name', 
               'label'   => 'Company name', 
               'rules'   => 'trim|required'
            ],
            [
               'field'   => 'phone', 
               'label'   => 'Phone', 
               'rules'   => 'trim|required'
            ],

            [
               'field'   => 'email', 
               'label'   => 'Email', 
               'rules'   => 'trim|required|valid_email'
            ]
         ],
      'contact_us_validation'          => 
         [
            [
               'field'   => 'name', 
               'label'   => 'Contact name', 
               'rules'   => 'trim|required'
            ],
            [
               'field'   => 'message', 
               'label'   => 'Your Message', 
               'rules'   => 'trim|required'
            ],

            [
               'field'   => 'email', 
               'label'   => 'Email Address', 
               'rules'   => 'trim|required|valid_email'
            ]
         ],

      'get_in_touch_validation'        => 
         [
            [
               'field'   => 'c_email', 
               'label'   => 'Email Address', 
               'rules'   => 'trim|required|valid_email'
            ]
         ],
         
      'quote_form_validation'          => 
         [
            [
               'field'   => 'c_name', 
               'label'   => 'Contact name', 
               'rules'   => 'trim|required'
            ],
            [
               'field'   => 'c_email', 
               'label'   => 'Email Address', 
               'rules'   => 'trim|required|valid_email'
            ],

            [
               'field'   => 'c_message', 
               'label'   => 'Message', 
               'rules'   => 'trim|required'

            ],

            [
               'field'   => 'c_phones', 
               'label'   => 'Contact number', 
               'rules'   => 'trim|numeric'
            ],
         ],

      'allquote_form_validation'          => 
         [
            [
               'field'   => 'c_email', 
               'label'   => 'Email Address', 
               'rules'   => 'trim|required|valid_email'
            ],
         ]

   ];


?>