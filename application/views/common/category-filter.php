<!--small product area start-->
<div class="small_product_area mb-68 br-1">
    <div class="clr-u-1">
        <h4>Product Categories</h4>
    </div>
    <div class="clr-u">
        <h4 >All Categories</h4>
    </div>
    <div class="small_product_container ">
        <?php $i = 1;
            foreach($main_cat as $cat){
                $section = get_cat_section($cat['section_id']);
                if($i <= count($main_cat)) {
        ?>
            <div class="small_product_list">
                <aside class="sidebar_widget">
                    <div class="widget_inner">
                    <div class="widget_list widget_categories">
                        <ul class="clr-2">
                            <li class="widget_sub_categories sub_categories<?=$i?>"><a href="javascript:void(0)"><?= $cat['name']?></a>
                                <ul class="widget_dropdown_categories dropdown_categories<?=$i?>" >
                                    <?php 
                                        $this->db->select('*')->from('categories')->order_by('name','ASC')->where('parent_id',$cat['id'])->where('status',1)->where('level',1);
                                        $query = $this->db->get();
                                        $response = array();
                                        if($query->num_rows() > 0){
                                        $level_one = $query->result_array();
                                        }	
                                        $j = 0;
                                        if($j <= count($level_one)) {
                                            if($cat['id'] == $level_one["$j"]['parent_id']){    
                                                foreach($level_one as $sub_cat){
                                    ?>
                                        <li><a href="<?= base_url()?><?= $section?>/<?= $sub_cat['page_url']?>"><?= $sub_cat['name']?></a></li>
                                            <?php 
                                                        }
                                                    }else{
                                            ?>
                                    
                                                <li><a href="<?= base_url()?><?= $section?>/<?= $cat['page_url']?>"><?= $cat['name']?></a></li>
                                    <?php 
                                                    }
                                        $j++;
                                        }
                                    ?>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </aside>
            </div>
        <?php 
            }
            $i++;
        }
        ?>
    </div>
</div>
<!--small product area end-->
