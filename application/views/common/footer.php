<!--footer area start-->
<footer class="footer_widgets foote_three">
  <div class="footer_top">
    <div class="container">
      <div class="row">
        <div class="col-lg-4 col-md-6 col-sm-7">
          <div class="widgets_container contact_us">
            <div class="footer_logo"> <a href="<?php echo base_url('home')?>"><img src="<?php echo base_url()?>assets/img/logo/logo.png" alt="logo"></a> </div>
            <p>Labonics is a leading manufacturer of Biosafety Cabinets, Fume Hoods, Cleanroom Equipment, Laminar Flow Cabinets and other laboratory furniture for Research, Medical and Industrial laboratories across the globe.</p>
            <div class="footer_social">
              <h3>Follow Us</h3>
              <ul>
                <li class="facebook"><a href="https://www.facebook.com/labonicsuk/?ref=bookmarks"><i class="fa fa-facebook"></i> </a></li>
                <li class="twitter"><a href="https://twitter.com/Labonics2"><i class="fa fa-twitter"></i> </a></li>
                <li class="pinterest"><a href="https://in.pinterest.com/labonicssystem/"><i class="fa fa-pinterest-p"></i> </a></li>
                <li class="Quora"><a href="https://www.quora.com/profile/Labonics-System"><i class="fa fa-quora"></i> </a></li>
                <li class="linkedin"><a href="https://www.linkedin.com/authwall?trk=gf&trkInfo=AQEfYf3pGq_qtgAAAXbWd8RwUVjAZ6TsG7E0cgC6_LfqVUvaYSQ3khWKzEuPJX30Jxilagjvn8g8GvU-Ogj4Se3AQ5svYKD16iZ9q60M326bYEDbbxr9qTiGDpR_QSgz5tBzXdM=&originalReferer=https://www.labonics.com/&sessionRedirect=https%3A%2F%2Fwww.linkedin.com%2Fin%2Flabonics-system-21a530172%2F"><i class="fa fa-linkedin"></i></a></li>
                <li class="Tumblr"><a href="https://www.tumblr.com/login?redirect_to=%2Fblog%2Fview%2Flabonics"><i class="fa fa-tumblr"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1"> 
        </div>
        <div class="col-lg-3 col-md-6 col-sm-5">
          <div class="widgets_container contact_us ">
            <h3>Product</h3>
            <div class="footer_menu">
              <ul>

                <?php $k = 0; foreach($main_section as $res){ ?>

                  <li><a href="<?php echo base_url()?><?php echo $res['page_url']?>" title="<?php echo $res['name']?>" ><?php echo $res['name']?></a></li>

                <?php if($k++ == 5) break; } ?>

              </ul>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-5">
          <div class="widgets_container contact_us">
            <h3>Address</h3>
            <p class="contact_info"><i class="fa fa-map-marker"></i> 128 Leucha road Walthamstow <br>
              <span> London E17 7lQ UK</span></p>
            <p><span> <i class="fa fa-envelope-o"></i> </span> <a href="mailto:info@labonics.com">info@labonics.com</a></p>
            <p><span><i class="fa fa-globe"></i></span> <a href="https://www.labonics.com/" target="_blank">www.labonics.com</a></p>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2479.3235690995784!2d-0.038449184688602936!3d51.58063291316774!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48761dc863a73b25%3A0x5b8f1bb44666cf84!2s128%20Leucha%20Rd%2C%20Walthamstow%2C%20London%20E17%207LQ%2C%20UK!5e0!3m2!1sen!2sin!4v1588235132242!5m2!1sen!2sin"
					width="100%" height="150px" frameborder="0" title="Labtron location"

					style="border: 5px solid #ffffff; border-radius: 6px;" allowfullscreen="">
            </iframe>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="footer_link">
    <div class="container">
      <div class="row text-center">
        <div class="col-lg-12 col-md-12 col-sm-6">
          <ul>
            <li><a href="<?php echo base_url('home')?>">Home</a></li>
            <li><a href="<?php echo base_url('about-us')?>">About Us</a></li>
            <li><a href="<?php echo base_url('contact-us')?>">Contact Us</a></li>
            <li><a href="<?php echo base_url('customization')?>">Customization</a></li>
            <li><a href="<?php echo base_url('sitemap')?>">Sitemap</a></li>
         
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="footer_bottom">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-lg-12 col-md-6">
          <div class="copyright_area text-center">
            <p>© Copyright by 2018 <a href="<?php echo base_url('home')?>">labonics.com</a> All Right Reserved </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>
<!--footer area end--> 

    <script type="text/javascript">
      var base_url = '<?php echo base_url()?>';
    </script> 
      <!-- Scripts -->
      <!-- <script src="{{ asset('js/app.js') }}" defer></script> -->
      <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
      <script src="<?php echo base_url();?>assets/js/plugins.js" ></script>
      <script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>

      <script src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>
      <script src="<?php echo base_url();?>assets/js/main.js" ></script>

      <script src="<?php echo base_url();?>assets/js/custom_main.js" ></script>
      <!--Toaster js -->
      <script src="<?php echo base_url();?>assets/vendor/laravel-admin/toastr/build/toastr.min.js"></script>
      <script src="<?php echo base_url();?>assets/vendor/laravel-admin/sweetalert2/dist/sweetalert2.min.js"></script>
      <script>
        $( document ).ready(function() {
          var activeurl = window.location;
          var baseurl = '<?php echo base_url()?>';

          if(activeurl != baseurl){
              $('a[href="'+activeurl+'"]').addClass('active');
              $('a[href="'+baseurl+'"]').removeClass('active');
          }
          
        });

      </script>

   </body>
</html>