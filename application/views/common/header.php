<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title><?php echo $meta['meta-title']; ?></title>    
      <meta name="description" content="<?php echo $meta['meta-description']; ?>"/>
      <meta name="keywords" content="<?php echo $meta['meta-keyword']; ?>"/> 
      <!-- CSRF Token -->
      <!-- <meta name="csrf-token" content="{{ csrf_token() }}"> -->

      <!-- Favicon -->
      <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>assets/img/favicon.ico">



      <!-- Fonts -->
      <!-- <link rel="dns-prefetch" href="//fonts.gstatic.com"> -->
      <!-- <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet"> -->

      <!-- Styles -->
      <link href="<?php echo base_url();?>assets/css/plugins.css" rel="stylesheet">
      <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
      <link href="<?php echo base_url();?>assets/css/demo.css" rel="stylesheet">
      <!-- toast CSS -->
      <link href="<?php echo base_url();?>assets/vendor/laravel-admin/toastr/build/toastr.min.css" rel="stylesheet">
      <link href="<?php echo base_url();?>assets/vendor/laravel-admin/sweetalert2/dist/sweetalert2.css" rel="stylesheet">
      
   </head>
   <body>
      
      <div class="main_header">
         <div class="header_top header_top_three">
            <div class="container">
               <div class="row text-white "> 
                  <div class="col-lg-6 col-md-6 text-left"> 
                     <p><span> <i class="fa fa-envelope-o"></i> </span> <a href="mailto:<?php echo bsnprm_value(BSN_WEBSITE_EMAIL_ADDRESS) ?>"><?php echo bsnprm_value(BSN_WEBSITE_EMAIL_ADDRESS) ?></a></p>
                  </div>
                  <div class="col-lg-6 col-md-6 text-right"> 
                     <p><span><i class="fa fa-user"></i></span> 
                        <?php if($user_session['id'] && $user_session['role'] == AUTH_ROLE_ADMIN) { ?>
                           <a href="<?php echo base_url('distributor-dashboard')?>">Dashboard</a>
                           <span class="br">|</span>
                           <a href="<?php echo base_url('logout')?>">Logout</a>
                           <?php }elseif($user_session['id'] && $user_session['role'] != AUTH_ROLE_ADMIN) { 
                                 $user_id = encrypt_openssl($user_session['id']);?>
                           <a href="<?php echo base_url('user-profile-'.$user_id)?>">User profile</a>
                           <span class="br">|</span>
                           <a href="<?php echo base_url('logout')?>">Logout</a>
                           <?php }else{ ?>
                           <a href="<?php echo base_url('register')?>">Register</a>
                           <span class="br">|</span>
                           <a href="<?php echo base_url('login')?>">Sign In</a>
                        <?php } ?>
                        
                     <!-- <a href="#">Distributor Login</a> -->
                     </p>  
                  </div>
               </div>   
                  
            </div>
         </div>
      </div>
      <!--mobile header start-->
      <div class="off_canvars_overlay"> </div>
      <div class="Offcanvas_menu Offcanvas_three">
         <div class="container">
            <div class="row">
               <div class="col-12">
               <div class="canvas_open"> <a href="javascript:void(0)"><i class="ion-navicon"></i></a> </div>
               <div class="Offcanvas_menu_wrapper">
                  <div class="canvas_close"> <a href="javascript:void(0)"><i class="ion-android-close"></i></a> </div>
                  <div id="menu" class="text-left">
                     <ul class="offcanvas_main_menu">
                     <li class=" active"> <a href="<?php echo base_url('home')?>">Home</a> </li>
                     <li > <a href="<?php echo base_url('about-us')?>">About Us</a> </li>
                     <li class="menu-item-has-children"> <a href="<?php echo base_url('products')?>">All Products</a>
                        <?php if(isset($main_cat) && $main_cat!=''){ ?>      
                        <ul class="sub-menu">
                        <?php  foreach($main_cat as $res){ ?>
                           <li>
                              <a title="<?php echo $res['name']?>" href="<?php echo base_url()?><?php echo $res['page_url']?>"><span itemprop="name"><span itemprop="name"><?php echo $res['name']?></span></span></a>
                           </li>
                        <?php } ?>
                        </ul>
                        <?php } ?>

                     </li>
                     <li> <a href="<?php echo base_url('categories')?>">Product Category</a> </li>
                     <li ><a href="<?php echo base_url('customization')?>">Customization</a></li>
                        <?php if($user_session['id'] && $user_session['role'] == AUTH_ROLE_ADMIN) { ?>
                           <li><a href="<?php echo base_url('distributor-dashboard')?>">Dashboard</a></li>
                           <li><a href="<?php echo base_url('logout')?>">Logout</a></li>
                           <?php }elseif($user_session['id'] && $user_session['role'] != AUTH_ROLE_ADMIN) { 
                                 $user_id = encrypt_openssl($user_session['id']);?>
                           <li><a href="<?php echo base_url('user-profile-'.$user_id)?>">User profile</a></li>
                           <li><a href="<?php echo base_url('logout')?>">Logout</a></li>
                           <?php }else{ ?>
                           <li><a href="<?php echo base_url('register')?>">Register</a></li>
                           <li><a href="<?php echo base_url('login')?>">Sign In</a></li>
                        <?php } ?>
                     <li> <a href="<?php echo base_url('contact-us')?>">Contact Us</a> </li>
                     </ul>
               
                  </div>
                  <div class="Offcanvas_footer"> <span><a href="mailto:<?php echo bsnprm_value(BSN_WEBSITE_EMAIL_ADDRESS) ?>"><i class="fa fa-envelope-o"></i><?php echo bsnprm_value(BSN_WEBSITE_EMAIL_ADDRESS) ?></a></span>
                  </div>
               </div>
               </div>
            </div>
         </div>
      </div>
      <!--mobile header end-->

      <header>
         <div class="main_header"> 
            <div class="header_middle">
               <div class="container">
                  <div class="row align-items-center">
                     <div class="col-lg-3">
                        <div class="logo"> <a href="<?php echo base_url('home')?>"><img src="<?php echo base_url();?>assets/img/logo/logo.png" alt="logo"></a> </div>
                     </div>
                     <div class="col-lg-8">
                        <div class="heder_middle_right">
                        <div class="search-container">
                           <form method="GET" action="<?php echo base_url('search')?>">
                              <div class="hover_category">
                              <?php if(isset($main_cat) && $main_cat!=''){ ?>   
                              <select class="select_option" name="category_id" id="categories">
                                 <option selected value="">All Categories</option>
                                 <?php  foreach($main_cat as $res){ ?>

                                 <option value="<?php echo $res['id']?>"><?php echo $res['name']?></option>
                                 <?php } ?>

                              </select>  
                              <?php } ?>
                                 
                              </div>
                              <div class="search_box_three">
                              <input id="keyword" name="keyword"  placeholder="Search product..." type="text" required>
                              <button type="submit"><i class="zmdi zmdi-search zmdi-hc-fw"></i></button>
                              </div>
                           </form>
                        </div>
                        </div>
                     </div>
                     <div class="col-lg-1">
                        <div class="heder_middle_right">
                           <div class="mini_cart_wrapper mini_cart_three">
                              <ul class="mr-3">
                                 <li class="mini_cart_wrapper cart-1"><a href="javascript:void(0)" class="minicart-icon"><i class="fa fa-shopping-bag fx-3"></i> <span class="lab_cart_count item_count"><?php echo count($this->cart->contents()); ?></span></a>
                                    <div class=" cart-dropdown mini_cart" style="max-height: 482px;">

                                    </div>
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            
            <div class="header_bottom sticky-header">
               <div class="container">
               <div class="row align-items-center">
                     <div class="main_menu menu_three menu_position" align="center">
                     <nav>
                        <ul>
                           <li><a class="active"  href="<?php echo site_url(); ?>">Home</a></li>
                           <li><a href="<?php echo site_url('about-us'); ?>">About Us</a></li>
                           <li><a href="<?php echo site_url('products'); ?>">All Products</a></li>
                           <li class="mega_items"><a href="<?php echo site_url('categories'); ?>">Product Category<i class="fa fa-angle-down"></i></a>
                              <div class="mega_menu">
                                 <ul class="mega_menu_inner">
                                    <?php if(isset($main_cat) && $main_cat!=''){ 
                                       foreach(array_chunk($main_cat,10) as $chunk){
                                       ?>
                                       <li style="padding-right: 16px;">
                                       <ul>
                                          <?php 
                                             foreach($chunk as $cat){
                                             $section = get_cat_section($cat['section_id']);
                                          ?>
                                          <li><a href="<?php echo base_url()?><?php echo $section?>/<?php echo $cat['page_url']?>"><?php echo $cat['name']?></a></li>
                                          <?php 
                                             }
                                          ?>                          
                                       </ul>
                                       
                                       </li>
                                       <?php 
                                       }
                                    }
                                       ?> 
                                 </ul>
                              </div>
                           </li>
                           <li ><a href="<?php echo site_url('customization'); ?>">Customization</a></li>
                           <li><a href="<?php echo site_url('contact-us'); ?>">Contact Us</a></li>
                        </ul>
                     </nav>
                     </div>
                    
               </div>
               </div>
            </div>
         </div>
      </header>
      <!--header area end--> 