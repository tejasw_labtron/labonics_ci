<?php
   $this->load->view('common/breadcrumb',['current' => 'cart view']);
?>
   <!--shopping cart area start -->
   <div class="shopping_cart_area mt-60">
      <div class="container">  
         <h3 class="page-title text-center" style="margin-bottom:25px;">Shopping cart</h3>
            <?php 
               $session_user_id = $this->session->userdata('id'); 
               $cart_data = $this->cart->contents();
               if(isset($cart_data) && $cart_data != '' && !empty($cart_data)){
            ?>
               <div class="row">
                  <div class="col-12">
                     <div class="table_desc">
                        <div class="cart_page table-responsive">
                           <table>
                              <thead>
                                 <tr>
                                    <th class="product_thumb">Image</th>
                                    <th class="product_name">Product</th>
                                    <?php if(!empty($session_user_id) && $session_user_id != ''){  ?>
                                    <th>Price</th>
                                    <?php } ?>
                                    <th class="product_quantity">Quantity</th> 
                                    <?php if(!empty($session_user_id) && $session_user_id != ''){  ?>
                                    <th>Total</th>
                                    <?php } ?>
                                    <th class="product-price">	Custom Requirement</th>
                                    <th class="product_remove">Delete</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <?php 
                                    $count = 0;
                                    foreach ($cart_data as $items) {

                                       $sproductData['product_sku']= $items['id'];
                                       $productDetail              = $this->product_model->get_products('row_array',$sproductData);
                                       $image_url                  =  image_url_helper($productDetail['image_url'],'small');
                                       $section = get_prd_section($productDetail['category_id']);
                                       $product_count              = count($cart_data) + 1;
                                       $count++;
                                 ?>
                                    <tr>
                                       <input type="hidden" id="rowid<?php echo $count?>" name="rowid<?php echo $count?>" class="form-control" value="<?php echo $items["rowid"] ;?>">
                                       <input type="hidden" name="theValue" id="theValue" value="<?php echo $product_count; ?>">
                                       <td class="product_thumb">
                                       <a href="<?php echo base_url()?><?= $section?>/<?php echo $productDetail['page_url']?>"><img  width="100px" height="100px" src="<?php echo base_url().$image_url ?>" alt="<?php echo $productDetail['name']; ?>" /></a>
                                       </td>
                                       <td class="product_name">
                                       <h5><a href="<?php echo base_url()?><?= $section?>/<?php echo $productDetail['page_url']?>"><?php echo $items['name'];  ?></a></h5>
                                       </td>
                                       <?php if(!empty($session_user_id) && $session_user_id != ''){  ?>
                                       <td>
                                          <div class="cart-product-price">
                                             $<?php echo $this->cart->format_number($items['price']); ?>   
                                       
                                          </div>
                                       </td>
                                       <?php } ?>
                                       <td class="product_quantity">
                                       <input type="text"  type="number" class="form-control input-text qty"  min=0 name="quantity<?php echo $count?>" value="<?php echo $items['qty'] ?>" id="quantity<?php echo $count?>">
                                       </td>
                                       <?php 
                                          if(!empty($session_user_id) && $session_user_id != ''){  ?>
                                          <td>
                                          <div class="cart-product-price">
                                             <span>  $<?php echo $this->cart->format_number($items['subtotal']); ?> </span>
                                          </div>
                                          </td>
                                       <?php } ?>
                                       <td class="custom_product"><textarea class="form-control form-mobile-cart" rows="10" style="font-size:13px;" placeholder="List Your Requirement" name="requirement[739da446d9181d0de8abcd6c2938c597]" id="requirement[739da446d9181d0de8abcd6c2938c597]"></textarea></td>
                                       <td class="product_remove">
                                          <a class="remove-from-cart" data-productrowid="<?php echo $items['rowid'];?>"><i class="fa fa-trash-o"  title="delete from cart"></i></a>
                                       </td>
                                    </tr>
                                 <?php } ?>
                              </tbody>
                           </table>   
                        </div>  
                        <div class="cart_submit">
                           <a href="<?php echo base_url('products') ?>"><button>Continue Shopping </button></a> 
                           <a href="javascript:void(0);" data-toggle="modal" data-target="#getAllQuote" class="btn-common"><button>Get Quote</button></a>
                           <a href="javascript:void(0);" class="empty-cart"><button>empty cart</button>
                           <a href="javascript:void(0);" class="update-to-cart"><button>UPDATE CART</button></a>
                        </div>    
                     </div>
                  </div>
               </div>
            <?php 
               }else{ ?> 
                <center><h2 class="mb-60"> Oops!!! your cart is empty!!</h2></center>
            <?php }  ?>
      </div>     
   </div>
   <!--shopping cart area end -->

   <div class="modal fade" id="getAllQuote" role="dialog">
      <div class="modal-dialog">
         <!-- Modal content-->
         <div class="modal-content">
            <div class="modal-header">
               <h4 class="modal-title">Please enter your email address</h4>
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               
            </div>
            <div class="modal-body">
               <div class="auth-box card">
               <div class="card-body">
                     <form id="getallquote_form" method="post">
                        <div class="col-lg-12 col-md-12">
                        <div class="single-input-item">
                           <input class="form-control" type="email" id="c_email" name="c_email" placeholder="Email" required="">
                        </div>
                        </div>
                        <br>
                        <div class="col-lg-6 col-md-12">
                        <div class="single-input-item">
                           <button class="btn btn-primary btn__bg" type="submit" id="form_submit_reg">Send Message</button>
                        </div>
                        </div>
                  </form>
                  </div>
               </div>
            </div>
            <div class="modal-footer">
               <button type="button"  class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
         </div>
      </div>
   </div>