<?php
   $this->load->view('common/breadcrumb',['current' => 'all products']);
?>
   <!--products section area-->
   <div class="unlimited_services">
      <div class="container">  
         <div class="masonry">
            <?php 
               foreach(array_chunk($main_cat,3) as $chunk){
                  foreach($chunk as $cat){
                    $section = get_cat_section($cat['section_id']);
            ?>
               <div class="item">
                  <div class="bg-clr">
                     <h5 class="clr-1"><a href="<?php echo base_url().$section.'/'.$cat['page_url']?>"><?php echo $cat['name']?></a></h5>
                     <ul class="abt-li">
                        <?php 
                           $this->db->select('*')->from('categories')->order_by('name','ASC')->where('parent_id',$cat['id'])->where('status',1)->where('level',1);
                           $query = $this->db->get();
                           if($query->num_rows() > 0){
                              $level_one = $query->result_array();
                           }	                             
                           if(isset($level_one) && $level_one !='')
                              foreach($level_one as $sub_cat){
                                 if($cat['id'] == $sub_cat['parent_id']){
                        ?>
                           <li><i class="fa fa-arrow-circle-right"></i><a href="<?php echo base_url().$section.'/'.$sub_cat['page_url']?>"><?php echo $sub_cat['name']?></a></li>
                           <ul class="abt-li2">
                              <?php 
                              
                                 $this->db->select('*')->from('categories')->order_by('name','ASC')->where('parent_id',$sub_cat['id'])->where('status',1)->where('level',2);
                                 $query2 = $this->db->get();
                                 if($query2->num_rows() > 0){
                                    $level_two = $query2->result_array();
                                 }	
                                 if(isset($level_two) && $level_two !='')
                                    foreach($level_two as $sub_cat2){
                                       if($sub_cat['id'] == $sub_cat2['parent_id']){
                              ?>
                              <li><i class="fa fa-caret-right"></i><a href="<?php echo base_url().$section.'/'.$sub_cat2['page_url']?>"><?php echo $sub_cat2['name']?></a></li>
                              <?php 
                                    }
                                 }
                                 
                              ?>  
                           </ul>   
                        <?php 
                              }
                           }
                        ?>  
                     </ul>     
                  </div>
               </div>             

            <?php 
            }}
            ?>
                              
         </div>
      </div>     
   </div> 
   <!--products section end--> 