<!-- Modal Header -->
 <div class="modal-header">
    <h4 class="modal-title"><?php echo $category_data['name'] ?></h4>
    <button type="button" class="close" data-dismiss="modal">&times;</button>
 </div>

<!-- Modal body -->
         <div class="modal-body">
               <div class="row">
                 <?php if(isset($all_products) && $all_products !='' && !empty($all_products)){ ?>
                  <div class="col-lg-12">
                     <div class="row">
                      <?php foreach($all_products as $allproduct){ 

                       $image_url =  image_url_helper($allproduct['image_url'],'small');

                        ?>
                        <!-- Grid column -->
                        <div class="col-lg-4 col-md-12">
                           <!--Panel-->
                           <div class="card card-body catalog-modal-body">
                              <h4 class="card-title catalog-title-modal">
                                 <a  href="<?php echo base_url();?>catalog/<?php echo $allproduct['sku'];?>" target="_blank" class="link-dwn"><i class="fa fa-download dwn" aria-hidden="true"></i><?php echo $allproduct['name']; ?></a>
                              </h4>
                           </div>
                           <!--/.Panel-->
                        </div>
                        <!-- Grid column -->
                        <?php } ?>
                     </div>
                  </div>
                   <?php } else { ?> 

                      <p> Please email us <i class="fa fa-envelope-o"></i> <?php echo bsnprm_value(BSN_WEBSITE_EMAIL_ADDRESS) ?> or contact us <i class="fa fa-phone"></i> <?php echo bsnprm_value(BSN_WEBSITE_PHONE_NUMBER) ?> for these catalogs</p>
                     <?php } ?>

               </div>
            </div>