<?php
   $this->load->view('common/breadcrumb',['current' => '']);
?>
   <!-- Product Details start-->   
   <div class="home_section_two color_two mb-60 mt-30">
      <div class="container">
         <div class="row">
            <?php $image_url =  image_url_helper($product_data['image_url'],'medium'); ?>
               <div class="col-lg-4 col-md-8 ">
                  <img style="margin-left: 7%;min-height: 80%;" src="<?php echo base_url().$image_url ?>" alt="<?= $product_data['name']?>" />
               </div>
               <div class="col-lg-8 col-md-12">
                  <div class="widget_list tags_widget">
                     <h4><?= $product_data['name']?></h4>
                     <p><?= $product_data['description']?></p>
                     <?php 
                        if(!empty($this->session->userdata('id')) && $this->session->userdata('id') != '' && !empty($product_data['price'])){  ?>
                        <div class="product-price-rating">
                          <strong><span>Price: $<?php echo $product_data['price']; ?></span></strong> 
                        </div>
                     <?php } ?>
                     <div class="tag_cloud3 text-center btn-1">
                        <a href="#" class="add-to-carts add-to-cart" data-productid="<?php echo $product_data['sku'];?>" data-productqty=1>Add to Cart</a>
                        <a href="#" data-toggle="modal" data-target="#getQuote" title="Get quote">Get Quote</a>                      
                        <a target="_blank" href="<?php echo base_url();?>catalog/<?php echo $product_data['sku'];?>">Generate Catalog</a>
                     </div>
                  </div>
               </div>
            </div>
        </div>
    </div>
    <!-- Product Details end-->

    <!-- tab section start--> 
    <div class="product_d_info mb-60">
        <div class="container">   
            <div class="row">
                <div class="col-12">
                    <div class="product_d_inner">   
                        <div class="product_info_button">    
                            <ul class="nav" role="tablist">
                                <li >
                                    <a class="active" data-toggle="tab" href="#Specifications" role="tab" aria-controls="Specifications" aria-selected="false">Specifications</a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#Features" role="tab" aria-controls="Features" aria-selected="false">Features</a>
                                </li>
                                <?php if(!empty($product_data['applications']))		{	?> 
                                <li>    
                                    <a data-toggle="tab" href="#Applications" role="tab" aria-controls="Applications" aria-selected="false">Applications</a> 
                                </li>
                                 <?php } ?>
                                <?php if(!empty($product_data['optional_filter']))		{	?>
                                <li>    
                                    <a data-toggle="tab" href="#optional_filter" role="tab" aria-controls="optional_filter" aria-selected="false">Optional Filter</a> 
                                </li>
                                <?php } ?>
                                <?php if(!empty($product_data['standard_filter']))		{	?> 
                                <li>    
                                    <a data-toggle="tab" href="#standard_filter" role="tab" aria-controls="standard_filter" aria-selected="false">Standard Filter</a> 
                                </li>
                                <?php } ?>
                                <?php if(!empty($product_data['standard_accessories']))		{	?> 
                                <li>    
                                    <a data-toggle="tab" href="#accessories" role="tab" aria-controls="accessories" aria-selected="false">Standard Accessories</a> 
                                </li>
                                <?php } ?>				
                                <?php if(!empty($product_data['optional_accessories']))		{	?> 
                                <li>    
                                    <a data-toggle="tab" href="#accessories_opt" role="tab" aria-controls="accessories_opt" aria-selected="false">Optional Accessories</a> 
                                </li>
                                <?php } ?>
                                
                                <?php if(!empty($product_data['fully_automated_cargo_shower']))		{	?> 
                                <li>    
                                    <a data-toggle="tab" href="#data1" role="tab" aria-controls="data1" aria-selected="false">Fully Automated Cargo Shower</a> 
                                </li>
                                <?php } ?>
                                <?php if(!empty($product_data['fast_rolling_curtain_door_cargo_shower']))		{	?> 
                                <li>    
                                    <a data-toggle="tab" href="#data2" role="tab" aria-controls="data2" aria-selected="false">Fast Rolling Curtain Door Cargo Shower</a> 
                                </li>
                                <?php } ?>
                                <?php if(!empty($product_data['double_door_cargo_shower']))		{	?> 
                                <li>    
                                    <a data-toggle="tab" href="#data3" role="tab" aria-controls="data3" aria-selected="false">Double Door Cargo Shower</a> 
                                </li>
                                <?php } ?>
                                <?php if(!empty($product_data['optional_configurations']))		{	?> 
                                <li>    
                                    <a data-toggle="tab" href="#data4" role="tab" aria-controls="data4" aria-selected="false">Optional Configurations</a> 
                                </li>
                                <?php } ?>
                            </ul>
                        </div>
                        <div class="tab-content">
                            <div id="Specifications" class="tab-pane fade in active" role="tabpanel"> 
                                <div class="chef-text product_d_table table-responsive-sm"> 
                                    <table class="table table-striped displayshow1">  
                                        <tbody> 
                                        <?php $json = str_replace(array("\t","\n"), "", $product_data['specifications']); 
                                                            $data = json_decode($json); 
                                                            foreach($data as $key => $value){ ?>
                                            <tr class="trset1"> 
                                            <?php if(!empty($value)){  ?>
                                                <td class="trset"><strong><?= $key ?></strong></td> 
                                                <td class=""> 
                                                    <?php if(is_object($value)){ ?>
                                                        <table class="."table table-striped displayshow1".">
                                                        <tbody>						 
                                                        <?php foreach($value as $k => $val){ ?> 
                                                            <tr> 
                                                            <?php	
                                                                if(!empty($val)){ 
                                                                    echo "<td class='trset1'><strong>".$k."</strong></td>"; 
                                                                    echo "<td>".$val."</td>"; 
                                                                }
                                                                ?> 
                                                            </tr> 
                                                        <?php }	echo "</tbody></table>"; 
                                                    }else  
                                                    {	echo $value;	} 
                                                    ?>								 
                                                </td> 
                                            <?php } ?> 
                                            </tr> 
                                        <?php } ?> 
                                        </tbody> 
                                    </table> 
                                </div> 
                            </div> 
                            <div id="Features" class="tab-pane fade" role="tabpanel">  
                                <div class="chef-text product_info_content"> 
                                    <!-- <p align="justify">  -->
                                        <?php 
                                            $features = $product_data['features']; 
                                            $features = explode("\n", $features); 
                                            $i = 1; 
                                            $ul = '<ul class="abt-li" title="Features">'; 
                                            foreach ($features as $feature) { 
                                                if ($feature == "") break; 
                                                $ul .= '<li><i class="fa fa-caret-right"> </i>' . 	str_replace(array('â‰¤','â€“'),array("≤","–"),$feature) . '' . '</li>'; 
                                                $i++; 
                                            } 
                                            $ul .= '</ul>'; 
                                            echo $ul;  
                                        ?> 
                                    <!-- </p>  -->
                                </div> 
                            </div> 
                            <?php if(!empty($product_data['applications']))		{	?> 
                                <div id="Applications" class="tab-pane fade" role="tabpanel"> 
                                    <div class="reviews_wrapper"> 
                                    <h5><?php echo $product_data['applications'] ?></h5> 
                                    </div> 
                                </div> 
                            <?php } ?>
                            <?php if(!empty($product_data['optional_filter']))		{	?> 
                                <div id="optional_filter" class="tab-pane fade" role="tabpanel"> 
                                    <div class="product_d_table table table-responsive-sm"> 
                                        <h5><?php echo $product_data['optional_filter'] ?></h5>
                                    </div> 
                                </div> 
                            <?php } ?> 
                            <?php if(!empty($product_data['standard_filter']))		{	?> 
                                <div id="standard_filter" class="tab-pane fade" role="tabpanel"> 
                                    <div class="product_d_table table table-responsive-sm"> 
                                        <h5><?php echo $product_data['standard_filter'] ?></h5> 
                                    </div> 
                                </div> 
                            <?php } ?>
                            
                            <?php if(!empty($product_data['standard_accessories']))		{	?> 
                                <div id="accessories" class="tab-pane fade" role="tabpanel"> 
                                    <div class="reviews_wrapper"> 
                                        <h5><?php echo $product_data['standard_accessories'] ?></h5> 
                                    </div> 
                                </div> 
                            <?php } ?>
                            
                            <?php if(!empty($product_data['fully_automated_cargo_shower']))		{	?> 
                                <div id="data1" class="tab-pane fade" role="tabpanel"> 
                                    <div class="reviews_wrapper"> 
                                        <h5><?php echo $product_data['fully_automated_cargo_shower'] ?></h5> 
                                    </div> 
                                </div> 
                            <?php } ?>
                            <?php if(!empty($product_data['fast_rolling_curtain_door_cargo_shower']))		{	?> 
                                <div id="data2" class="tab-pane fade" role="tabpanel"> 
                                    <div class="reviews_wrapper"> 
                                        <h5><?php echo $product_data['fast_rolling_curtain_door_cargo_shower'] ?></h5> 
                                    </div> 
                                </div> 
                            <?php } ?>
                            <?php if(!empty($product_data['double_door_cargo_shower']))		{	?> 
                                <div id="data3" class="tab-pane fade" role="tabpanel"> 
                                    <div class="reviews_wrapper"> 
                                        <h5><?php echo $product_data['double_door_cargo_shower'] ?></h5> 
                                    </div> 
                                </div> 
                            <?php } ?>
                            <?php if(!empty($product_data['optional_configurations']))		{	?> 
                                <div id="data4" class="tab-pane fade" role="tabpanel"> 
                                    <div class="reviews_wrapper"> 
                                        <h5><?php echo $product_data['optional_configurations'] ?></h5> 
                                    </div> 
                                </div> 
                            <?php } ?>
                            <?php if(!empty($product_data['optional_accessories']))		{	?> 
                                <div id="accessories_opt" class="tab-pane fade" role="tabpanel"> 
                                    <div class="product_d_table table-responsive-sm"> 
                                        <h5><?php echo $product_data['optional_accessories'] ?></h5> 
                                    </div> 
                                </div> 
                            <?php } ?>  
                        </div>
                    </div>     
                </div>
            </div>
        </div>    
    </div>
    <!-- tab section end-->  
   <!-- Modal -->
   <div class="modal fade" id="getQuote" tabindex="-1" role="dialog" 
      aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-md">
         <div class="modal-content">
               <!-- Modal Header -->
               <div class="modal-header">
                  
                  <h4 class="modal-title" id="myModalLabel">
                     Please fill the form to get Quote
                  </h4>
               </div>
               <form id="getquote_form" method="post">
               <!-- Modal Body -->
               <div class="modal-body"> 
                  <div class="row" style=" margin-right: 4px;margin-left: 4px;">
                     <div class="col-lg-12"> 
                     <label>Company Name<span class="required red-text">*</span></label>
                     <input type="text" required name="c_name" class="form-control">
                        <div class="help-block errormesssage"></div>
                        </div>
               
                  <div class="col-lg-12">
                     <label>Company Email<span class="required red-text">*</span></label>
                     <input type="email" required class="form-control" name="c_email">
                        <div class="help-block errormesssage"></div>
                        </div>
                        
                  <div class="col-lg-12">
                     <label>Company Contact Number</label>
                     <input type="text" class="form-control" name="c_phone">
                        <div class="help-block errormesssage"></div>
                        </div>
                  
                  <div class="col-lg-12">
                     <label>Product Name</label>
                     <input type="text" readonly class="form-control" name="c_product" value="<?php echo $product_data['name'] ?>">
                        <div class="help-block errormesssage"></div>
                        </div>
                  
                  <div class="col-lg-12">
                     <label>Subject</label>
                     <input type="text" class="form-control" name="c_subject" >
                        <div class="help-block errormesssage"></div>
               </div>
                  <div class="col-lg-12"> 
                     <label>Company Message<span class="required red-text">*</span></label>
                     <textarea name="c_message" required class="textarea-message form-control" rows="2"></textarea>   
                        <div class="help-block errormesssage"></div>
                        </div>
                  </div>
               </div>
            <!-- Modal Footer -->
            <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">                            Close
                  </button>
                  <button type="submit" class="btn btn-primary" id="form_submit_reg" value="Submit">
                     Get Quote
                  </button>
               </div>
            </form>
         </div>
      </div>
   </div>

    <!--Related Products  start-->
    <div class="home_section_two color_two mb-70">
        <div class="container">
            <div class="row">
            
                <div class="col-lg-11 col-md-12">
                    <div class="home_section_right">
                        <!--product area start-->
                        <div class="product_area mb-65">
                            <div class="section_title section_title_style2">
                                <h2>Related Products </h2>
                            </div>
                            <div class="row">
                                <div class="product_carousel product_column3 owl-carousel">
                                    <?php 
                                       if(isset($relate_products) && $relate_products !=''){
                                          foreach($relate_products as $product){
                                             $image_url =  image_url_helper($product['image_url'],'small');
                                             $section = get_prd_section($product['category_id']);

                                    ?>
                                    <div class="col-lg-3">
                                        <article class="single_product">
                                            <figure>
                                                <div class="product_thumb">
                                                    <a class="primary_img" href="<?= base_url()?><?= $section ?>/<?= $product['page_url'] ?>"><img src="<?php echo base_url().$image_url ?>" alt="<?= $product['name']?>"></a>
                                                
                                                </div>
                                                <div class="mt-2 rp-1" align="center">
                                                
                                                <a href="<?= base_url()?><?= $section ?>/<?= $product['page_url'] ?>"><?= $product['name']?></a>
                                                </div>
                                            </figure>
                                        </article>
                                    </div> 
                                    <?php 
                                }
                                        }
                                    ?>      
                                </div> 
                            </div>
                        </div>
                        <!--product area end-->
                    
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Related Products end-->