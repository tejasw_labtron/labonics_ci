<?php
   $this->load->view('common/breadcrumb',['current' => 'product categories']);
?>
    <!--categories section area-->
    <div class="unlimited_services">
        <div class="container">  
            <div class="row ml-1">
              <?php 
                foreach(array_chunk($main_cat,3) as $chunk){
                  foreach($chunk as $cat){
                    $section = get_cat_section($cat['section_id']);
              ?>
              <div class="col-lg-4 col-md-6" style="margin:15px 0px">
                <div class="row ">
              
                  <div class="col-lg-4 col-md-6">
                    <img src="<?php echo base_url()?>assets/images/categories/<?php echo $cat['name'].".jpg";?>" alt="<?php echo $cat['name']?>" class="brd-1">
                  </div>
                  <div class="col-lg-8 col-md-6"> 
                    <h5 class="clr-1 res-1"><a href="<?php echo base_url()?><?php echo $section?>/<?php echo $cat['page_url']?>"><?php echo $cat['name']?></a></h5>
                    <ul class="abt-li">
                      <?php 
                        $this->db->select('*')->from('categories')->order_by('name','ASC')->where('parent_id',$cat['id'])->where('status',1)->where('level',1);
                        $query = $this->db->get();
                        $response = array();
                        if($query->num_rows() > 0){
                           $level_one = $query->result_array();
                        }	
                        foreach($level_one as $sub_cat){
                      ?>
                      <li><a href="<?php echo base_url()?><?php echo $section?>/<?php echo $sub_cat['page_url']?>"><i class="fa fa-caret-right"></i><?php echo $sub_cat['name']?></a></li>
                      <?php 
                        }
                      ?> 
                    </ul>      
                  </div>
                </div>
              </div>
              <?php 
                  }
                }
              ?> 
            
            </div> 
                     
        </div>
    </div>     
    <!--categories section end--> 