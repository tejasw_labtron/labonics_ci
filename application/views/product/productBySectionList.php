<?php
   $this->load->view('common/breadcrumb',['current' => '']);
?>
<div class="unlimited_services tex">
        <div class="container">  
            <div class="row">
                <div class="col-lg-1 col-md-1">
                </div>
                <div class="col-lg-10 col-md-10">
                    <div class="section_title2 mt-10">
                        <h2><?php echo $section_data->name;?></h2>
                    </div>
                    <h5 class="text-center"><?php echo $section_data->description?></h5>
                </div>
                <div class="col-lg-1 col-md-1">
                </div>
            </div>
        </div>     
    </div> 

    <div class="unlimited_services ">
        <div class="container">  
            <div class="row ml-1 ">

                    <?php 
                        if(isset($all_categories) && $all_categories!='') {    
                            foreach($all_categories as $all_categorie){
                                
                                if($all_categorie['level'] == 0){
                                    $new_section = get_cat_section($all_categorie['section_id']);

                    ?>
                    <div class="col-lg-4 col-md-6">

                    <div class="row" style="margin-top: 53px">  
                        <div class="col-lg-4 col-md-6">
                            <img src="<?php echo base_url()?>assets/images/categories/<?php echo $all_categorie['name'].".jpg";?>" alt="<?php echo $all_categorie['name']?>" alt="<?php echo $all_categorie['name'];?>" class="brd-1">
                        </div>
                        <div class="col-lg-8 col-md-6">
                            <h5 class="clr-1 res-1"><a href="<?php echo base_url()?><?php echo $new_section?>/<?php echo $all_categorie['page_url']?>"><?php echo $all_categorie['name'];?></a></h5>
                            <ul class="abt-li">
                                <?php 
                                    foreach ($all_categories as $key => $all_sub_categorie) {  

                                        if($all_sub_categorie['parent_id'] == $all_categorie['id']){
                                            $new_section2 = get_cat_section($all_sub_categorie['section_id']);

                                ?>
                                <li><i class="fa fa-caret-right"></i><a href="<?php echo base_url()?><?php echo $new_section2?>/<?php echo $all_sub_categorie['page_url']?>"><?php echo $all_sub_categorie['name'];?></a></li>
                                <?php 
                                    }}
                                ?>
                            </ul>
                        </div>
                    </div>
                    </div>

                    <?php 
                        }
                    }
                }
                    ?>
                        
            </div>
        </div>
        
    </div> 