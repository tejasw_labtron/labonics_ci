    <!--search-page area start-->
<div class="unlimited_services">
    <div class="container"> 
 
        <div class="section_title3 mt-10">
            <h2>Search Result : " <?php echo $scontent['search_product'];?>"</h2>
            <p>Found "<?php echo count($all_products)?>" Results</p>
        </div>  
        <?php 
            if(count($all_products) != 0){
                foreach(array_chunk($all_products,4) as $chunk){
        ?> 
        <div class="row mt-60" align="center">   
            <?php 
                foreach($chunk as $product){
                    $image_url =  image_url_helper($product['image_url'],'small');

                    $this->db->select('*')->from('categories')->order_by('name','ASC')->where('parent_id',$product['id'])->where('status',1)->where('level',1);
                    $query = $this->db->get();
                    $response = array();
                    if($query->num_rows() > 0){
                    $response = $query->result_array();
                    }	
            ?>
            <div class="col-lg-3 col-md-6">
                <a href="#"><img src="<?php echo base_url().$image_url ?>" alt="<?php echo $product['sku']?>"></a>
                <h5 class="text-center mt-23 mb-30"> <a href="<?php echo base_url().$product['page_url']?>"><?php echo $product['name']?></a></h5>
                <button id="btn-new"><a href="<?php echo base_url().$product['page_url']?>">View Product</a></button>
            </div>
            <?php
                }
                echo '</div>';
                }
            }else{
            ?>
                <div class="row mt-60" align="center"> 
                    <div class="col"> 
                        <h2 class="text-center mt-23 mb-30">PRODUCTS NOT FOUND</h2>
                    </div>
                </div>
            <?php
                }
            ?> 
        </div>          
    </div>          
</div>     
<!--search-page area end--> 