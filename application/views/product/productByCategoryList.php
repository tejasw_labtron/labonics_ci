<?php
   $this->load->view('common/breadcrumb',['current' => '']);
?>
<div class="home_section_two color_two mb-70 mt-30">
   <div class="container">
      <div class="row">
         <div class="col-lg-3 col-md-4 col-sm-11">
            <div class="home_section_left ">
               <?php
                  $this->load->view('common/category-filter');
               ?>
            </div>
         </div>
         <div class="col-lg-9 col-md-8 col-sm-11">
            <div class="widget_list tags_widget">
               <h4><?php echo $category_data['name'] ?></h4>
               <p class="text-center mt-30"><?php echo $category_data['description'] ?></p>           

                  <?php 
                        $this->db->select('*')->from('categories')->order_by('name','ASC')->where('parent_id',$category_data['id'])->where('status',1)->where('level',1);
                        $query = $this->db->get();
                        $response = array();
                        if($query->num_rows() > 0){
                           $level_one = $query->result_array();
                        }	
                        if(isset($level_one) && $level_one !=''){
                           echo '<div class="tag_cloud text-center">';

                        foreach($level_one as $sub_cat){
                      ?>

                     <a href="<?= base_url()?><?php echo $section ?>/<?php echo $sub_cat['page_url'] ?>"><?php echo $sub_cat['name'] ?></a>
                     <?php 
                     }
                     echo '</div>';
                  }else{
                        $this->db->select('*')->from('categories')->order_by('name','ASC')->where('parent_id',$category_data['id'])->where('status',1)->where('level',2);
                        $query = $this->db->get();
                        $response = array();
                        if($query->num_rows() > 0){
                           $level_one = $query->result_array();
                        }	
                        if(isset($level_one) && $level_one !=''){
                           echo '<div class="tag_cloud2 text-center">';
                        foreach($level_one as $sub_cat){
                      ?>
                     <a href="<?= base_url()?><?php echo $section ?>/<?php echo $sub_cat['page_url'] ?>"><?php echo $sub_cat['name'] ?></a>
                     
                     <?php 
                     }
                     echo '</div>';
                     }
                  }
                     ?>
               
            </div>
            <div class="col-md-2">
               <div class="compare">
                  <a href="javascript:" class="btn btn-danger btn-compare"
                  style="display:none;" title="COMPARE" > COMPARE </a>
                  <br>
               </div>
            </div>
            <hr style="border-top: 1px solid #105e79;">                    
            <div class="masonry">
               <?php 
                  if(isset($level_one) && $level_one !='')
                  foreach($level_one as $res){
                     if(isset($all_products) && $all_products !='')
                     foreach($all_products as $product){
                        $image_url =  image_url_helper($product['image_url'],'small');
               ?>     
                  <div class="product padding">
                     <div class="product_block thumbnail">
         
                        <div class="product_image">
         
                           <a href="<?= base_url()?><?= $section ?>/<?= $product['page_url']?>"><img src="<?php echo base_url().$image_url ?>" alt="<?= $product['name']?>"></a>
         
                        </div>
                        <div class="product_title" title="<?= $product['name']?>">                       
            
                           <p align="center"><a  href="<?= base_url()?><?= $section ?>/<?= $product['page_url']?>" ><?= $product['name']?></a></p>
                           <div class="row ml-4">
               
                                 <a href="<?= base_url()?>catalog/<?= $product['sku']?>">
                                    <button class="action action--button action--buy"><i class="fa fa-file-pdf-o"></i></button> 
                                 </a>
                                 <a href="#" class="add-to-cart" data-productid="<?php echo $product['sku'];?>" data-productqty=1>
                                    <button class="action add_to_cart action--button2 action--buy" >
                                       <i class="zmdi zmdi-shopping-cart"></i>
                                    </button> 
                                 </a>

                              
                        
                           </div>
                           <br>
                        </div> 
                        <label class="action action--compare-add text-center" style="border:0px">
                           <input class="check-hidden checkbox" type="checkbox" autocomplete="off"	value="<?= $product['id']?>">
                           <i class="fa fa-plus"></i>
                           <i class="fa fa-check"></i>
                           <span class="action__text action__text--invisible" title="Add to compare" >Add to compare</span>
                        </label>				
         
                     </div>
                  </div>
               <?php
                        }
               }else{
                  if(isset($all_products) && $all_products !='')
                  foreach($all_products as $product){
                     $image_url =  image_url_helper($product['image_url'],'small');

               ?>     
                  <div class="product padding">
            
                        <div class="product_block thumbnail">
            
                           <div class="product_image">
            
                              <a href="<?= base_url()?><?= $section ?>/<?= $product['page_url']?>"><img src="<?php echo base_url().$image_url ?>" alt="<?= $product['name']?>"></a>
            
                           </div>
                           <div class="product_title" title="<?= $product['name']?>">                       
            
                              <p align="center"><a  href="<?= base_url()?><?= $section ?>/<?= $product['page_url']?>" ><?= $product['name']?></a></p>
                              <div class="row ml-4">
                                 <a href="<?= base_url()?>catalog/<?= $product['sku']?>">
                                    <button class="action action--button action--buy"><i class="fa fa-file-pdf-o"></i></button> 
                                 </a>
                                 <a href="#" class="add-to-cart" data-productid="<?php echo $product['sku'];?>" data-productqty=1>
                                    <button class="action add_to_cart action--button2 action--buy" >
                                       <i class="zmdi zmdi-shopping-cart"></i>
                                    </button> 
                                 </a>
                           
                              </div>
                              <br>
                           </div> 
                           
                           <label class="action action--compare-add text-center" style="border:0px">
                              <input class="check-hidden checkbox" type="checkbox" autocomplete="off"	value="<?= $product['id']?>">
                              <i class="fa fa-plus"></i>
                              <i class="fa fa-check"></i>
                              <span class="action__text action__text--invisible" title="Add to compare" >Add to compare</span>
                           </label>				
            
                        </div>
                  </div>
               <?php 
                        }
               }
               ?>
            </div>

         </div>
      </div>
   </div>
</div>