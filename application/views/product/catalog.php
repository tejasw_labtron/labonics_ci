<?php
   $this->load->view('common/breadcrumb',['current' => 'catalog']);
?>
<style>

.no-border>tbody>tr>td{border:none;}

table>thead>tr>th, table>tbody>tr>th, table>tfoot>tr>th, table>thead>tr>td, table>tbody>tr>td, table>tfoot>tr>td{border-top: 1px solid #808080;}hr{border-top: 1px solid #868686 !important;}.last-page{margin-top:10%;margin-bottom:20%;}

.foot-link{color:#fff;}
.left-nav,
.right-nav{border-right:4px solid #e2e2e2;border-top:4px solid #e2e2e2;border-bottom:4px solid #e2e2e2}.sticky{position:-webkit-sticky;position:sticky;top:20px}.left-nav{border-left:4px solid #e2e2e2}.right-nav{padding:1%}.level-2-table tr td{text-align:center;vertical-align:middle;font-size:12px;font-weight:700}.spec .level-2-table tr td hr{margin-top:.5rem;margin-bottom:.5rem;border:0;border-top:1px solid rgba(0,0,0,.1)}.thumbnail{margin:0 auto}.page-template>*{position:absolute;font-family:"DejaVu Sans",Arial,sans-serif!important;left:20px;right:20px;font-size:90%;font-weight:400}.page-template .header{top:20px;border-bottom:0 solid #000}.page-template .footer{bottom:20px;border-top:0 solid #000}.page-template .watermark{font-weight:700;font-size:400%;text-align:right;margin-top:70%;color:#aaa;opacity:.4}.catalog-background{box-shadow:0 2px 2px 0 rgba(0,0,0,0.16),0 0 0 1px rgba(0,0,0,0.08);}#customers{background-color:#fff;width:100%}.wx-holder{max-width:80%;margin:0 auto;padding:15px 0}.small-text{font-size:12px}.text-right{text-align:right}.text-left{text-align:left}.text-upper{text-transform:uppercase}.bluecolor-text{color:#3c6586}.bold-text{font-weight:700!important}.italic-font{font-style:italic}.orborder-right{border-right:2px solid #fa9d2d}.style-font{font-weight:700}.medium-font{padding-top:10px}.medium-font p{font-size:16px;font-weight:400}.wx-clearafter:after{clear:both;display:block;content:"";width:0;height:0}.wx-left{float:left;width:150px}.wx-right{float:left;width:calc(100% - 150px)}.wx-leftmain{height:480px;float:left;width:200px}.wx-rightmain{float:left;width:calc(100% - 205px)}.wx-img{padding-right:5px}.wx-img img{max-width:100%}.wx-border{border-right:1px solid #ddd}.wxborder-header{font-weight:500;font-size:12px;display:table;position:relative;padding:0 25px 0 55px}.wxborder-header:after{position:absolute;content:"";left:0;border-bottom:2px solid #fa9d2d;width:300px}.contentmain{padding:0 20px}.first-block{padding:15px 0 70px}.large-font{font-size:18px}.medium-font{font-size:16px}
.content{padding:15px 0}.contentmain p+p{padding-top:15px}.contentmain ul{padding:10px 40px 25px}.contentmain ul li{list-style-position:inside;padding-bottom:10px}.p-class{padding:15px 0}.p-top{padding-top:15px}.wx-innercontent{padding:0 25px}.wx-innercontent>div{padding:15px 0}.wx-innercontent>div h3{padding-bottom:10px;color:#333;font-size:20px}.orange-ul{padding:10px 0 0}.orange-ul li{color:#fa9d2d;padding-bottom:10px}
.orange-ul li p{color:#2a201b}
.margin-left{margin-right:21px;text-align:justify}.address,.info>span{color:#989898}.info{color:#000}.last-page-margin{margin-top:50%}.contact-details1{background:#105e79}.body-last,.body1{background-repeat:no-repeat}.table-bordered>tbody>tr>td,.table-bordered>tbody>tr>th,.table-bordered>tfoot>tr>td,.table-bordered>tfoot>tr>th,.table-bordered>thead>tr>td,.table-bordered>thead>tr>th{border:1px solid #868686}.company-logo-top{padding-bottom:5%;}.product-image{margin-left:20%;max-height: 288px;max-width:60%;height:auto;border: 3px solid #fd840a;    border-radius: 20%;}.contact-details{color:#fff;font-size:19px;padding:6% 4%}.body,.body-last,.body1{padding:4%}.logo{width:240px;}.body1{background-image:url('<?= base_url() ?>assets/image/catalog-background/Template5.jpg')}.body-last{background-image:url('<?= base_url() ?>assets/images/catalog-background/Template5.jpg.png');}.catalog-description{padding-left:26%;padding-right:18%;text-align:justify}.product-name{letter-spacing: 1.2px;font-style: italic;font-weight:900;font-family: inherit;color:#000!important;font-size:30px;padding:10% 1% 1% 1%;margin-top:0;margin-right:8%;height:282px}.pdf-page{margin:0 auto;box-sizing:border-box;box-shadow:0 5px 10px 0 rgba(0,0,0,.3);background-color:#fff;color:#333;position:relative}.pdf-body,.pdf-footer,.pdf-header{position:absolute}.invoice-number{padding-top:.17in;float:right}.size-a4{width:8.3in;height:11.7in}.size-letter{width:8.5in;height:11in}.size-executive{width:7.25in;height:10.5in}.for,.from{position:absolute;top:1.5in;width:2.5in}.company-logo{font-size:30px;font-weight:700;color:#3aabf0}.for{left:.5in}.from{right:.5in}.for p,.from p{color:#787878}.signature{padding-top:.5in}

.body1 {
      background-image: url('<?php echo base_url()?>assets/img/Template5.jpg');
      background-repeat: no-repeat;
      background-size: 100% 100%;
  }

.last-page {
   margin-top: 5%;
   margin-bottom: 20%;
   padding-bottom: 5%;
   box-shadow: 0px 5px 20px rgb(0 0 0 / 10%);
}

</style>

<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.2.621/styles/kendo.common-material.min.css" />

<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.2.621/styles/kendo.material.min.css" />

<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.2.621/styles/kendo.material.mobile.min.css" /> 

<script src="https://kendo.cdn.telerik.com/2017.2.621/js/jquery.min.js"></script>

<script src="https://kendo.cdn.telerik.com/2017.2.621/js/jszip.min.js"></script>

<script src="https://kendo.cdn.telerik.com/2017.2.621/js/kendo.all.min.js"></script>  

<script type="x/kendo-template" id="page-template">

<div class="page-template my-content"></div>

</script>

<div class="container" style="padding:3%;">
   <div class="row">
      <div class="col-lg-3 col-md-5" align="center">
         <h4>Generate Catalog For :</h4>   
         <h5 class="mt-23 mb-30"><?= $product['name']; ?> </h5>
         <button id="btn-new1" onclick="ExportPdf()">Download Catalog </button>
      </div>  
      <div class="col-lg-9 col-md-9">
         <div class="container" style="width:80%;">
            <div class="wrapper" id="myCanvas">
               <div class="catalog-background"> 
                  <div class="company-logo-top">
                     <div class="row" style="padding:4%;">
                        <div class="col-md-6"> 
                           <a href="<?= base_url() ?>"><img class="logo" src="<?= base_url()?>assets/images/logo2.png"/></a>
                        </div>
                     </div>    
                  </div> 
                  <br>
                  <div class="body1"> 
                     <div class="row">   
                        <div class="col-md-12">  
                           <?php $image_src =  image_url_helper($product['image_url'],'medium'); ?> 
                           <img class="product-image" src="<?= base_url().$image_src ?>"/>
                           <h1 class="product-name text-center"><?php echo $product['name']; ?></h1> 
                        </div> 
                     </div>
                  </div>  
                  <div class="contact-details1">
                     <div class="contact-details text-center">
                        <span><a class="foot-link" href="<?= base_url()?>"><?php echo bsnprm_value(BSN_WEBSITE_LINK) ?></a></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><a class="foot-link" href="mailto:<?php echo bsnprm_value(BSN_WEBSITE_EMAIL_ADDRESS) ?>"><?php echo bsnprm_value(BSN_WEBSITE_EMAIL_ADDRESS) ?></a></span>
                     </div>
                  </div>
                  <div class="page-break"></div>

                  <div class="body prevent-split">
							<h4><b>Description : </b></h4>
                     <?php if($product['description']){ ?>
                        <p><?= $product['description'] ?></p>
                     <?php } ?>
                     <br>
                     <h4><b>Specifications : </b></h4>
                     <div class="table-responsive">
                        <table class="table table-condensed displayshow1"> 
                           <tbody> 
                              <?php 
                                 $json = str_replace(array("\t","\n"), "", $product['specifications']); 
                                 $data = json_decode($json); 
                                 $j = 1; 
                                 foreach ($data as $spec_key => $spec_value) { if($spec_value){
                              ?> 
                                 <tr class="trset1">
                                    <td><?php echo $spec_key; ?></td>
                                    <td>
                                       <?php
                                       if (is_object($spec_value)){
                                          echo '<table class="table level-2-table"><thead>';
                                          echo "<tr class='table-light'>";
                                          foreach ($spec_value as $n => $n_spec) {
                                                if ($n_spec) {
                                                   echo '<td>' . $n . '</td>';
                                                }
                                          }
                                          echo "</tr></thead><tbody><tr>";
                                          foreach ($spec_value as $n => $n_spec) {
                                          if ($n_spec) {
                                                echo '<td>'.$n_spec. '</td>';
                                          }
                                          }
                                          echo '</tr></tbody></table>';
                                       }else{
                                          echo $spec_value ;
                                       }?>
                                    </td>
                                 </tr>
                              <?php }} ?>
                           </tbody>
                        </table>
                     </div>
                     <br>
                     <table class="no-border">
                        <tr>
                           <td>
                              <h4><b>Features : </b></h4>
                              <p style="text-align:justify">
                                 <?php
                                    if ($product['features']) { 
                                    $features = $product['features'];
                                    if ($features) {
                                       $features = explode("<br/>", $features);
                                       if (count($features) < 2) {
                                       $features = explode("\n", $features[0]);
                                       }
                                    }
                                    if (is_array($features)) {
                                       $ul = '<ul class="abt-li" title="Features">';
                                       foreach ($features as $feature) {
                                       if ($feature) {
                                             $feature = str_replace('pbull','',$feature);
                                             $feature = str_replace('bull','',$feature);
                                             $ul .= '<li><i class="fa fa-caret-right"> </i>' . htmlspecialchars_decode($feature,ENT_QUOTES).'</li>';
                                       }
                                       }
                                       $ul .= '</ul>';
                                       echo htmlspecialchars_decode($ul,ENT_QUOTES);
                                    } 
                                    }
                                    echo '<br>';
                                 ?>
                              </p>
                           </td>
                        </tr>
                     </table>                    
                     <?php if($product['applications']){?>
                        <table class="no-border">
                           <tr>    
                              <td>
                                 <h4><b>Applications : </b></h4>
                                 <p><?php echo $product['applications']; ?></p>
                              </td>
                           </tr>
                        </table><br>
                     <?php } ?> 
                     <?php if($product['standard_accessories']){?>
                        <table class="no-border">
                           <tr>    
                              <td>
                                 <h4><b>Standard Accessories : </b></h4>
                                 <p><?php echo $product['standard_accessories']; ?></p>
                              </td>
                           </tr>
                        </table><br>
                     <?php } ?>

                     <?php if($product['fully_automated_cargo_shower']){?>
                        <table class="no-border">
                           <tr>    
                              <td>
                                 <h4><b>Fully Automated Cargo Shower : </b></h4>
                                 <p><?php echo $product['fully_automated_cargo_shower']; ?></p>
                              </td>
                           </tr>
                        </table><br>
                     <?php } ?>
                     <?php if($product['fast_rolling_curtain_door_cargo_shower']){?>
                        <table class="no-border">
                           <tr>    
                              <td>
                                 <h4><b>Fast Rolling Curtain Door Cargo Shower : </b></h4>
                                 <p><?php echo $product['fast_rolling_curtain_door_cargo_shower']; ?></p>
                              </td>
                           </tr>
                        </table><br>
                     <?php } ?>
                     <?php if($product['double_door_cargo_shower']){?>
                        <table class="no-border">
                           <tr>    
                              <td>
                                 <h4><b>Double Door Cargo Shower : </b></h4>
                                 <p><?php echo $product['double_door_cargo_shower']; ?></p>
                              </td>
                           </tr>
                        </table><br>
                     <?php } ?>
                     <?php if($product['optional_configurations']){?>
                        <table class="no-border">
                           <tr>    
                              <td>
                                    <h4><b>Optional Configurations : </b></h4>
                                    <p><?php echo $product['optional_configurations']; ?></p>
                              </td>
                           </tr>
                        </table><br>
                     <?php } ?>
                     <?php if($product['optional_accessories']){?>
                        <table class="no-border">
                           <tr>    
                              <td>
                                 <h4><b>Optional Accessories : </b></h4>
                                 <p><?php echo $product['optional_accessories']; ?></p>
                              </td>
                           </tr>
                        </table><br>
                     <?php } ?>
                  </div> 
                  <div class="last-page prevent-split">
                     <div class="text-center">
                        <img style="width:40%; margin-top: 30px;" src="<?= base_url() ?>assets/images/logo2.png" alt="Labonics">
                        <p class="mt-23"><strong>Labonics Systems Ltd.</strong></p>
                        <h6>9 Overton Road Walthamstow London E10 7NN UK</h6>
                        <p style="font-size:12px;font-weight: bolder;">
                           <span>Email: </span><a style="color:#016c9f;" href="mailto:<?php echo bsnprm_value(BSN_WEBSITE_EMAIL_ADDRESS) ?>"><?php echo bsnprm_value(BSN_WEBSITE_EMAIL_ADDRESS) ?></a> | 
                           <span>Website: </span><a style="color:#016c9f;" href="<?php echo bsnprm_value(BSN_WEBSITE_LINK) ?>"><?php echo bsnprm_value(BSN_WEBSITE_LINK) ?></a> <br>
                           <span>Phone: </span><a style="color:#016c9f;" href="tel:<?php echo bsnprm_value(BSN_WEBSITE_PHONE_NUMBER) ?>"><?php echo bsnprm_value(BSN_WEBSITE_PHONE_NUMBER) ?></a> 
                        </p>
                     </div>
                  </div>
               </div> 
            </div> 
         </div>
      </div>
   </div>    
</div>

<script src="../content/shared/js/pako.min.js"></script> 

<script> 

    kendo.pdf.defineFont({

        "DejaVu Sans"             : "https://kendo.cdn.telerik.com/2016.2.607/styles/fonts/DejaVu/DejaVuSans.ttf",

        "DejaVu Sans|Bold"        : "https://kendo.cdn.telerik.com/2016.2.607/styles/fonts/DejaVu/DejaVuSans-Bold.ttf",

        "DejaVu Sans|Bold|Italic" : "https://kendo.cdn.telerik.com/2016.2.607/styles/fonts/DejaVu/DejaVuSans-Oblique.ttf",

        "DejaVu Sans|Italic"      : "https://kendo.cdn.telerik.com/2016.2.607/styles/fonts/DejaVu/DejaVuSans-Oblique.ttf",

        "WebComponentsIcons"      : "https://kendo.cdn.telerik.com/2017.1.223/styles/fonts/glyphs/WebComponentsIcons.ttf"

    });

    function ExportPdf(){ 

        kendo.drawing

        .drawDOM("#myCanvas", 

        { 

            forcePageBreak: ".page-break", 

            paperSize: "A4", 

            template: $("#page-template").html(),

            keepTogether: ".prevent-split"

        }).then(function(group){

                kendo.drawing.pdf.saveAs(group, "<?= str_replace(" ","-",$product['name']) ?>.pdf")

            });

    }

</script> 