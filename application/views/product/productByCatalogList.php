<?php $this->load->view('common/header')?>
<link href="<?php echo base_url()?>assets/css/custom/catalog_style.css" rel="stylesheet">
<!--products-area start-->
<div class="products-area">
   <div class="container">
      <div class="row">
         <div class="col-xl-12 col-lg-12">
            <li class="lgbread">
                <?php echo ci_breadcrumbs_helper(); ?>
            </li>
            <h3 class=" heading text-center" style="margin-top: 25px;">Catalog List</h3>
            <br>
            <!--products-area start-->
            <div class="container">
               <?php if(isset($all_category) && $all_category !=''){ ?>
               <div class="row">
                  <div class="masonry">
                    <?php 
                    foreach ($all_category as $key => $all_categorie) { 
                          if($all_categorie['level'] == 0){ ?>
                     <div class="item">
                        <div class="card">
                           <div class="card-body">
                              <h5 class="card-title"><a href="javascript:void(0);" onclick="return getproducts_cat(<?php echo $all_categorie['id'] ?>)"><?php echo $all_categorie['name']?></a></h5>
                              <ul class="liststy">
                                <?php if(isset($all_categorie['all_children_ids']) && $all_categorie['all_children_ids'] != ''){
                                  foreach($all_categorie['all_children_ids'] as $all_sub_categorie){   ?>
                                 <li class="font400" > <a href="javascript:void(0);" onclick="return getproducts_cat(<?php echo $all_category[$all_sub_categorie]['id'] ?>)"><i class="fa fa-angle-right right-arrow"></i>  <?php echo $all_category[$all_sub_categorie]['name'] ?></a></li>
                                 <?php }} ?>
                              </ul>
                           </div>
                        </div>
                     </div>
                    <?php }} ?>
                  </div>
               </div>
             <?php } ?>
            </div>
         </div>
      </div>
   </div>
</div>
<!--brands-area start-->
<div class="container-fluid mt-50">
</div>
<!--brands-area end-->
<?php $this->load->view('common/footer')?>

 <!-- The Modal -->
<div class="modal fade" id="myCatalogModal">
   <div class="modal-dialog modal-lg  modal-dialog-centered">
      <div class="modal-content" id="addcards">
        
         <!-- Modal footer -->
         <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>


<script type="text/javascript">
   $(document).ready(function () {
   $('.navbar-light .dmenu').hover(function () {
           $(this).find('.sm-menu').first().stop(true, true).slideDown(150);
       }, function () {
           $(this).find('.sm-menu').first().stop(true, true).slideUp(105)
       });
   });
</script>

