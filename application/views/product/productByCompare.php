<?php
   $this->load->view('common/breadcrumb',['current' => 'compare products']);
?>
<!--Compare area start-->
<div class="mt-60 mb-60">
   <div class="container"> 
      <div class="row">   
         <div class="col-lg-12 col-md-12" align="center">   
            <?php  if(isset($product) && $product != '') { ?> 
               <div class="table-responsive">
                  <table class="table">
                     <tbody>
                        <?php

                        if ($product) {
                           echo '<tr><td style="padding-top:90px; background:#105e79;color:#fff;">Compare  ' . count($product) . ' Items</td>';

                        foreach ($product as $pro) {
                           $image_url                  =  image_url_helper($pro['image_url'],'small');
                           $section = get_prd_section($pro['category_id']);
                           ?>
                        <td class="cam-1"><img src="<?php echo base_url().$image_url ?>" alt="<?= $pro['name']?>"><br><a href="<?php echo base_url()?><?= $section?>/<?php echo $pro['page_url']?>"><?= $pro['name']?></a></td>
                        <?php
                        }
                        echo '</tr>';
                        }
                        ?>
                         <?php 
                              if(isset($spec_key) && $spec_key != '') {
                              foreach ($spec_key as $k => $item) {
                                echo '<tr>';
                                echo "<td>" . $k . "</td>";
                                if ($item['child']) {
                                    foreach ($item['child'] as $v) {
                                        if (!$v) {
                                            $v = '&mdash;';
                                        }
                                    if(is_array($v))
                                    {
                                      $string = '';
                                      foreach ($v as $key => $value) {
                                        if(!empty($value))
                                          {
                                            $string .= '<strong>'.$key.'</strong> : '.$value.'<br>';
                                          }
                                      }
                                      echo '<td>' . $string . '</td>';
                                    }
                                    else
                                    {
                                      echo '<td>' . $v . '</td>';
                                    }
                                    //echo '<td>' . $v . '</td>';
                                    }
                                }
                                echo '</tr>';
                                }}
                                ?>      
                     </tbody>
                  </table>

               </div>
            <?php } ?>
         </div>
      </div>
   </div>                    
</div>     
<!--compare area end--> 