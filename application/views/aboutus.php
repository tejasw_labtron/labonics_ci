<?php
   $this->load->view('common/breadcrumb',['current' => 'about us']);
?>
    <!--services section area-->
    <div class="unlimited_services tex">
        <div class="container">  
            <div class="row">
                <div class="col-lg-1 col-md-1"></div>        
                    <div class="col-lg-10 col-md-5">
                        <div class="section_title2 mt-10">
                            <h2>About Us</h2>
                        </div>
                        <ul class="abt-li">
                            <li><i class="fa fa-arrow-circle-right"></i>Labonics is a recognized global leader of Laboratory Furniture and Cabinets for any application from clinical to research. We endeavour to bring to you solutions that improve the quality of your results, save time and lower overall costs.</li>

                            <li><i class="fa fa-arrow-circle-right"></i>Laboratory equipment from Labonics includes a wide variety of Laboratory furniture , Laminar Air flow cabinets, Bio Safety cabinets, Fume hoods , Safety containment cabinets etc.</li>

                            <li><i class="fa fa-arrow-circle-right"></i>Our products and services help accelerate the pace of scientific discovery, and solve analytical challenges ranging from complex research to routine testing to field applications. Whether you’re looking for cutting-edge technology to enliven and support your labs in biology, chemistry, physics, or engineering there’s a Labonics solution appropriate for every level offering unparalleled support for the life of your products.</li>
                        </ul>
            
                        <!--banner area start-->
                        <div class="banner_area banner_four mt-23">
                            <div class="container">
                                <div class="row">
                                    <div class="col-12">
                                        <figure class="single_banner">
                                            <div class="banner_thumb">
                                                <img src="<?php echo base_url()?>assets/img/banner15.jpg" alt="about us">
                                            </div>
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--banner area end-->
                    </div>
                    <div class="col-lg-1 col-md-1">
         
                  
                    </div>
                </div>
          </div>     
    </div> 
    <!--services section end--> 
