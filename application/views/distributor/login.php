<!-- distributed login start -->
<div class="customer_login mt-60">
    <div class="container">
        <div class="row ">
            <!--login area start-->
            <div class="col-lg-12 col-md-6">
                <div class="account_form">          
                    <form class="md-float-material form-material" method="post" id="login_form" style="border:0px;">
                        <p class="cls-ft">Welcome to labonics.com</p>
                        <p >
                            <label for="email" >E-Mail Address</label>

                            <input id="email" type="email" class="form-control" name="email" placeholder="Enter Your Email Id" required autocomplete="email" autofocus>

                                <div class="help-block errormesssage"></div>
                        </p>

                        <p >
                            <label for="password" >Password</label>

                            <input id="password" type="password" class="form-control" name="password" placeholder="Enter Your Password" required autocomplete="current-password">

                            <div class="help-block errormesssage"></div>
                        </p>
                        <div class="login_submit">   
                        <button type="submit" class="btn btn-md waves-effect text-center m-b-20 c-frm-btn-1" id="form_submit"> Login</button> 
                        
                            <a href="<?php echo base_url('forgot-password') ?>">Forgot Your Password?</a>
                        </div> 
                        <h4 align="center"><span style="color:#000;">Become a distributor,</span> <a href="<?php echo base_url('register') ?>"><strong style="color:#f7dbbf">Register here</strong></a></h4>
                    </form>
                    
                    </div>    
            </div>
            <!--login area start-->

        
        </div>
    </div>    
</div>
    <!-- distributed login end -->