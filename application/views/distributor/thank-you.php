<div id="notfound">
   <div class="notfound">
      <div class="thank-you">
         <img src="<?php echo base_url(); ?>assets/images/Thank-you.png" alt="Thank you"/>
         <p style="text-align: center;">
            <span style="font-family: Montserrat; font-size: 30px;">
               <strong>
                  <span style="color: #393942;">Thank You For Registering with Us!</span>
               </strong>
            </span>
         </p>
         <h4>We will get back to you when the account is activated.</h4>
      </div>
      <a href="<?php echo site_url(); ?>">Back To Homepage</a>
   </div>
</div>
