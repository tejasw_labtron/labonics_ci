<!-- distributed login start -->
<div class="customer_login mt-60">
   <div class="container">
      <div class="row ">
         <!--login area start-->
         <div class="col-lg-12 col-md-6">
            <div class="account_form">
               <form enctype="multipart/form-data" class="md-float-material form-material" role="form" id="reg-frm" name="reg-frm" method="post" style="border:0px;">

                     <p class="cls-ft">Distributor Registration !!!</p>
                     <p >
                        <label for="company_name">Company Name<span class="required"> *</span></label>

                           <input id="company_name" type="text" class="form-control" name="company_name"  required autofocus>

                              <div class="help-block errormesssage"></div>
                     </p>
                     <p >
                        <label for="contact_person">Contact Person<span class="required"> *</span></label>
                           <input id="contact_person" type="text" class="form-control " name="contact_person" required autocomplete="contact_person">

                              <div class="help-block errormesssage"></div>
                     </p>
                     <p >
                        <label for="contact_number">Contact Number<span class="required"> *</span></label>
                           <input id="contact_number" type="text" class="form-control" name="contact_number" required autocomplete="contact_number">

                              <div class="help-block errormesssage"></div>
                     </p>
                     <p >
                        <label for="country">Country<span class="required"> *</span></label>
                           <input id="country" type="text" class="form-control" name="country" required autocomplete="country">

                              <div class="help-block errormesssage"></div>
                     </p>
                     <p >
                        <label for="email">E-Mail Address<span class="required"> *</span></label>

         
                           <input id="email" type="email" class="form-control" name="email" required autocomplete="email">
                              <div class="help-block errormesssage"></div>
                     </p>

                     <p >
                        <label for="password">Password<span class="required"> *</span></label>

         
                           <input id="password" type="password" class="form-control" name="password" required autocomplete="new-password">
                           <div class="help-block errormesssage"></div>
                     </p>

                     <p >
                        <label for="password-confirm">Confirm Password<span class="required"> *</span></label>

         
                           <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                     </p>
                     <p >
                        <label for="address">Address</label>

         
                           <input id="address" type="text" class="form-control" name="address" autocomplete="address">

                              <div class="help-block errormesssage"></div>
                     </p>                            
                     <div class="login_submit">       
                     <button type="submit" class="btn btn-md waves-effect text-center m-b-20 c-frm-btn-1 btn_regsave" id="form_submit_reg"> Register</button> 
                     </div>
                  <h4 align="center"><span style="color:#000;">Already have an account ? </span><a href="<?php echo base_url('login'); ?>"><strong style="color:#f7dbbf">Login here</strong></a></h4>
               </form>
            </div>    
         </div>
         <!--login area start-->
      </div>
   </div>    
</div>
    <!-- distributed login end -->