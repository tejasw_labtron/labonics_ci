<div class="customer_login mt-60">
   <div class="container">
      <div class="row justify-content-center">
         <div class="col-md-8">
            <div class="card">
               <div class="card-header">Reset Password</div>
                  <div class="card-body">
                     <form class="md-float-material form-material" style="border: 0px;" method="post" id="ftpw_email_form">
                        <div class="form-group row">
                           <label for="email" class="col-md-4 col-form-label text-md-right">E-Mail Address</label>

                           <div class="col-md-6">
                              <input id="email" type="email" class="form-control " name="email"  required autocomplete="email" autofocus>

                              <div class="help-block errormesssage"></div>
                           </div>
                        </div>

                        <div class="form-group row mb-0">
                           <div class="col-md-6 offset-md-4">
                              <button type="submit" class="btn btn-md btn-primary waves-effect text-center m-b-20 c-frm-btn-pwd" id="form_submit_ftpw_email">Send Password Reset Link</button>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>