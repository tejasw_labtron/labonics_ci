<?php
   $this->load->view('common/breadcrumb',['current' => 'distributor dashboard']);
?>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css"/>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/autofill/2.3.4/css/autoFill.dataTables.min.css"/>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css"/>
<!-- DataTables CSS -->
<link href="<?php echo base_url()?>assets/css/addons/datatables.min.css" rel="stylesheet">
<!-- DataTables Select CSS -->
<link href="<?php echo base_url()?>assets/css/addons/datatables-select.min.css" rel="stylesheet">
<!--products-area start-->
<div class="products-area" >
   <div class="container-fluid">
      <div class="row" style="margin: 0px;">
         <div class="col-xl-12 col-lg-12">
            <h2 class="text-center">Admin Dashboard</h2>
            <div class="container">
               <?php if(isset($distributor_list) && $distributor_list !=''){ ?>
               <div class="col-lg-12 tabstyle">
                  <table id="dtBasicExample" class="table table-responsive table-striped " cellspacing="0">
                     <thead>
                        <tr>
                          <th>Sr No.</th>
                          <th>Company Name</th>
                          <th>Company Person</th>
                          <th>Location</th>
                          <th>Contact No.</th>
                          <th>Registered Email</th>
                          <!-- <th>Registered Password</th> -->
                          <th>Status</th>
                          <th>Active User</th>
                          <th>Block User</th>
                        </tr>
                     </thead>
                     <tbody>
                       <?php
                        $i = 0;
                        if(isset($distributor_list) && $distributor_list !=''){
                          foreach ($distributor_list as $key => $distributorlist) { $i++; ?>

                       <tr>
                         <input type="hidden" name="auth_id" id="auth_id" value="<?php echo $distributorlist['id']; ?>">
                        <td class="col-style"><?php echo $i ?></td>
                        <td class="col-style"><?php echo $distributorlist['company_name'] ?></td>
                        <td class="col-style"><?php echo $distributorlist['company_person'] ?></td>
                        <td class="col-style"><?php echo $distributorlist['country'] ?></td>
                        <td class="col-style"><?php echo $distributorlist['contact_number'] ?></td>
                        <td class="col-style"><?php echo $distributorlist['email'] ?></td>
                        <!-- <td class="col-style"><?php echo decrypt_password($distributorlist['password']) ?></td> -->
                        <td class="col-style"> <?php  
                              if($distributorlist['status'] == STATUS_ACTIVE)
                             {
                               echo '<button type="button" class="btn-success btn-medium">Active</button>';
                             }
                             elseif($distributorlist['status'] == STATUS_BLOCKED)
                             {
                                echo '<button type="button" class="btn-danger btn-medium">Blocked</button>';
                             }
                             else
                             {
                                 echo '<button type="button" class="btn-info btn-medium">Inactive</button>';
                             }
                          ?>
                          
                        </td>
                        <td class="col-style">
                          <?php  
                              if($distributorlist['role'] != AUTH_ROLE_ADMIN && $distributorlist['status'] != STATUS_ACTIVE)
                             { ?>
                             <button style="background:#fff;border:2px solid #333" class="btn" onclick="activate_distributor(<?php echo $distributorlist['id'] ?>)" id="distributor_active" type="submit"><i class="fa fa-check" style="font-size:18px;color:green" ></i></button>
                          <?php } ?>
                        </td>
                        <td class="col-style">
                           <?php  
                              if($distributorlist['role'] != AUTH_ROLE_ADMIN && $distributorlist['status'] != STATUS_BLOCKED)
                             { ?>
                          <button style="background:#fff;border:2px solid #333" class="btn" id="distributor_block" onclick="block_distributor(<?php echo $distributorlist['id'] ?>)" type="submit"><i class="fa fa-ban" style="font-size:18px;color:red"></i></button>
                           <?php }?>
                        </td>
                      </tr>
                      
                      <?php }} ?>
                        
                     </tbody>
                  </table>
               </div> 
                <?php }else{ ?>

                  <h1>
                    YOUR ARE NOT AUTHORIZED TO ACCESS THIS CONTENT
                  </h1>
               <?php } ?>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
<!--products-area end-->
<script src="<?php echo base_url()?>assets/js/form_validation_distributor.js" ></script>
<!-- DataTables Select JS -->
<script href="<?php echo base_url()?>assets/js/addons/datatables-select.min.js" rel="stylesheet"></script>
<!-- DataTables JS -->
<script href="<?php echo base_url()?>assets/js/addons/datatables.min.js" rel="stylesheet"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script> 
<script type="text/javascript" src="https://cdn.datatables.net/autofill/2.3.4/js/dataTables.autoFill.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.colVis.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>
<script>
   $(document).ready(function () {
   $('#dtBasicExample').DataTable();
   $('.dataTables_length').addClass('bs-select');
   });
</script>