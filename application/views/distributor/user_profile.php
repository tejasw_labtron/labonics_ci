<?php
   $this->load->view('common/breadcrumb',['current' => 'distributor profile']);
?>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/editprofile.css">
<!--products-area start-->
<div class="products-area">
   <div class="container-fluid">
      <div class="row">
         <div class="col-xl-12 col-lg-12">
            <section class="login-block">
               <div class="container">
                  <div class="row justify-content-center">
                     <!-- start left panel -->
                     <div class="col-lg-4 col-md-4">
                        <div class="account-pannel">
                           <div class="card img-profile-block">
                              <div class="text-center">
                                 <div class="padding">
                                    <img class="img-fluid rounded-circle img-thumbnail" src="<?php echo base_url();?>assets/images/profile-new.png" >
                                 </div>
                                 <h4><?php echo $user_data['company_person'] ?></h4>
                                 <p class="taag-line"><?php echo $user_data['email'] ?></p>
                                 <div class="footer">
                                 <strong>Not You?</strong><a href="<?php echo site_url('logout') ?>" class="taag-line-edit">Logout</a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- end left panel -->
                     <!-- start right panel -->
                     <div class="col-lg-8 col-md-8">
                           <div class="auth-box card">
                              <div class="card-block">
                                 <div class="row">
                                    <div class="col-md-12">
                                       <h3 class="text-center heading" style="margin-bottom:25px; fonr-size:20px;">Your Profile</h3>
                                    </div>
                                 </div>
                                 <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label class="labels-new">Company Name</label>
                                     <p class="para-label"><?php echo $user_data['company_name'] ?></p>
                                    </div>
                                    <div class="form-group col-md-6">
                                      <label class="labels-new">Contact Person</label>
                                       <p class="para-label"><?php echo $user_data['company_person'] ?></p>
                                    </div>
                                 </div>
                                 <div class="form-row">
                                    <div class="form-group col-md-6">
                                       <label class="labels-new">Phone Number</label>
                                         <p class="para-label"><?php echo $user_data['contact_number'] ?></p>
                                    </div>
                                    <div class="form-group col-md-6">
                                       <label class="labels-new">Email ID</label>
                                       <p class="para-label"><?php echo $user_data['email'] ?></p>
                                    </div>
                                 </div>
                            
                                 <div class="form-group form-primary"> 
                                     <label class="labels-new">Country</label>
                                  <p class="para-label"><?php echo $user_data['country'] ?></p> 
                                 </div>
                              
                                 <div class="row">
                                    <div class="col text-center mt-30">   
                                       <a href="<?php echo base_url('edit-profile-'.$enc_user_id) ?>"><button type="button" class="btn btn-md btn-primary waves-effect text-center m-b-20 c-frm-btn" > Edit profile </button> </a>
                                    </div>
                                 </div>
                                 <p class="text-inverse text-center" style="color: #545454;background-color: #ffffff;box-shadow: 0 1px 2px 1px #ddd; padding: 10px;margin-top:30px;"><a href="<?php echo base_url('forgot-password') ?>" data-abc="true"><span style="color: #6689F9;font-weight:600">Change Password?  </span></a></p>
                              </div>
                           </div>
                     </div>
                     <!-- end right panel -->
                  </div>
               </div>
            </section>
         </div>
      </div>
   </div>
</div>
<!--products-area end-->
