<?php
   $this->load->view('common/breadcrumb',['current' => 'profile edit']);
?>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/editprofile.css">
<!--products-area start-->
<div class="products-area">
   <div class="container">
      <div class="row">
         <div class="col-xl-12 col-lg-12">
            <section class="login-block">
               <div class="container">
                  <div class="row justify-content-center">
                     <!-- start left panel -->
                     <div class="col-lg-4 col-md-4">
                        <div class="account-pannel">
                           <div class="card img-profile-block">
                              <div class="text-center">
                                 <div class="padding">
                                    <img class="img-fluid rounded-circle img-thumbnail" src="<?php echo base_url();?>assets/images/profile-new.png" >
                                 </div>
                                 <h4><?php echo $user_data['company_person'] ?></h4>
                                 <p class="taag-line"><?php echo $user_data['email'] ?></p>
                                 <div class="footer">
                                 <strong>Not You?</strong><a href="<?php echo site_url('logout') ?>" class="taag-line-edit">Logout</a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- end left panel -->
                     <!-- start right panel -->
                     <div class="col-lg-8  col-md-8">
                        <form class="md-float-material " enctype="multipart/form-data" id="user-edit" name="user-edit" method="post" style="border:0px;">

                            <input type="hidden" name="enc_user_id" id="enc_user_id" value="<?php echo $enc_user_id?>">
                           <div class="auth-box card">
                              <div class="card-block">
                                 <div class="row">
                                    <div class="col-md-12">
                                       <h3 class="text-center heading" style="margin-bottom:25px; font-size:20px;">Edit Profile</h3>
                                    </div>
                                 </div>
                                 <div class="form-row">
                                    <div class="form-group col-md-6">
                                       <div class="form-group form-primary">
                                           <input class="form-control" type="text"  name="c_name" id="c_name" value="<?php echo $user_data['company_name'] ?>"  required />
                                    <div class="help-block errormesssage"></div>
                                         </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                       <div class="form-group form-primary">
                                        <input class="form-control" type="text" name="c_person" id="c_person" value="<?php echo $user_data['company_person'] ?>" required />
                                 <div class="help-block errormesssage"></div>
                                         </div>
                                    </div>
                                 </div>
                                 <div class="form-row">
                                    <div class="form-group col-md-6">
                                       <div class="form-group form-primary">
                                       <input class="form-control" type="text" name="c_phone" id="c_phone" value="<?php echo $user_data['contact_number'] ?>" required />
                                    <div class="help-block errormesssage"></div>
                                         </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                       <div class="form-group form-primary">
                                         <input class="form-control" type="email" name="ci_email" id="ci_email" value="<?php echo $user_data['email'] ?>"   readonly="" />
                                     <input type="hidden" name="c_email" id="c_email" value="<?php echo $user_data['email'] ?>"  />
                                         </div>
                                    </div>
                                 </div>
                                
                                 <div class="form-group form-primary">
                                 <input class="form-control" type="text" name="c_location" id="mylocation" value="<?php echo $user_data['country'] ?>"  required />
                                    <div class="help-block errormesssage"></div>
                                   </div>
                                
                                 <div class="row" style="margin-top:20px;">
                                    <div class="col-md-12 ">   
                                       <!-- <button type="button" class="btn btn-md waves-effect text-center m-b-20 c-frm-btn-12" > Cancel </button>  -->
                                       <button type="submit" class="btn btn-md btn-primary waves-effect text-center m-b-20 c-frm-btn-1 btn_regsave" id="form_submit_reg"> Save Changes </button> 
                                    </div>
                                 </div>
                                 <div class="row" style="margin-top:20px;">
                                    <div class="col-md-12 text-center ">   
                                       <a href="<?php echo base_url('user-profile-'.$enc_user_id) ?>"><button type="button" class="btn btn-md btn-primary waves-effect m-b-20 c-frm-btn" > View profile </button> </a>
                                    </div>
                                 </div>
                                 <p class="text-inverse text-center" style="box-shadow: 0 1px 2px 1px #ddd; padding: 10px;margin-top:30px;"><a href="<?php echo base_url('forgot-password') ?>" data-abc="true"><span style="color: #105e79;font-weight:600">Change Password?  </span></a></p>
                              </div>
                           </div>
                        </form>
                     </div>
                     <!-- end right panel -->
                  </div>
               </div>
            </section>
         </div>
      </div>
   </div>
</div>
<!--products-area end-->