<div class="customer_login mt-60">
   <div class="container">
      <div class="row justify-content-center">
         <div class="col-md-8">
            <div class="card">
               <div class="card-header">Reset Password</div>
                  <div class="card-body">
                     <form class="md-float-material form-material" style="border: 0px;" method="post" id="reset_password_form">
                        <input type="hidden" id="auth_id" value="<?php echo $user_data['id'] ?>">
                        <input type="hidden" id="encrypted_id" value="<?php echo $user_data['encrypted_id'] ?>">
                        <div class="form-group row">
                           <label for="c_password" class="col-md-4 col-form-label text-md-right">Password</label>
                           <div class="col-md-6">
                              <input class="form-control text-left" type="password" name="c_password" id="c_password" placeholder="Enter Your New Password" required>
                              <div class="help-block errormesssage"></div>
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="confirm_password" class="col-md-4 col-form-label text-md-right">Confirm Password</label>
                           <div class="col-md-6">
                              <input class="form-control text-left"  type="password" name="confirm_password" id="confirm_password" placeholder="Re-enter Your New Password" required>
                              <div class="help-block errormesssage"></div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-12 text-center">
                              <button type="submit" class="btn btn-md btn-primary c-frm-btn-pwd" id="form_submit_rst_pswd"> Update</button>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
        </div>
      </div>
   </div>
</div>