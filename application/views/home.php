<!--slider area start-->
<section class="slider_section slider_s_three">
  <div class="slider_area owl-carousel">
    <div class="single_slider d-flex align-items-center" data-bgimg="<?php echo base_url();?>assets/img/slider/slider5.jpg"> </div>
  </div>
</section>
<!--slider area end--> 

<!-- body area start-->
<div class="blog_details">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-6">
        <div class="blog_wrapper mr-4">
          <article class="single_blog">
            <figure>
              <figcaption class="blog_content">
                <div class="post_content">
                  <blockquote>
                    <p>Labonics is a leading manufacturer of Biosafety Cabinets, Fume Hoods, Cleanroom Equipment, Laminar Flow Cabinets and other laboratory furniture for Research, Medical and Industrial laboratories across the globe.</p>
                    <p>We have been focused on efforts to provide one-stop services for various industries that include: Laboratory consulting, designing, planning, manufacturing and installation of furniture. It helps the customers to build modern laboratories, experimental rooms, lab furniture products with reasonable structure, bearing capacity, can be dissembled conveniently, easy maintenance, flexible installation, healthier, cleaner and safer!</p>
                    <p>All Labonics products are tested in compliance with International Standards and certified with CE, ISO 13485:2016 and ISO 9001:2015 Certificates. </p>
                  </blockquote>
                </div>
              </figcaption>
            </figure>
          </article>
        </div>
      </div>
    </div>
  </div>
</div>
<!--blog section area end--> 

<!--product area start-->
<div class="product_area product_tab_style tab_style_five color_eight">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="section_title2">
          <h2>Product Categories</h2>
        </div>
      </div>
    </div>
    <div class="tab-content">
      <div class="tab-pane fade show active" id="Running" role="tabpanel">
        <div class="row">
          <div class="product_carousel product_tab_column3 owl-carousel mb-40">
          <?php foreach($main_cat as $res){ 
               $section = get_cat_section($res['section_id']);
             ?>  

            <div class="col-lg-3">
              <div class="product_items">
                <article class="single_product">
                  <figure>
                    <div class="product_thumb"> <a class="primary_img" href="<?php echo base_url()?><?php echo $section?>/<?php echo $res['page_url']?>"><img src="<?php echo base_url()?>assets/images/categories/<?php echo $res['name'].".jpg";?>" alt="<?php echo $res['name']?>"></a> </div>
                    <figcaption class="product_content">
                      <h4 class="product_name mt-30 clr-1"><a href="<?php echo base_url()?><?php echo $section?>/<?php echo $res['page_url']?>"><?php echo $res['name']?></a></h4>
                    </figcaption>
                    <div class="row price_box">
                      <div class="col-12">
                      <ul class="abt-li">
                        <?php 
                           $this->db->select('*')->from('categories')->order_by('name','ASC')->where('parent_id',$res['id'])->where('status',1)->where('level',1);
                           $query = $this->db->get();
                           $response = array();
                           if($query->num_rows() > 0){
                              $response = $query->result_array();
                           }			
                           $i = 0;
                           foreach($response as $row){
                              $section2 = get_cat_section($row['section_id']);
                              if($i <= 2) { 
                        ?>
                        <li><i class="fa fa-arrow-circle-right"></i> <span class="spc-1"><?php echo $row['name']?></span></li>
                        <?php 
                            }
                            $i++;
                          }
                        ?> 
                      </ul>
                      </div>
                      
                      <div align="center" class="col-12 mt-2">
                        <footer class="btn_more bt-clr"> <a href="<?php echo base_url()?><?php echo $section?>/<?php echo $res['page_url']?>"> Read more <i class="zmdi zmdi-long-arrow-right"></i></a> </footer>
                      </div>
                    </div>
                  </figure>
                </article>
              </div>
            </div>
            <?php 
              }
            ?>
            
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--product area end-->

<section class="main_content_area" align="center">
  <div class="container">
    <div class="section_title mt-10">
      <h2>Product At A Glance</h2>
    </div>
    <div class="account_dashboard">
      <div class="row">
        <div class="col-sm-1 col-md-1 col-lg-1"> 
          <!-- Nav tabs --> 
        </div>
        <div class="col-sm-12 col-md-4 col-lg-4"> 
          <!-- Nav tabs -->
            <div class="dashboard_tab_button">
               <ul class="nav flex-column dashboard-list">
                  <?php 
                  if(isset($rndm_products) && $rndm_products!=''){    
                     
                  $i = 1;
                     foreach($rndm_products as $res){ 
                        if($i <= 7) { 
                  ?>
                  <li><a href="#<?php echo $res['sku']?>" data-toggle="tab" class="nav-link <?php if($i == 1) echo 'active'; ?>"><?php echo $res['name']?></a></li>
                  <?php 
                     }
                     $i++;
                     }
                  }
                  ?>
               </ul>
            </div>
        </div>
        <div class="col-sm-12 col-md-7 col-lg-7" align="center"> 
          <!-- Tab panes -->
            <div class="tab-content dashboard_content">
               <?php 
                  if(isset($rndm_products) && $rndm_products!=''){    
                  $i = 1;
                  foreach($rndm_products as $res){ 
                     if($i <= 7) { 
                  $image_url =  image_url_helper($res['image_url'],'small'); 
                  $section = get_prd_section($res['category_id']);
               ?>
               <div class="tab-pane fade <?php if($i == 1)  echo'active show';?>" id="<?php echo $res['sku']?>">
                  <h3><a  href="<?php echo base_url().$section.'/'.$res['page_url']?>" ><?php echo $res['name']?></a></h3>
                  <div class="row">
                     <div class="col-lg-3 ">
                        <div class="brd-1"> <img src="<?php echo base_url().$image_url ?>" alt="<?php echo $res['name']?>"></div>
                     </div>
                     <div class="col-lg-8">
                        <div class="table-responsive">
                        <table class="table">
                           <tbody>
                              <?php $j = 1;
                              $specifications = str_replace(array("\t","\n"), "", $res['specifications']);
                              $specs = json_decode($specifications);

                              foreach($specs as $key => $value){
                                 if($j <= 4) {
                              echo "<tr>";
                              echo "<td>".$key."</td>"; 
                              echo "<td>".$value."</td>"; 
                              echo "</tr>";
                              }
                                    $j++;
                              }
                              ?>
                           </tbody>
                        </table>
                        </div>
                     </div>
                  </div>
               </div>
               <?php 
                     }
                  $i++;
                  }
               }
               ?>
          </div>
        </div>
        
      </div>
    </div>
  </div>
</section>

<!--instagram area start-->
<!-- <div class="instagram_area">
  <div class="container-fluid p-0">
    <div class="row no-gutters">
      <div class="col-12">
        <div class="instagram_container instagram_column7 owl-carousel">
          @php $i = 1;
            $products = App\Models\Product::inRandomOrder()->get();
            foreach($products as $product){
              $category = App\Models\Category::find($product->category_id);
              if($i <= 19) { 
          @endphp
          <div class="single_instagram"> <a href="/{{$category->section->slug}}/{{$product->page_url}}"><img src="{{ asset('storage/'.$product->image) }}" alt="{{$product->name}}"></a> </div>
          @php 
              }
              $i++;
            }
          @endphp 
        </div>
      </div>
    </div>
  </div>
</div> -->
<!--instagram area end--> 
