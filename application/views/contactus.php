<?php
   $this->load->view('common/breadcrumb',['current' => 'contact us']);
?>
    <!--contact area start-->
    <div class="contact_area mt-30">
        <div class="container">  
            <div class="section_title2 mt-10">
            <h2>Get In Touch</h2>
            </div> 
            <div class="row">
                <div class="col-lg-6 col-md-12">
                   <div class="contact_message form">
                      
                        <form class="register-form" role="form" id="contact_form">
                             <div class="row"> 
                                <div class="form-group col-md-6">
                                    <label for="name" >Name <span class="required"> *</span></label>

                                        <input id="name" type="text" class="form-control" name="name" placeholder="Enter Name" required autofocus>

                                        <div class="help-block errormesssage"></div>

                                </div>
                                <div class="form-group col-md-6">
                                    <label for="phone" >Phone <span class="required"> *</span></label>

                                        <input id="phone" type="text" class="form-control" name="phone" placeholder="Enter Phone No" required autofocus>

                                        <div class="help-block errormesssage"></div>

                                </div>
                                <div class="form-group col-md-6">
                                    <label for="email" >Email <span class="required"> *</span></label>

                                        <input id="email" type="text" class="form-control" name="email" placeholder="Enter Email" required autofocus>

                                        <div class="help-block errormesssage"></div>

                                </div>
                                <div class="form-group col-md-6">
                                    <label for="subject" >Subject <span class="required"> *</span></label>

                                        <input id="subject" type="text" class="form-control " name="subject"  placeholder="Enter Subject" required autofocus>

                                        <div class="help-block errormesssage"></div>

                                </div>
                                <div class="form-group col-md-12">
                                    <label for="message" >Message <span class="required"> *</span></label>

                                        <textarea id="message" placeholder="Your message" name="message"  class="form-control" required ></textarea>     

                                        <div class="help-block errormesssage"></div>

                                </div>
                            </div> 
                            <div class="col-sm-12 mt-20">

                              <button class="btn-common" type="submit" id="form_submit_reg">Send message</button>

                              </div>



                              <div class="col-sm-8 text-left pt-30">

                              <div id="msgSubmit" class="hidden form-messege"></div>

                              </div>
                        </form> 

                    </div> 
                </div>
                <div class="col-lg-6 col-md-12">
                   <div class="contact_message content">
                        <h3 class="clr-1">Labonics Systems Ltd</h3>
                        <ul class="abt-li3">
                            <li><i class="fa fa-map-marker"></i>  128 Leucha road Walthamstow 
                            London E17 7lq UK</li>
                            <li><i class="fa fa-globe"></i><a href="https://www.labonics.com/" target="_blank">www.labonics.com</a>  </li>

                            <li><i class="fa fa-envelope-o"></i> <a href="mailto:Infor@roadthemes.com">Infor@roadthemes.com</a></li>
                          
                        </ul>             
                    </div> 
                </div>
            </div>
        </div>    
    </div>

    <!--contact area end-->