
<?php
   $this->load->view('common/breadcrumb',['current' => 'sitemap']);
?>
<div id="about-overview">
   <div class="section-header">
      <div class="content-header mb-30">
         <div class="container sitemap-container">
            <h3 class="site mt-30 "> Sitemap </h3>
               <div class="row">
                  <div class="col-md-9 col-sm-8 col-xs-12">    
                     <div class="content-panel sitemap">
                        <ul>
                        <li>
                           <a class="l0bg" href="<?php echo site_url(); ?>">Home</a>
                           <ul>
                              <li><a class="l1bg" href="<?php echo site_url('about-us'); ?>">About Us</a></li>
                              <li><a class="l1bg" href="<?php echo site_url('contact-us'); ?>">Contact Us</a></li>
                              <li><a class="l1bg" href="<?php echo site_url('customization'); ?>">Customization</a></li>
                              <li><a class="l1bg" href="<?php echo site_url('products'); ?>">Products</a></li>
                              <li>
                                 <a class="l1bg" href="<?php echo site_url('categories'); ?>">Categories</a>
                                 <ul>
                                    <?php 
                                       if(isset($all_category) && $all_category !=''){
                                          foreach ($all_category as $key => $cat) { 
                                             if($cat['level'] == 0){
                                                $section = get_cat_section($cat['section_id']);
                                    ?>
                                    <li>
                                       <a class="l4bg" href="<?php echo $cat['category_url'] ?>"><?php echo $cat['name'] ?></a>
                                       <?php 
                                          $this->db->select('*')->from('categories')->order_by('name','ASC')->where('parent_id',$cat['id'])->where('status',1)->where('level',1);
                                          $query = $this->db->get();
                                          if($query->num_rows() > 0){
                                             $level_one = $query->result_array();
                                          }	                             
                                          if(isset($level_one) && $level_one !=''){
                                             foreach($level_one as $sub_cat){
                                                if($cat['id'] == $sub_cat['parent_id']){
                                       ?>
                                          <ul>
                                             <li><a class="l6bg" href="<?php echo base_url().$section.'/'.$sub_cat['page_url']?>"><?php echo $sub_cat['name']?></a> </li>
                                             <ul>
                                                <?php 
                                                
                                                   $this->db->select('*')->from('categories')->order_by('name','ASC')->where('parent_id',$sub_cat['id'])->where('status',1)->where('level',2);
                                                   $query2 = $this->db->get();
                                                   if($query2->num_rows() > 0){
                                                      $level_two = $query2->result_array();
                                                   }	
                                                   if(isset($level_two) && $level_two !='')
                                                      foreach($level_two as $sub_cat2){
                                                         if($sub_cat['id'] == $sub_cat2['parent_id']){
                                                ?>
                                                   <li><a class="l7bg" href="<?php echo base_url().$section.'/'.$sub_cat2['page_url']?>"><?php echo $sub_cat2['name']?></a></li>
                                                <?php 
                                                      }
                                                   }
                                                   
                                                ?>  
                                             </ul> 
                                          </ul>
                                     <?php } }}?>
                                    </li>
                                   <?php }}} ?>
                                 </ul>
                              </li>
                             
                           </ul>
                        </li>
                     </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

