<?php
$cart_data = $this->cart->contents(); 
$session_user_id = $this->session->userdata('id'); 
  if(isset($cart_data) && $cart_data != '' && !empty($cart_data)){  
     
   echo "<div class='cart_gallery'>"; 
   foreach ($cart_data as $items)
          {
            $sproductData['product_sku']  = $items['id'];
            $productDetail                = $this->product_model->get_products('row_array',$sproductData);
            $image_url                    = image_url_helper($productDetail['image_url'],'small');
            $section = get_prd_section($productDetail['category_id']);

         ?>
               
            <div class="cart_item">
               <div class="cart_img">
                  <a href="<?php echo base_url()?><?= $section ?>/<?php echo $productDetail['page_url']?>"><img src="<?php echo base_url().$image_url ?>" alt="<?php echo $productDetail['name']; ?>"></a>
               </div>
               <div class="cart_info">
               <?php 
              $price = '';
              if(!empty($session_user_id) && $session_user_id != '' && !empty($items['price'])){ 
                $price = ' X Price: '.$items['price']; }?>
                  <span class="clr-1">Qty: <?php echo $items['qty'].$price ?></span>
                     <a href="<?php echo base_url()?><?= $section ?>/<?php echo $productDetail['page_url']?>"><?php echo $productDetail['name']; ?></a>
                     
                  </div>
               <div class="cart_remove">
                  <a href="#"><i class="ion-android-close delete-from-cart" data-productrowid="<?php echo $items['rowid'];?>"></i></a>
               </div>
            </div> 

         <?php  }
         echo "</div>"; 
         ?>
     
       <?php if(!empty($session_user_id) && $session_user_id != ''){  ?>
      <div class="minicart-total fix">
         <span class="pull-left clr-1">TOTAL:</span>
         <span class="pull-right clr-1">$<?php echo $this->cart->format_number($this->cart->total()); ?></span>
      </div>
      <?php } ?>
      <div class="mini_cart_footer mini-cart-checkout">
                     <a class="cart_button" href="<?php echo base_url('cart-view') ?>">View cart</a>
                     <a href="#" class="btn-common cart_button checkout mt-10 empty-cart">Empty cart</a>
                  </div>



 <?php }else{ ?>
   <ul> <h5>  Oops !! Your cart is empty !! </h5></ul>
    <br>
  <?php } ?>

    
   <script src="<?php echo base_url()?>assets/js/cart.js" type="text/javascript"></script>