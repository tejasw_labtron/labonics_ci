<?php
   $this->load->view('common/breadcrumb',['current' => 'customization']);
?>
    <!--customization area start-->
    <div class="contact_area mt-30">
        <div class="container">  
            <div class="section_title2 mt-10">
                <h2>CUSTOMIZATION FORM</h2>
                <p class="mt-2 clr-4"> Tell us about your specific product requirements</p>
            </div> 
            <div class="row" >
                <div class="col-lg-1 col-md-1">
                </div>
                <div class="col-lg-10 col-md-10">
                    <div class="contact_message form">
                      
                        <form class="register-form" role="form" id="customization_form">
                                                    
                            <div class="row"> 
                                <div class="form-group col-md-6">
                                    <label for="contact_person" >Name <span class="required"> *</span></label>

                                        <input id="contact_person" type="text" class="form-control" name="contact_person"  placeholder="Enter Name" required autofocus>

                                        <div class="help-block errormesssage"></div>

                                </div>
                                <div class="form-group col-md-6">
                                    <label for="phone" >Phone <span class="required"> *</span></label>

                                        <input id="phone" type="text" class="form-control" name="phone"  placeholder="Enter Phone No" required autofocus>

                                        <div class="help-block errormesssage"></div>

                                </div>
                                <div class="form-group col-md-6">
                                    <label for="email" >Email <span class="required"> *</span></label>

                                        <input id="email" type="text" class="form-control" name="email" placeholder="Enter Email" required autofocus>

                                        <div class="help-block errormesssage"></div>

                                </div>
                                <div class="form-group col-md-6">
                                    <label for="company_name" >Company Name <span class="required"> *</span></label>

                                        <input id="company_name" type="text" class="form-control" name="company_name" placeholder="Enter Company Name" required autofocus>

                                        <div class="help-block errormesssage"></div>

                                </div>
                                <div class="form-group col-md-12">
                                    <label for="product_name" >Product Name <span class="required"> *</span></label>

                                        <input id="product_name" type="text" class="form-control" name="product_name" placeholder="Enter Product Name" required autofocus>

                                        <div class="help-block errormesssage"></div>

                                </div>
                                <div class="form-group col-md-12">
                                    <label for="product_specifications" >Product Design Specifications <span class="required"> *</span></label>

                                        <textarea id="product_specifications" placeholder="About your product specifications" name="product_specifications"  class="form-control" required ></textarea>     

                                        <div class="help-block errormesssage"></div>

                                </div>
                            </div>  

                            <div class="col-sm-12 mt-20">

                            <button class="btn-common" type="submit" id="form_submit_reg">Send message</button>

                            </div>



                            <div class="col-sm-8 text-left pt-30">

                            <div id="msgSubmit" class="hidden form-messege"></div>

                            </div>                      
                        </form> 

                    </div> 
                </div>
                
            </div>
        </div>    
    </div>

    <!--customization area end-->