<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    protected function render_page($view,$data)
    {
        $view_data = $data;
        
		/****** COMMON DATA  ****/
		$data 							= common_data();
        
		/****** COMMON DATA  ****/
		$data['main_cat'] = $this->general_model->get_main_categories();
		$data['rand_prod'] = $this->general_model->get_random_products();
		$data['main_section'] = $this->general_model->get_main_section();
    
        $this->load->view('common/header', $data);
        $this->load->view($view, $view_data);
        $this->load->view('common/footer', $data);
    }   

}
