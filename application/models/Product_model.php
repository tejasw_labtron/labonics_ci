<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_model extends CI_Model
{

    public function get_products($array_type,$data = array())
    {
        $this->db->select('prd.*,pl.final_inr as price,cat.page_url as category_url,pl.final_inr, cat.name as category_name');
        $this->db->from('products prd');
        $this->db->join('categories cat','cat.id = prd.category_id','left');
        $this->db->join('price_list as pl','pl.sku = prd.sku','left');

        $this->db->where('prd.status',STATUS_ACTIVE);

        if(isset($data['category_section']) && $data['category_section'] !='' && $data['category_section'] != 'all')
        {
            $this->db->where('cat.section_id',$data['category_section']);
        }
       /* if(isset($data['product_url']) && $data['product_url'] !='')
        {
            $this->db->where('prd.url_title',$data['product_url']);
        }*/
        if(isset($data['product_category']) && $data['product_category'] !='')
        {
            $this->db->where('prd.category_id',$data['product_category']);
        }

        if(isset($data['product_category_ids']) && $data['product_category_ids'] !='')
        {
            $this->db->where_in('prd.category_id',$data['product_category_ids']);
        }

        if(isset($data['product_name']) && $data['product_name'] !='')
        {
            $this->db->where('prd.name',$data['product_name']);
        }
        if(isset($data['search_product']) && $data['search_product'] !='')
        {
            $this->db->like('prd.name',$data['search_product'], 'both');
        }
        if(isset($data['product_sku']) && $data['product_sku'] !='')
        {
            $this->db->where('prd.sku',$data['product_sku']);
        }
        if(isset($data['product_skus']) && $data['product_skus'] !='')
        {
            $this->db->where_in('prd.sku',$data['product_skus']);
        }
        if(isset($data['product_id']) && $data['product_id'] !='')
        {
            $this->db->where('prd.id',$data['product_id']);
        }
        if(isset($data['except_id']) && $data['except_id'] !='')
        {
            $this->db->where_not_in('prd.id',$data['except_id']);
        }
        if(isset($data['product_ids']) && $data['product_ids'] !='')
        {
            $this->db->where_in('prd.id',$data['product_ids']);
        }
        if(isset($data['product_limit']) && $data['product_limit'] !='')
        {
            $this->db->limit($data['product_limit']);
        }
        if(isset($data['product_orderby']) && $data['product_orderby'] !='')
        {
            $this->db->order_by($data['product_orderby'],'asc');
        }else{
            $this->db->order_by('rand()');
        }
   
        $query = $this->db->get();

        $response = array();

        if($query->num_rows() > 0)
        {
            $response = $query->$array_type();
        }
        return $response; 
    }

    public function get_new_products($array_type)
    {
        $this->db->select('prd.*,pl.final_inr as price,cat.page_url as category_url,pl.final_inr, cat.name as category_name');
        $this->db->from('products prd');
        $this->db->join('categories cat','cat.id = prd.category_id','left');
        $this->db->join('price_list as pl','pl.sku = prd.sku','left');

        $this->db->where('prd.status',STATUS_ACTIVE);
         $this->db->limit(8);
        $this->db->order_by('prd.id','desc');


        $query = $this->db->get();

        $response = array();

        if($query->num_rows() > 0)
        {
            $response = $query->$array_type();
        }
        return $response; 
    }

    public function get_category($array_type,$data = array())
    {
        $this->db->select('*');
        $this->db->from('categories');

        if(isset($data['category_url']) && $data['category_url'] !='')
        {
            $this->db->where('url_title',$data['category_url']);
        }
        if(isset($data['category_page_url']) && $data['category_page_url'] !='')
        {
            $this->db->where('page_url',$data['category_page_url']);
        }
        if(isset($data['category_id']) && $data['category_id'] !='')
        {
            $this->db->where('id',$data['category_id']);
        }
        if(isset($data['category_parent_id']) && $data['category_parent_id'] !='')
        {
            $this->db->where('parent_id',$data['category_parent_id']);
        }
         if(isset($data['category_parent_ids']) && $data['category_parent_ids'] !='')
        {
            $this->db->where_in('parent_id',$data['category_parent_ids']);
        }
        if(isset($data['category_section']) && $data['category_section'] !='' && $data['category_section'] != 'all')
        {
            $this->db->where('section_id',$data['category_section']);
        }
        if(isset($data['category_level']) && $data['category_level'] !='' && $data['category_level'] != 'all')
        {
            $this->db->where('level',(int)$data['category_level']);
        }
        // if(empty($data['category_id'])  && empty($data['category_level']) && empty($data['category_page_url']))
        // {
        //      $this->db->where('level',0);
        // }
       

   
        $this->db->where('status',STATUS_ACTIVE);
        $this->db->order_by('level asc, name asc'); 
    

        $query = $this->db->get();

        $response = array();

        if($query->num_rows() > 0)
        {
            $response = $query->$array_type();
        }
        if($array_type == 'result_array')
        {
            $categories = array();
            
            foreach($response as $category)
            {
                $categories[$category['id']] = $category;
            }  

            foreach($categories as $category_id => $category) 
            {
                if($category['level'] == 0) 
                {       
                    $categories[$category_id]['all_children_ids'] = array();
                    $categories[$category_id]['children_ids'] = array(); 
                    $this->make_nodes_leaves($categories, $category_id);                
                }
            }

            $products = $this->get_first_products();

            foreach($categories as $category){
                foreach($products as $product){
                    if($category['id']==$product['category_id']){
                        $categories[$category['id']]['image_url'] = $product['image_url'];
                    }
                    if(!empty($category['all_children_ids'])){
                        for($i=0;$i<sizeof($category['all_children_ids']);$i++){
                            if($category['all_children_ids'][$i]==$product['category_id']){
                                $categories[$category['id']]['image_url'] = $product['image_url'];
                            } 
                        } 
                    }
                } 
                $categories[$category['id']]['category_url'] =  base_url().$category['page_url']; 
            }
            
            return $categories;    
        }
        else
        {
            return $response;
        }
    }

    public function get_section($array_type,$data = array())
    {
        $this->db->select('*');
        $this->db->from('sections');


        if(isset($data['section_id']) && $data['section_id'] !='')
        {
            $this->db->where('id',$data['section_id']);
        }
        if(isset($data['section_url']) && $data['section_url'] !='')
        {
            $this->db->where('page_url',$data['section_url']);
        }
        if(isset($data['section_name']) && $data['section_name'] !='' && $data['section_name'] != 'all')
        {
            $this->db->where('section',$data['section_name']);
        }
        
        // $this->db->where('status',STATUS_ACTIVE);
        $this->db->order_by('id','asc');

        $query = $this->db->get();
        $response = array();

        if($query->num_rows() > 0)
        {
            $response = $query->$array_type();
        }
        return $response;
    }

    function make_nodes_leaves(&$categories, &$parent_id)
    {
        //creating tree structure for category hierarchy
        foreach($categories as $category_id => $category) 
        {
            if($category['parent_id'] == $parent_id)
            {
                $categories[$parent_id]['children_ids'][]       = $category_id;
                $categories[$parent_id]['all_children_ids'][]   = $category_id;
                $categories[$category_id]['all_children_ids']   = array();
                $categories[$category_id]['children_ids']       = array();
                $this->make_nodes_leaves($categories, $category_id);
                $categories[$parent_id]['all_children_ids']     = array_merge($categories[$parent_id]['all_children_ids'], $categories[$category_id]['all_children_ids']);
            }   
        }
    }
    
    function get_first_products()
    {
        //get first product of all categories
        $this->db->select('sku,name,category_id,image_url,page_url')->from('products')->where('status',STATUS_ACTIVE)->order_by('category_id');
        $query = $this->db->get();
        $response = array();
        if($query->num_rows() > 0)
        {
            $response = $query->result_array();
        }
        
        $products = array();

        foreach($response as $product)
        {
            if(empty($products[$product['category_id']]))
            {
                $products[$product['category_id']] = $product;
            }            
        }   
        return $products;             
    }
    
    public function get_alphabets($table)
    {
        $this->db->select('SUBSTRING(name,1,1) as alpha',FALSE)->from($table)->where('status',STATUS_ACTIVE)->order_by('name','ASC');
        $query = $this->db->get();
        $response = array();
        if($query->num_rows() > 0)
        {
            $response = $query->result_array();
        }
        $alpha = array();
        foreach($response as $letter)
        {
            if(!in_array($letter['alpha'],$alpha))
            {
                $alpha[] = $letter['alpha'];                
            }
        }
        return $alpha;
    }

    public function get_cat_products($array_type,$data = array())
    {
        $id = $data['category_section'];
        $q = $data['search_product'];
        
        if($id != ''){
            $this->db->select('prd.*,cat.page_url as category_url,cat.name as category_name');
            $this->db->from('products prd');
            $this->db->join('categories cat','cat.id = prd.category_id','left');
            $this->db->join('categories b','b.id = cat.parent_id');
            $this->db->where('prd.status',STATUS_ACTIVE);
            $this->db->group_start(); //this will start grouping
            $this->db->where('cat.id',$id);
            $this->db->or_where('cat.parent_id',$id);
            $this->db->or_where('b.parent_id',$id);
            $this->db->group_end(); //this will end grouping
            $this->db->group_start(); //this will start grouping
            $this->db->like('prd.name', $q);
            $this->db->or_like('prd.sku', $q);
            $this->db->group_end(); //this will end grouping
            $query = $this->db->get();
        }else{
            $this->db->select('*');
            $this->db->from('products');
            $this->db->where('status',STATUS_ACTIVE);
            $this->db->group_start(); //this will start grouping
            $this->db->like('name', $q);
            $this->db->or_like('sku', $q);
            $this->db->group_end(); //this will end grouping
            $query = $this->db->get();

        }


        $response = array();

        if($query->num_rows() > 0)
        {
            $response = $query->$array_type();
        }
        return $response; 
    }

    public function get_category_products($array_type,$data = array())
    {
        $this->db->select('cat.*');
        $this->db->from('categories cat');
        $this->db->join('products prd','cat.id = prd.category_id','left');
        $this->db->join('price_list as pl','pl.sku = prd.sku');
        $this->db->where('cat.status',STATUS_ACTIVE);

        
        $query = $this->db->get();

        $response = array();

        if($query->num_rows() > 0)
        {
            $response = $query->$array_type();
        }
        return $response; 
    }

}
?>