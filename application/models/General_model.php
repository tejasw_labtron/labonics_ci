<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class General_model extends CI_Model
{

	 /* Directly writing the sql query */
    function executeSqlQuery($sqlQuery, $resType)
    {

        $query = $this->db->query($sqlQuery);
        switch ($resType)
        {
        case 'row':
            $result = $query->row();
            break;

        case 'result':
            $result = $query->result();
            break;

        case 'result_array':
            $result = $query->result_array();
            break;

        case 'row_array':
            $result = $query->row_array();
            break;

        case 'update':
            $result = 'true';
            break;

        default:
            $result = '';
            break;
        }

        // echo ' last query : '.$this->db->last_query();

        return $result;
    }


    public function getauthData($data = array())
    {
        log_message('ERROR','>> getauthData function');

        $this->db->select('*');
        $this->db->from('distributors');
        
        if(isset($data['auth_id']) && $data['auth_id'] != '')
        {
            $this->db->where('id',$data['auth_id']);
        }

        if(isset($data['email']) &&  $data['email'] != '')
        {
             $this->db->where('email', $data['email']);
        }

        if(isset($data['password']) && $data['password'] != '')
        {
            $this->db->where('password', $data['password']);
        }

         $query = $this->db->get();

        log_message('ERROR','>> getauthData >> '.$this->db->last_query());

       

        if ($query->num_rows() > 0)
        {
            return $query->row_array();
        }

        return false;
    }

    function distributor_list()
    {
        $this->db->select('*')->from('distributors');
        
        $query = $this->db->get();
        $response = array();
        if($query->num_rows() > 0){
            $response = $query->result_array();
        }
        return $response;
    }

    public function chk_email_exists($emails = '')
    {
        $emails = $this->db->escape_str($emails);
        $this->db->select('id');
        $this->db->from('distributors');
        $this->db->where('email',$emails);
        $query = $this->db->get();
    
        if ($query->num_rows() > 0)
        {
            return true;
        }
        return false;
    }
    public function chk_username_exists($username = '')
    {
        $this->db->select('id');
        $this->db->from('distributors');
        $this->db->where('username',$username);
        $query = $this->db->get();
    
        if ($query->num_rows() > 0)
        {
            return true;
        }
        return false;
    }
	function get_main_section(){
		$this->db->select('*')->from('sections');
		$this->db->order_by('name', 'ASC');
		$query = $this->db->get();
		$response = array();
		if($query->num_rows() > 0){
			$response = $query->result_array();
		}
        // for($i=0;$i<sizeof($response);$i++){
        //     if(array_search("Fume-Hoods",$response[$i])){
        //         $swap1 = $i;
        //     }
        //     if(array_search("Laboratory-Furniture",$response[$i])){
        //         $swap2 = $i;
        //     }
        //     if(array_search("Laminar-Airflow-Cabinets",$response[$i])){
        //         $swap3 = $i;
        //     }
        // }
        // $temp = array();
        // $temp = $response[$swap1];
        // $response[$swap1] = $response[$swap3];
        // $response[$swap3] = $response[$swap2];
        // $response[$swap2] = $temp;
		return $response;
	}
	function get_all_categories(){
		
		$this->db->select('*')->from('categories')->order_by('name','ASC')->where('status',1)->where('level',0);
		
		/* $this->db->select('c.*');
		$this->db->where('c.inactive', 0);
		//$this->db->distinct();
		$this->db->order_by('c.name', 'ASC');
		$this->db->from('categories as c');
		$this->db->join('products as p', 'p.category_id = c.id');
		$this->db->group_by('c.name'); */
		$query = $this->db->get();
		$res = array();
		if($query->num_rows() > 0){
			$res = $query->result_array();
		}
		return $res;
	}
	function get_all_products(){
		$this->db->select('id,sku,name,category_id,page_url');
		$this->db->order_by('name', 'ASC');
		$this->db->from('products');
		$query = $this->db->get();
		$res = array();
		if($query->num_rows() > 0){
			$res = $query->result_array();
		}
		return $res;
	}
	function get_main_categories(){
		$this->db->select('*')->from('categories')->order_by('name','ASC')->where('status',1)->where('parent_id',0);
		$query = $this->db->get();
		$response = array();
		if($query->num_rows() > 0){
			$response = $query->result_array();
		}
		return $response;
	}
	function get_subcat_specific($catid){
		$this->db->select('*')->from('categories')->where('parent_id',$catid)->where('status',1);
		$query = $this->db->get();
		$response = array();
		if($query->num_rows() > 0){
			$response = $query->result_array();
		}
		return $response;
	}
	function get_cat_specific($caturl){
		$this->db->select('*')->from('categories')->where('url_title',$caturl);
		$query = $this->db->get();
		$response = array();
		if($query->num_rows() > 0){
			$response = $query->result_array();
		}
		return $response;
	}
	function get_all_subcats_products($catid){
		$this->db->select('p.*');
		$this->db->where('ca.id',$catid);
		$this->db->from('categories as ca');
		$this->db->join('categories as cb', 'ca.id = cb.parent_id', 'self');
		$this->db->join('products as p', 'cb.id = p.category_id', 'left');
		$query = $this->db->get();
		$response = $query->result_array();
		return $response;
	}
	function get_products_specific($catid){
		$this->db->select('*')->from('products')->where('category_id',$catid);
		$query = $this->db->get();
		$response = array();
		if($query->num_rows() > 0){
			$response = $query->result_array();
		}
		/* echo "<pre>";
		print_r($response);die; */
		return $response;
	}
	function get_subcat_url_specific($subcaturl){
		$this->db->select('*')->from('categories')->where('url_title',$subcaturl)->where('status',1);
		$query = $this->db->get();
		$response = array();
		if($query->num_rows() > 0){
			$response = $query->result_array();
		}
		/* echo "<pre>";
		print_r($response);die; */
		return $response;
	}
	function get_prod_for_subcat($subcat_id){
		$this->db->select('*')->from('products')->where('category_id',$subcat_id);
		$query = $this->db->get();
		$response = array();
		if($query->num_rows() > 0){
			$response = $query->result_array();
		}
		/* echo "<pre>";
		print_r($response);die; */
		return $response;
	}
	function get_final_prod_data($produrl){
		$this->db->select('p.*,pl.final_inr');
		$this->db->from('products as p');
		$this->db->join('price_list as pl','pl.sku = p.sku','left');
		$this->db->where('p.url_title',$produrl);
		$query = $this->db->get();
		$response = array();
		if($query->num_rows() > 0)
		{
			$response = $query->result_array();
		}
		/* echo "<pre>";
		print_r($response);die; */
		return $response;
	}
	function get_rel_prod_data($rel_id){
		$this->db->select('*')->from('products')->where('category_id',$rel_id);
		$query = $this->db->get();
		$response = array();
		if($query->num_rows() > 0){
			$response = $query->result_array();
		}
		return $response;
	}
	function get_random_products(){
		$this->db->select('*')->from('products');
		$this->db->order_by('id','RANDOM');
		$this->db->limit(20);
		$query = $this->db->get();
		$response = array();
		if($query->num_rows() > 0){
			$response = $query->result_array();
		}
		return $response;
	}
	function get_meta_home(){
		$key = "home";
		$this->db->select('*')->from('pages')->where('name',$key);
		$query = $this->db->get();
		$response = array();
		if($query->num_rows() > 0){
			$response = $query->result_array();
		}
		return $response;
	}
	function get_meta_about(){
		$key = "about";
		$this->db->select('*')->from('pages')->where('name',$key);
		$query = $this->db->get();
		$response = array();
		if($query->num_rows() > 0){
			$response = $query->result_array();
		}
		return $response;
	}
}
?>